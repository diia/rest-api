package models;

import com.avaje.ebean.Ebean;
import com.avaje.ebean.Model;
import com.avaje.ebean.SqlRow;
import com.avaje.ebean.SqlUpdate;
import org.joda.time.Years;

import javax.persistence.*;
import javax.validation.constraints.Size;
import java.time.Duration;
import java.time.LocalDate;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.concurrent.TimeUnit;

@Entity
public class Estudiante {
    public static char tipoNodo = 'e';
    @Id
    @Column(name = "id_estudiante")
    private Long idEstudiante;
    @Size(max = 150)
    private String nombre;
    @Size(max = 150)
    private String apellidos;
    @Size(max = 10)
    private String cedula;
    private Date fechaNacimiento;
    private char sexo;
    //@Column(columnDefinition = "decimal(4,2) DEFAULT '0.00'")
    private float promedioVigente;
    //@Column(columnDefinition = "decimal(4,2) DEFAULT '0.00'")
    private float promedioAcumulado;
    @Size(max = 100)
    private String correo;
    @Size(max = 13)
    private String telefono;
    @Size(max = 100)
    private String institucion;
    @Size(max = 20)
    private String idFacebook;
    @Size(max = 20)
    private String idPam;
    @Size(max = 20)
    private String idCrea;
    @Size(max = 20)
    private String idMoodle;
    @Size(max = 150)
    private String ciudad_uno;
    @Size(max = 150)
    private String localidad_uno;
    @Size(max = 150)
    private String domicilio_uno;
    @Size(max = 150)
    private String ciudad_dos;
    @Size(max = 150)
    private String localidad_dos;
    @Size(max = 150)
    private String domicilio_dos;

    public static Model.Finder<Long, Estudiante> find() {
        return new Model.Finder<>(Estudiante.class);
    }

    public SqlRow detalle(){
        String sql = "SELECT Estudiante.* FROM Estudiante WHERE id_estudiante = :id_estudiante";
        return Ebean.createSqlQuery(sql)
                .setParameter("id_estudiante", this.idEstudiante)
                .findUnique();
    }

    public List<SqlRow> trabajos(){
        String sql = "SELECT * FROM Trabajo WHERE id_estudiante = :id_estudiante";
        return Ebean.createSqlQuery(sql)
                .setParameter("id_estudiante", this.idEstudiante)
                .findList();
    }

    public List<SqlRow> informacion(){
        String interaccionesSql = "SELECT Estudiante.* FROM Estudiante WHERE id_estudiante = :id_estudiante";
        List<SqlRow> interacciones = Ebean.createSqlQuery(interaccionesSql)
                .setParameter("id_estudiante", this.idEstudiante)
                .findList();
        return interacciones;
    }

    public List<SqlRow> estudianteCon(String fechaInicio, String fechaFin, String plataforma, String date_filter){
        String estConSql = "SELECT to_char(conexion.timestamp::date, ':date_filter') AS fecha, COUNT(*) AS cantidad FROM conexion WHERE id_estudiante=:id_estudiante AND plataforma = ':plataforma' AND timestamp BETWEEN ':fecha_inicio' AND ':fecha_fin' GROUP BY fecha;";
        estConSql=estConSql.replace(":id_estudiante", this.idEstudiante.toString());
        estConSql=estConSql.replace(":plataforma", plataforma);
        estConSql=estConSql.replace(":fecha_inicio", fechaInicio);
        estConSql=estConSql.replace(":fecha_fin", fechaFin);
        estConSql=estConSql.replace(":date_filter", date_filter);

        return Ebean.createSqlQuery(estConSql)
                //.setParameter("id_estudiante", this.idEstudiante.toString())
                //.setParameter("plataforma", plataforma)
                //.setParameter("fecha_inicio", fechaInicio)
                //.setParameter("fecha_fin", fechaFin)
                .findList();
    }

    public List<SqlRow> cursos(){
        /*
        return Ebean.find(Curso.class)
                //.setDistinct(true)
                //.fetch("titulo")
                //.select("titulo")
                .where().in("idCurso",
                        Ebean.createQuery(EstudianteCurso.class)
                                .where().eq("idEstudiante", this.idEstudiante)
                                .select("idCurso")
                )
                .findList();
        */
        //SELECT idCurso, titulo from curso WHERE id_curso IN (SELECT id_curso FROM EstudianteCurso where id_estudiante = :id_estudiante)
        String cursosSql = "SELECT id_curso, titulo, (SELECT riesgo FROM RiesgoCurso WHERE id_estudiante = :id_estudiante ORDER BY riesgo DESC LIMIT 1) AS riesgo, (select CONCAT_WS(', ', CASE WHEN length(id_facebook)>0 THEN 'facebook' ELSE NULL END, CASE WHEN length(id_moodle)>0 THEN 'moodle' ELSE NULL END, CASE WHEN length(id_pam)>0 THEN 'pam' ELSE NULL END, CASE WHEN length(id_crea)>0 THEN 'crea' ELSE NULL END) from estudiante where id_estudiante = :id_estudiante) AS plataformas from curso WHERE id_curso IN (SELECT id_curso FROM EstudianteCurso where id_estudiante = :id_estudiante)";
        List<SqlRow> cursos = Ebean.createSqlQuery(cursosSql)
                .setParameter("id_estudiante", this.idEstudiante)
                .findList();
        return cursos;
    }

    public List<SqlRow> curso(Long id_curso){
        /*
        return Ebean.find(Curso.class)
                //.setDistinct(true)
                //.fetch("titulo")
                //.select("titulo")
                .where().in("idCurso",
                        Ebean.createQuery(EstudianteCurso.class)
                                .where().eq("idEstudiante", this.idEstudiante)
                                .select("idCurso")
                )
                .findList();
        */
        //SELECT idCurso, titulo from curso WHERE id_curso IN (SELECT id_curso FROM EstudianteCurso where id_estudiante = :id_estudiante)
        String cursosSql = "SELECT id_curso, titulo, (SELECT riesgo FROM RiesgoCurso WHERE id_estudiante = :id_estudiante ORDER BY riesgo DESC LIMIT 1) AS riesgo, (select CONCAT_WS(', ', CASE WHEN length(id_facebook)>0 THEN 'facebook' ELSE NULL END, CASE WHEN length(id_moodle)>0 THEN 'moodle' ELSE NULL END, CASE WHEN length(id_pam)>0 THEN 'pam' ELSE NULL END, CASE WHEN length(id_crea)>0 THEN 'crea' ELSE NULL END) from estudiante where id_estudiante = :id_estudiante) AS plataformas from curso WHERE id_curso IN (SELECT id_curso FROM EstudianteCurso where id_estudiante = :id_estudiante AND id_curso = :id_curso)";
        List<SqlRow> cursos = Ebean.createSqlQuery(cursosSql)
                .setParameter("id_estudiante", this.idEstudiante)
                .setParameter("id_curso", id_curso)
                .findList();
        return cursos;
    }

    public List<SqlRow> plataformas(){
        String interaccionesSql = "SELECT DISTINCT plataforma AS nombrePlataforma, MAX(timestamp) AS ultimaConexion FROM conexion WHERE id_estudiante = :id_estudiante GROUP BY plataforma;";
        List<SqlRow> plataformas = Ebean.createSqlQuery(interaccionesSql)
                .setParameter("id_estudiante", this.idEstudiante)
                .findList();
        return plataformas;
    }

    public static List<SqlRow> actividadesByCurso(Long id_estudiante, Long id_curso, String estado_actividad){
        String sql = "SELECT Actividad.*, Material.plataforma, Material.nombre, Material.tipo_contenido, MAterial.fecha_publicacion, Material.numero_accesos, Material.url_ubicacion from estudianteactividad RIGHT JOIN Material ON Material.id_material = EstudianteActividad.id_actividad RIGHT JOIN Actividad ON Actividad.id_actividad = EstudianteActividad.id_actividad WHERE EstudianteActividad.id_estudiante = :id_estudiante AND Material.id_curso = :id_curso AND estudianteactividad.estado = :estado_actividad;";

        if(estado_actividad.equals("*")){
            sql = "SELECT Actividad.*, Material.plataforma, Material.nombre, Material.tipo_contenido, MAterial.fecha_publicacion, Material.numero_accesos, Material.url_ubicacion from estudianteactividad RIGHT JOIN Material ON Material.id_material = EstudianteActividad.id_actividad RIGHT JOIN Actividad ON Actividad.id_actividad = EstudianteActividad.id_actividad WHERE EstudianteActividad.id_estudiante = :id_estudiante AND Material.id_curso = :id_curso;";
        }
        return Ebean.createSqlQuery(sql)
                .setParameter("id_estudiante", id_estudiante)
                .setParameter("id_curso", id_curso)
                .setParameter("estado_actividad", estado_actividad)
                .findList();
    }

    public List<SqlRow> materiales(){

        String interaccionesSql = "SELECT Material.* FROM Material WHERE id_material IN (SELECT id_material FROM MaterialEstudiante WHERE id_estudiante = :id_estudiante);";
        return Ebean.createSqlQuery(interaccionesSql).setParameter("id_estudiante", this.idEstudiante)
                .findList();
    }

    public List<SqlRow> intActividades(Long idCurso){
        String interaccionesSql = "SELECT subq.id_actividad AS id_actividad, material.nombre AS nombre, subq.timestamp AS timestamp, actividad.tipo AS tipo, subq.num_interacciones AS num_interacciones, Material.plataforma, (SELECT EstudianteActividad.estado FROM EstudianteActividad WHERE id_actividad = subq.id_actividad AND id_estudiante = :id_estudiante) AS estado, (SELECT EstudianteActividad.calificacion FROM EstudianteActividad WHERE id_actividad = subq.id_actividad AND id_estudiante = :id_estudiante) AS calificacion FROM (SELECT COUNT(interaccion.id_interaccion) AS num_interacciones, interaccion.nodo_destino AS id_actividad, MAX(interaccion.timestamp) AS timestamp FROM interaccion LEFT JOIN nodo nodod ON interaccion.nodo_destino = nodod.id_nodo WHERE nodod.tipo = :tipo_actividad AND interaccion.nodo_origen = :id_estudiante GROUP BY nodo_destino) subq LEFT JOIN actividad ON subq.id_actividad = actividad.id_actividad LEFT JOIN material ON subq.id_actividad = material.id_material";
            if(idCurso>0){
            interaccionesSql += " WHERE Material.id_curso = :id_curso";
        }
        interaccionesSql += " ORDER BY num_interacciones DESC;";
        List<SqlRow> intActividades = Ebean.createSqlQuery(interaccionesSql)
                .setParameter("id_curso", idCurso)
                .setParameter("id_estudiante", this.idEstudiante)
                .setParameter("tipo_actividad", String.valueOf(Actividad.tipoNodo))
                .findList();
        System.out.println("Query:"+interaccionesSql);
        return intActividades;
    }

    public List<SqlRow> actividades(String estado, Long id_curso){
        String sql = "SELECT Actividad.id_actividad AS id_actividad, Material.id_curso AS id_curso, Material.plataforma AS plataforma, Material.nombre AS nombre, Actividad.tipo AS tipo, Material.tipo_contenido AS tipo_contenido, Material.numero_accesos, Material.url_ubicacion AS url_ubicacion, (SELECT EstudianteActividad.estado FROM EstudianteActividad WHERE id_estudiante = :id_estudiante AND id_actividad = Actividad.id_actividad) AS estado, Actividad.tipo AS tipo, Actividad.fecha_limite AS fecha_limite, Actividad.calificacion_minima AS calificacion_minima, Actividad.calificacion_maxima AS calificacion_maxima FROM Material RIGHT JOIN Actividad ON Actividad.id_actividad = Material.id_material WHERE id_actividad IN (SELECT id_actividad FROM EstudianteActividad WHERE id_estudiante = :id_estudiante";
        if(!estado.equals("*")){
            sql += " AND EstudianteActividad.estado='"+estado+"'";
        }
        sql += ")";
        if(id_curso != null){
            sql += " AND Material.id_curso="+id_curso+"";
        }
        return Ebean.createSqlQuery(sql)
                .setParameter("id_estudiante", this.idEstudiante)
                .findList();
    }

    public List<SqlRow> estudianteCurso(Long id_curso){
        String sql = "SELECT * FROM EstudianteCurso WHERE id_estudiante = :id_estudiante AND id_curso = :id_curso";

        return Ebean.createSqlQuery(sql)
                .setParameter("id_estudiante", this.idEstudiante)
                .setParameter("id_curso", id_curso)
                .findList();
    }

    public Long getIdEstudiante() {
        return idEstudiante;
    }

    public String getNombre() {
        return nombre;
    }

    public String getApellidos() {
        return apellidos;
    }

    public Date getFechaNacimiento() {
        return fechaNacimiento;
    }

    public char getSexo() {
        return sexo;
    }

    public float getPromedioVigente() {
        return promedioVigente;
    }

    public float getPromedioAcumulado() {
        return promedioAcumulado;
    }

    public String getIdFacebook() {
        return idFacebook;
    }

    public String getIdPam() {
        return idPam;
    }

    public String getIdCrea() {
        return idCrea;
    }

    public String getIdMoodle() {
        return idMoodle;
    }

    public String getCorreo() {
        return correo;
    }

    public String getTelefono() {
        return telefono;
    }

    public String getInstitucion() {
        return institucion;
    }

    public String nombreCompleto(){
        return this.getNombre() + " " + this.getApellidos();
    }

    public long edad(){
        return yearsSince(getFechaNacimiento());
    }

    public String getCiudad_uno() {
        return ciudad_uno;
    }

    public String getDomicilio_uno() {
        return domicilio_uno;
    }

    public String getCiudad_dos() {
        return ciudad_dos;
    }

    public String getDomicilio_dos() {
        return domicilio_dos;
    }

    public String getLocalidad_uno() {
        return localidad_uno;
    }

    public String getLocalidad_dos() {
        return localidad_dos;
    }

    public String getCedula() {
        return cedula;
    }

    public static int yearsSince(Date dateLater) {
        Calendar cal = Calendar.getInstance();
        cal.setTime(dateLater);
        return Math.abs(
                LocalDate.now().getYear() - cal.get(Calendar.YEAR)
        );
    }

    public List<SqlRow> riesgos(Long id_curso){
        String riesgosSql = "SELECT id_curso, riesgo FROM RiesgoCurso WHERE id_estudiante = :id_estudiante";
        if(id_curso>=0){
            riesgosSql += " AND id_curso=:id_curso";
        }
        List<SqlRow> riesgos = Ebean.createSqlQuery(riesgosSql)
                .setParameter("id_estudiante", this.idEstudiante)
                .setParameter("id_curso", id_curso)
                .findList();
        return riesgos;
    }

    public static void nuevoEstudiante(String cedula, String nombre , String apellidos , String fecha_nacimiento , String sexo , Double promedio_vigente , Double promedio_acumulado, String correo , String telefono , String institucion , String id_facebook, String id_pam, String id_crea, String id_moodle, String ciudad_uno, String localidad_uno, String domicilio_uno, String ciudad_dos, String localidad_dos, String domicilio_dos){
        //Crear nodo
        SqlUpdate sql = Ebean.createSqlUpdate("insert into nodo (tipo) values (:tipo);");
        sql.setParameter("tipo", "e");
        sql.execute();

        //Obtener ultimo id
        List<SqlRow> ultimoId = Ebean.createSqlQuery("select max(id_nodo) as id from nodo;").findList();
        Integer nuevoId = ultimoId.get(0).getInteger("id");

        //Crear estudiante
        sql = Ebean.createSqlUpdate("insert into estudiante (id_estudiante, cedula, nombre, apellidos, fecha_nacimiento, sexo, promedio_vigente, promedio_acumulado, id_facebook, id_pam, id_crea, id_moodle, correo, telefono, institucion, ciudad_uno, localidad_uno, domicilio_uno, ciudad_dos, localidad_dos, domicilio_dos) values (:id_estudiante, :cedula, :nombre, :apellidos, :fecha_nacimiento, :sexo, :promedio_vigente, :promedio_acumulado, :id_facebook, :id_pam, :id_crea, :id_moodle, :correo, :telefono, :institucion, :ciudad_uno, :localidad_uno, :domicilio_uno, :ciudad_dos, :localidad_dos, :domicilio_dos);");
        sql.setParameter("id_estudiante", nuevoId);
        sql.setParameter("cedula", cedula);
        sql.setParameter("nombre", nombre);
        sql.setParameter("apellidos", apellidos);
        sql.setParameter("fecha_nacimiento", fecha_nacimiento);
        sql.setParameter("sexo", sexo);
        sql.setParameter("promedio_vigente", promedio_vigente);
        sql.setParameter("promedio_acumulado", promedio_acumulado);
        sql.setParameter("id_facebook", id_facebook);
        sql.setParameter("id_pam", id_pam);
        sql.setParameter("id_crea", id_crea);
        sql.setParameter("id_moodle", id_moodle);
        sql.setParameter("correo", correo);
        sql.setParameter("telefono", telefono);
        sql.setParameter("institucion", institucion);
        sql.setParameter("ciudad_uno", ciudad_uno);
        sql.setParameter("localidad_uno", localidad_uno);
        sql.setParameter("domicilio_uno", domicilio_uno);
        sql.setParameter("ciudad_dos", ciudad_dos);
        sql.setParameter("localidad_dos", localidad_dos);
        sql.setParameter("domicilio_dos", domicilio_dos);
        sql.execute();
    }


    public static void actualizarEstudiante(Long id_estudiante, String cedula, String nombre , String apellidos , String fecha_nacimiento , String sexo , Double promedio_vigente , Double promedio_acumulado, String correo , String telefono , String institucion , String id_facebook, String id_pam, String id_crea, String id_moodle, String ciudad_uno, String localidad_uno, String domicilio_uno, String ciudad_dos, String localidad_dos, String domicilio_dos){
        String query = "update estudiante set cedula=:cedula, nombre=:nombre, apellidos=:apellidos, fecha_nacimiento=:fecha_nacimiento, sexo=:sexo, promedio_vigente=:promedio_vigente promedio_acumulado=:promedio_acumulado, correo=:correo, telefono=:telefono, institucion=:institucion, id_facebook=:id_facebook, id_pam=:id_pam, id_crea=:id_crea, id_moodle=:id_moodle, ciudad_uno=:ciudad_uno, localidad_uno=:localidad_uno, domicilio_uno=:domicilio_uno, ciudad_dos=:ciudad_dos, localidad_dos=:localidad_dos, domicilio_dos=:domicilio_dos where id_estudiante=:id_estudiante;";
        Ebean.createSqlUpdate(query).
                setParameter("id_estudiante", id_estudiante).
                setParameter("cedula", cedula).
                setParameter("nombre", nombre).
                setParameter("apellidos", apellidos).
                setParameter("fecha_nacimiento", fecha_nacimiento).
                setParameter("sexo", sexo).
                setParameter("promedio_vigente", promedio_vigente).
                setParameter("promedio_acumulado", promedio_acumulado).
                setParameter("correo", correo).
                setParameter("telefono", telefono).
                setParameter("institucion", institucion).
                setParameter("id_facebook", id_facebook).
                setParameter("id_pam", id_pam).
                setParameter("id_crea", id_crea).
                setParameter("id_moodle", id_moodle).
                setParameter("ciudad_uno", ciudad_uno).
                setParameter("localidad_uno", localidad_uno).
                setParameter("domicilio_uno", domicilio_uno).
                setParameter("ciudad_dos", ciudad_dos).
                setParameter("localidad_dos", localidad_dos).
                setParameter("domicilio_dos", domicilio_dos)
                .execute();
    }

    public static void eliminarEstudiante(Integer id){
        String query = "delete from estudiante where id_estudiante=:id_estudiante;";
        Ebean.createSqlUpdate(query).
                setParameter("id_estudiante", id).
                execute();
    }

    /**
     * Charts
     */

    /**
     * Lista de actividades que fueron entregadas antes de la fecha límite en un curso especifico.
     * @return
     */
    public List<SqlRow> actsEntreAntFL(Long idCurso){
        String sql = "SELECT Material.nombre, Actividad.fecha_limite, EstudianteActividad.fecha_entrega FROM EstudianteActividad INNER JOIN Actividad ON Actividad.id_actividad = EstudianteActividad.id_actividad INNER JOIN Material ON Material.id_material = EstudianteActividad.id_actividad WHERE EstudianteActividad.id_estudiante = :id_estudiante AND Material.id_curso = :id_curso AND EstudianteActividad.estado = 'fi' AND EstudianteActividad.fecha_entrega <= Actividad.fecha_limite";
        return Ebean.createSqlQuery(sql)
                .setParameter("id_estudiante", this.idEstudiante)
                .setParameter("id_curso", idCurso)
                .findList();
    }

    /**
     * Lista de actividades que fueron entregadas despues de la fecha límite en un curso especifico.
     * @return
     */
    public List<SqlRow> actsEntreDesFL(Long idCurso){
        String sql = "SELECT Material.nombre, Actividad.fecha_limite, EstudianteActividad.fecha_entrega FROM EstudianteActividad INNER JOIN Actividad ON Actividad.id_actividad = EstudianteActividad.id_actividad INNER JOIN Material ON Material.id_material = EstudianteActividad.id_actividad WHERE EstudianteActividad.id_estudiante = :id_estudiante AND Material.id_curso = :id_curso AND EstudianteActividad.estado = 'fi' AND EstudianteActividad.fecha_entrega > Actividad.fecha_limite";
        return Ebean.createSqlQuery(sql)
                .setParameter("id_estudiante", this.idEstudiante)
                .setParameter("id_curso", idCurso)
                .findList();
    }

    /**
     * Lista de actividades de un curso especifico que no fueron entregadas y ya ha pasado la fecha limite.
     * @return
     */
    public List<SqlRow> actsNoEntre(Long idCurso){
        String sql = "SELECT Material.nombre, Actividad.fecha_limite, EstudianteActividad.estado FROM EstudianteActividad INNER JOIN Actividad ON Actividad.id_actividad = EstudianteActividad.id_actividad INNER JOIN Material ON Material.id_material = EstudianteActividad.id_actividad WHERE EstudianteActividad.id_estudiante = :id_estudiante AND Material.id_curso = :id_curso AND EstudianteActividad.estado != 'fi' AND now() > Actividad.fecha_limite";
        return Ebean.createSqlQuery(sql)
                .setParameter("id_estudiante", this.idEstudiante)
                .setParameter("id_curso", idCurso)
                .findList();
    }

    /**
     * Lista de actividades de un curso especifico que no han sido entregadas y que tienen una fecha limite vigente.
     * @return
     */
    public List<SqlRow> actsNoEntreVig(Long idCurso){
        String sql = "SELECT Material.nombre, Actividad.fecha_limite, EstudianteActividad.estado FROM EstudianteActividad INNER JOIN Actividad ON Actividad.id_actividad = EstudianteActividad.id_actividad INNER JOIN Material ON Material.id_material = EstudianteActividad.id_actividad WHERE EstudianteActividad.id_estudiante = :id_estudiante AND Material.id_curso = :id_curso AND EstudianteActividad.estado != 'fi' AND now() <= Actividad.fecha_limite";
        return Ebean.createSqlQuery(sql)
                .setParameter("id_estudiante", this.idEstudiante)
                .setParameter("id_curso", idCurso)
                .findList();
    }
}
