package models;

import com.avaje.ebean.Ebean;
import com.avaje.ebean.RawSql;
import com.avaje.ebean.SqlRow;
import com.avaje.ebean.SqlUpdate;
import org.jdbcdslog.SlowQueryLogger;
import scala.Char;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;
import javax.validation.constraints.Size;
import java.util.Date;
import java.util.List;

@Entity
public class Actividad extends play.db.ebean.Model{
    public static char tipoNodo = 'a';
    public static String tipoMaterial = "ac";

    @Id
    private Long idActividad;
    @Size(max = 2)
    private String tipo;
    private Date fechaLimite;
    private float calificacionMinima;
    private float calificacionMaxima;

    /**
     * The EBean ORM finder method for database queries.
     * @return The finder method for Escuela.
     */
    public static com.avaje.ebean.Model.Finder<Long, Actividad> find() {
        return new Finder<>(Actividad.class);
    }

    public SqlRow detalle(){
        String detalleSql = "select material.id_curso, material.plataforma, material.nombre, material.tipo_contenido, material.numero_accesos, material.url_ubicacion, actividad.fecha_limite, material.fecha_publicacion, actividad.tipo, actividad.calificacion_maxima, actividad.calificacion_minima from material INNER JOIN actividad ON material.id_material = actividad.id_actividad where material.id_material=:id_actividad";

        SqlRow coincidence = null;

        try {
            coincidence = Ebean.createSqlQuery(detalleSql).setParameter("id_actividad", this.idActividad)
                    .findUnique();
        }catch (Exception e){
            e.printStackTrace();
        }
        return coincidence;
    }

    public List<SqlRow> estudiantes(){
        String sql = "select estudianteactividad.id_estudiante as id_estudiante, concat(estudiante.nombre, ' ', estudiante.apellidos) as nombreCompleto, estudianteactividad.estado as estado, estudianteactividad.numero_intentos as numeroIntentos, estudianteactividad.fecha_entrega as fecha_entrega, estudianteactividad.calificacion as calificacion from estudianteactividad RIGHT JOIN estudiante ON estudianteactividad.id_estudiante = estudiante.id_estudiante where estudianteactividad.id_actividad=:id_actividad;";
        return Ebean.createSqlQuery(sql).setParameter("id_actividad", this.idActividad)
                .findList();
    }

    public List<SqlRow> resultados(){
        String sql = "SELECT EstudianteActividad.id_estudiante, concat(Estudiante.nombre, ' ', Estudiante.apellidos) as nombre, EstudianteActividad.calificacion AS nota, estudianteactividad.fecha_entrega FROM EstudianteActividad INNER JOIN estudiante ON estudiante.id_estudiante = estudianteactividad.id_estudiante WHERE EstudianteActividad.id_actividad = :id_actividad GROUP BY EstudianteActividad.id_estudiante, EstudianteActividad.id_actividad, Estudiante.nombre, Estudiante.apellidos";
        return Ebean.createSqlQuery(sql)
                .setParameter("id_actividad", this.idActividad)
                .findList();
    }


    /*
    public Long actividad() {
        return idActividad;
    }
    */

    public void setIdActividad(Long idActividad) {
        this.idActividad = idActividad;
    }

    public String getTipo() {
        return tipo;
    }

    public void setTipo(String tipo) {
        this.tipo = tipo;
    }

    public Date getFechaLimite() {
        return fechaLimite;
    }

    public void setFechaLimite(Date fechaLimite) {
        this.fechaLimite = fechaLimite;
    }

    public float getCalificacionMinima() {
        return calificacionMinima;
    }

    public void setCalificacionMinima(float calificacionMinima) {
        this.calificacionMinima = calificacionMinima;
    }

    public float getCalificacionMaxima() {
        return calificacionMaxima;
    }

    public void setCalificacionMaxima(float calificacionMaxima) {
        this.calificacionMaxima = calificacionMaxima;
    }

    public static void nuevo(String tipo, String fecha_limite, Double calificacion_minima, Double calificacion_maxima){
        SqlUpdate sql = Ebean.createSqlUpdate("insert into actividad (tipo, fecha_limite, calificacion_minima, calificacion_maxima) values (:tipo, :fecha_limite::timestamp, :calificacion_minima, :calificacion_maxima);");
        sql.setParameter("tipo", tipo);
        sql.setParameter("fecha_limite", fecha_limite);
        sql.setParameter("calificacion_minima", calificacion_minima);
        sql.setParameter("calificacion_maxima", calificacion_maxima);
        sql.execute();
    }

    public static void actualizar(Long id_actividad, String tipo, String fecha_limite, Double calificacion_minima, Double calificacion_maxima){
        String query = "update actividad set tipo=:tipo, fecha_limite=:fecha_limite, calificacion_minima=:calificacion_minima, calificacion_maxima=:calificacion_maxima where id_actividad=:id_actividad;";
        Ebean.createSqlUpdate(query).
                setParameter("id_actividad", id_actividad).
                setParameter("tipo", tipo).
                setParameter("fecha_limite", fecha_limite).
                setParameter("calificacion_minima", calificacion_minima).
                setParameter("calificacion_maxima", calificacion_maxima).
                execute();
    }

    public static void eliminar(Long id){
        String query = "delete from actividad where id_actividad=:id_actividad;";
        Ebean.createSqlUpdate(query).
                setParameter("id_actividad", id).
                execute();
    }
}
