package models;

import com.avaje.ebean.Ebean;
import com.avaje.ebean.Model;
import com.avaje.ebean.SqlRow;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import java.util.List;

@Entity
public class Programa extends play.db.ebean.Model{
    @Id
    @Column(name = "id_programa")
    private Long idPrograma;
    private Long idEscuela;
    private String nombre;

    /**
     * The EBean ORM finder method for database queries.
     * @return The finder method for Escuela.
     */
    public static com.avaje.ebean.Model.Finder<Long, Programa> find() {
        return new Model.Finder<>(Programa.class);
    }

    public Long getIdPrograma() {
        return idPrograma;
    }

    public Long getIdEscuela() {
        return idEscuela;
    }

    public String getNombre() {
        return nombre;
    }

    public List<SqlRow> cursos(){
        String sqlCursos = "SELECT Curso.*, (SELECT COUNT(EstudianteCurso.*) FROM EstudianteCurso WHERE id_curso = Curso.id_curso) AS contador_estudiante, (SELECT COUNT(Material.*) FROM Material WHERE id_curso = Curso.id_curso) AS contador_recurso, (SELECT SUM(Calificacion.calificacion)/COUNT(Calificacion.*) FROM Calificacion WHERE  id_curso = Curso.id_curso) AS promedio_general FROM Curso WHERE id_curso IN (SELECT id_curso FROM programacurso WHERE id_programa = :id_programa);";
        return Ebean.createSqlQuery(sqlCursos)
                .setParameter("id_programa", this.idPrograma)
                .findList();
    }

    public static void nuevoPrograma(Long id_escuela, String nombre){
        String query = "insert into programa (id_escuela, nombre) values (:id_escuela, :nombre);";
        Ebean.createSqlUpdate(query).
                setParameter("id_escuela", id_escuela).
                setParameter("nombre", nombre).
                execute();
    }


    public static void actualizarPrograma(Long id_programa, Long id_escuela, String nombre){
        String query = "update programa set nombre=:nombre, id_escuela=:id_escuela where id_programa=:id_programa;";
        Ebean.createSqlUpdate(query).
                setParameter("id_programa", id_programa).
                setParameter("id_escuela", id_escuela).
                setParameter("nombre", nombre)
                .execute();
    }

    public static void eliminarPrograma(Long id_programa){
        String query = "delete from programa where id_programa=:id_programa;";
        Ebean.createSqlUpdate(query).
                setParameter("id_programa", id_programa).
                execute();
    }
}
