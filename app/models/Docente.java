package models;

import com.avaje.ebean.*;
import javax.persistence.*;
import javax.swing.event.DocumentEvent;
import javax.validation.constraints.Size;
import java.awt.*;
import java.util.*;
import java.util.List;

@Entity
public class Docente extends play.db.ebean.Model {
    public static char tipoNodo = 'd';
    @Id
    @Column(name = "id_docente")
    private Long idDocente;
    @Size(max = 150)
    private String nombre;
    @Size(max = 150)
    private String apellidos;
    private char sexo;
    private Date fechaNacimiento;
    @Size(max = 100)
    private String correo;
    @Size(max = 30)
    private String area;
    @Size(max = 20)
    private String idFacebook;
    @Size(max = 20)
    private String idPam;
    @Size(max = 20)
    private String idCrea;
    @Size(max = 20)
    private String idMoodle;


    /**
     * The EBean ORM finder method for database queries.
     * @return The finder method for Escuela.
     */
    public static com.avaje.ebean.Model.Finder<Long, Docente> find() {
        return new Finder<>(Docente.class);
    }

    public SqlRow detalle(){
        String sql = "SELECT Docente.* FROM Docente WHERE id_docente = :id_docente";
        return Ebean.createSqlQuery(sql)
                .setParameter("id_docente", this.idDocente)
                .findUnique();
    }

    public List<SqlRow> cursos(){
        String cursosSql = "SELECT * FROM Curso where id_curso IN (select id_curso from CursoDocente where id_docente=:id_docente) AND id_curso = 3";
        return Ebean.createSqlQuery(cursosSql)
                .setParameter("id_docente", this.idDocente)
                .findList();
    }

    public List<SqlRow> estudiantes(){
        String cursosSql = "SELECT Estudiante.*, (SELECT string_agg(CAST(sub.id_curso AS VARCHAR), ',') FROM (SELECT id_curso, id_estudiante FROM EstudianteCurso WHERE EstudianteCurso.id_curso IN (SELECT id_curso FROM CursoDocente WHERE id_docente = :id_docente)) sub GROUP BY sub.id_estudiante HAVING sub.id_estudiante = Estudiante.id_estudiante) AS cursos FROM Estudiante WHERE id_estudiante IN (SELECT id_estudiante FROM EstudianteCurso WHERE id_curso IN (SELECT id_curso FROM CursoDocente WHERE id_docente = :id_docente) );";
        return Ebean.createSqlQuery(cursosSql)
                .setParameter("id_docente", this.idDocente)
                .findList();
    }

    public List<SqlRow> recursos(Long id_curso){
        String interaccionesSql = "SELECT Material.id_material, Material.plataforma, Material.nombre, Material.tipo, Material.tipo_contenido, Material.fecha_publicacion, Material.numero_accesos, Material.url_ubicacion  FROM Material WHERE id_material NOT IN (SELECT id_material FROM MaterialEstudiante) AND";
        if(id_curso != null){
            interaccionesSql += " id_curso = :id_curso;";
        }else{
            interaccionesSql += " id_curso IN (SELECT id_curso FROM CursoDocente WHERE id_docente = :id_docente);";
        }
        return Ebean.createSqlQuery(interaccionesSql)
                .setParameter("id_curso", id_curso)
                .setParameter("id_docente", this.idDocente)
                .findList();
    }

    public List<SqlRow> allNodo(Long id_curso){
        String actividadesSQL = "SELECT nodo.id_nodo AS id, nodo.tipo, material.nombre, material.id_curso, material.fecha_publicacion AS publicado, (SELECT COUNT(*) FROM Interaccion WHERE nodo_destino = nodo.id_nodo) AS cantInter, (SELECT coalesce(avg(calificacion), 0) from estudianteactividad where id_actividad = nodo.id_nodo) AS notaProm, material.plataforma AS plat, material.tipo_contenido AS cont from nodo left join material ON material.id_material = nodo.id_nodo left join actividad ON actividad.id_actividad = nodo.id_nodo where material.id_curso in (select id_curso from cursodocente where id_docente = :id_docente) and nodo.tipo = :tipo_actividad";
        String materialesSql= "SELECT nodo.id_nodo AS id, nodo.tipo, material.nombre, material.id_curso, material.fecha_publicacion AS public, (SELECT COUNT(*) FROM Interaccion WHERE nodo_destino = nodo.id_nodo) AS cantInter, material.plataforma AS plat, material.tipo_contenido AS cont from nodo left join material ON material.id_material = nodo.id_nodo where material.id_curso in (select id_curso from cursodocente where id_docente = :id_docente) and nodo.tipo = :tipo_material";
        String estudiantesSql = "SELECT nodo.id_nodo AS id, nodo.tipo, Concat(estudiante.nombre, ' ', estudiante.apellidos) AS nombre, String_agg(Cast(estudiantecurso.id_curso AS VARCHAR), ',') AS cursos, (SELECT coalesce(avg(calificacion), 0) from estudianteactividad where id_estudiante = nodo.id_nodo) AS notaProm, (SELECT COUNT(*) FROM conexion WHERE conexion.id_estudiante = nodo.id_nodo) AS cantInter, (SELECT timestamp FROM conexion WHERE conexion.id_estudiante = nodo.id_nodo ORDER BY conexion.timestamp DESC LIMIT 1) AS ultimaInter FROM nodo LEFT JOIN estudiante ON estudiante.id_estudiante = nodo.id_nodo LEFT JOIN estudiantecurso ON estudiante.id_estudiante = estudiantecurso.id_estudiante WHERE estudiantecurso.id_curso IN (SELECT id_curso FROM cursodocente WHERE id_docente = :id_docente) AND nodo.tipo = :tipo_estudiante GROUP BY nodo.id_nodo, estudiante.id_estudiante";
        String docenteSql = "SELECT nodo.id_nodo AS id, nodo.tipo, Concat(docente.nombre, ' ', docente.apellidos) AS nombre, String_agg(Cast(cursodocente.id_curso AS VARCHAR), ',') AS cursos FROM nodo LEFT JOIN docente ON docente.id_docente = nodo.id_nodo LEFT JOIN cursodocente ON cursodocente.id_docente = nodo.id_nodo WHERE id_nodo = :id_docente GROUP BY nodo.id_nodo, docente.id_docente";
        if(id_curso != null){
            materialesSql += " AND material.id_curso = :id_curso";
            actividadesSQL += " and material.id_curso = :id_curso";
            estudiantesSql += " HAVING COUNT(case when estudiantecurso.id_curso  = :id_curso then 0 end) > 0";
        }

        List<SqlRow> nodos = Ebean.createSqlQuery(materialesSql)
                .setParameter("tipo_material", String.valueOf(Material.tipoNodo))
                .setParameter("id_docente", this.idDocente)
                .setParameter("id_curso", id_curso)
                .findList();
        nodos.addAll(
                Ebean.createSqlQuery(actividadesSQL)
                        .setParameter("tipo_actividad", String.valueOf(Actividad.tipoNodo))
                        .setParameter("id_docente", this.idDocente)
                        .setParameter("id_curso", id_curso)
                        .findList()
        );
        nodos.addAll(
                Ebean.createSqlQuery(estudiantesSql)
                        .setParameter("id_docente", this.idDocente)
                        .setParameter("tipo_estudiante", String.valueOf(Estudiante.tipoNodo))
                        .setParameter("id_curso", id_curso)
                        .findList()
        );
        nodos.addAll(
                Ebean.createSqlQuery(docenteSql)
                        .setParameter("id_docente", this.idDocente)
                        .findList()
        );
        return nodos;
    }

    public List<SqlRow> allInteraccion(Long id_curso){
        String interaccionesSql = "select id_interaccion, plataforma, timestamp as fecha, nodo_origen as origen, no.tipo as origen_tipo, nodo_destino as destino, nd.tipo as destino_tipo, id_curso_destino as curso, tipo_interaccion as tipo, sentimiento as polaridad from interaccion LEFT JOIN nodo AS no ON interaccion.nodo_origen=no.id_nodo LEFT JOIN nodo AS nd ON interaccion.nodo_destino=nd.id_nodo where interaccion.id_curso_destino in (select cursodocente.id_curso from cursodocente where id_docente=:id_docente)";
        if(id_curso != null){
            interaccionesSql += " AND (id_curso_origen = :id_curso OR id_curso_destino = :id_curso)";
        }
        List<SqlRow> interacciones = Ebean.createSqlQuery(interaccionesSql)
                .setParameter("id_docente", this.idDocente)
                .setParameter("id_curso", id_curso)
                .findList();
        return interacciones;
    }


    /**
     * Lista de interacciones entre estudiantes de un curso
     * @return
     */
    public List<SqlRow> interacciones(Long idCursoOrigen, List<String> interactions, List<String> intTypes, String fecha_inicio, String fecha_fin, List<Long> ids){
        String interaccionesSql = "SELECT interaccion.id_interaccion AS id_interaccion, interaccion.contenido, interaccion.tipo_interaccion, interaccion.nodo_origen AS nodo_origen, n.tipo AS tipo_origen, interaccion.nodo_destino AS nodo_destino, nn.tipo AS tipo_destino FROM interaccion LEFT JOIN nodo n ON interaccion.nodo_origen = n.id_nodo LEFT JOIN nodo nn ON interaccion.nodo_destino = nn.id_nodo WHERE";
        if(idCursoOrigen!=null && idCursoOrigen > 0){
            interaccionesSql += " interaccion.id_curso_origen = :id_curso";
        }else{
            interaccionesSql += " interaccion.id_curso_origen IN (select cursodocente.id_curso from cursodocente where id_docente=:id_docente)";
        }

        if(ids != null && ids.size()>0){
            interaccionesSql += " AND (interaccion.nodo_origen IN (:ids) AND interaccion.nodo_destino IN (:ids))";
        }
        //May be here can be added the "in" clausule if the List isn't empty

        //Add interactions
        //AND n.tipo = :tipo_estudiante AND nn.tipo = :tipo_estudiante
        if(interactions!=null && interactions.size()>0 && !(interactions.size()==1 && interactions.get(0).equals("**")) ){
            interaccionesSql += " AND (";
            for(int x=0;x < interactions.size() ;x++){
                char nodoOri = interactions.get(x).charAt(0);
                char nodoDst = interactions.get(x).charAt(1);

                interaccionesSql += " (";

                if(nodoOri != '*'){
                    interaccionesSql += " n.tipo = '" + nodoOri +"'";
                }
                if(nodoOri != '*' && nodoDst != '*'){
                    interaccionesSql += " AND";
                }
                if(nodoDst != '*'){
                    interaccionesSql += " nn.tipo = '" + nodoDst +"'";
                }
                interaccionesSql += " )";

                if( x != (interactions.size()-1) ){
                    interaccionesSql += " OR";
                }
            }
            interaccionesSql += " )";
        }

        //Add interaction types
        if( intTypes.size()>0 && !intTypes.contains("*") ){
            interaccionesSql += " AND (";
            for(int x=0;x < intTypes.size() ;x++){
                interaccionesSql += " interaccion.tipo_interaccion = '" + intTypes.get(x)+"'";
                if( x != intTypes.size()-1 ){
                    interaccionesSql += " OR";
                }
            }
            interaccionesSql += " )";
        }

        //Date
        if( (fecha_inicio != null && fecha_inicio.length()>2) && (fecha_fin != null && fecha_fin.length()>2) ){ // ! *
            interaccionesSql += " AND timestamp >= '"+fecha_inicio+" 00:00:00' AND timestamp <= '"+fecha_fin+" 00:00:00'";
        }


        System.out.println("Query:"+interaccionesSql);
        List<SqlRow> interacciones = Ebean.createSqlQuery(interaccionesSql)
                .setParameter("id_curso", idCursoOrigen)
                .setParameter("tipo_estudiante", String.valueOf(Estudiante.tipoNodo))
                .setParameter("ids", ids)
                .setParameter("id_docente", this.idDocente)
                .findList();
        return interacciones;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getApellidos() {
        return apellidos;
    }

    public void setApellidos(String apellidos) {
        this.apellidos = apellidos;
    }

    public char getSexo() {
        return sexo;
    }

    public void setSexo(char sexo) {
        this.sexo = sexo;
    }

    public Long getIdDocente() {
        return idDocente;
    }

    public void setIdDocente(Long idDocente) {
        this.idDocente = idDocente;
    }

    public Date getFechaNacimiento() {
        return fechaNacimiento;
    }

    public void setFechaNacimiento(Date fechaNacimiento) {
        this.fechaNacimiento = fechaNacimiento;
    }

    public String getIdFacebook() {
        return idFacebook;
    }

    public void setIdFacebook(String idFacebook) {
        this.idFacebook = idFacebook;
    }

    public String getIdPam() {
        return idPam;
    }

    public void setIdPam(String idPam) {
        this.idPam = idPam;
    }

    public String getIdCrea() {
        return idCrea;
    }

    public void setIdCrea(String idCrea) {
        this.idCrea = idCrea;
    }

    public String getIdMoodle() {
        return idMoodle;
    }

    public void setIdMoodle(String idMoodle) {
        this.idMoodle = idMoodle;
    }

    public String getCorreo() {
        return correo;
    }

    public String getArea() {
        return area;
    }

    public static void nuevo(String nombre, String apellidos, String sexo, String fecha_nacimiento, String correo, String area, String id_facebook, String id_pam, String id_crea, String id_moodle){
        String query = "insert into docente (id_docente, nombre, apellidos, sexo, fecha_nacimiento, id_facebook, id_pam, id_crea, id_moodle, correo, area) values (:id_docente, :nombre, :apellidos, :sexo, :fecha_nacimiento::date, :id_facebook, :id_pam, :id_crea, :id_moodle, :correo, :area);";
        Ebean.createSqlUpdate(query).
                setParameter("nombre", nombre).
                setParameter("apellidos", apellidos).
                setParameter("sexo", sexo).
                setParameter("fecha_nacimiento", fecha_nacimiento).
                setParameter("id_facebook", id_facebook).
                setParameter("id_pam", id_pam).
                setParameter("id_crea", id_crea).
                setParameter("id_moodle", id_moodle).
                setParameter("correo", correo).
                setParameter("area", area).
                execute();
    }


    public static void actualizar(Long id_docente, String nombre, String apellidos, String sexo, String fecha_nacimiento, String correo, String area, String id_facebook, String id_pam, String id_crea, String id_moodle){
        String query = "update docente set nombre=:nombre, apellidos=:apellidos, sexo=:sexo, fecha_nacimiento=:fecha_nacimiento, id_facebook=:id_facebook, id_pam=:id_pam, id_crea=:id_crea, id_moodle=:id_moodle, correo=:correo, area=:area where id_docente=:id_docente;";
        Ebean.createSqlUpdate(query).
                setParameter("id_docente", id_docente).
                setParameter("nombre", nombre).
                setParameter("apellidos", apellidos).
                setParameter("sexo", sexo).
                setParameter("fecha_nacimiento", fecha_nacimiento).
                setParameter("id_facebook", id_facebook).
                setParameter("id_pam", id_pam).
                setParameter("id_crea", id_crea).
                setParameter("id_moodle", id_moodle).
                setParameter("correo", correo).
                setParameter("area", area).
                execute();
    }

    public static void eliminar(Long id){
        String query = "delete from docente where id_docente=:id_docente;";
        Ebean.createSqlUpdate(query).
                setParameter("id_docente", id).
                execute();
    }
}
