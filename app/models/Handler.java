package models;

import com.avaje.ebean.Ebean;
import com.avaje.ebean.SqlRow;
import play.db.ebean.Model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import java.util.List;

@Entity
//@Embeddable
//@Embeddable
public class Handler extends Model {

    public static void nuevo(Integer hi, String h, Integer rti, Integer ri){
        String query = "insert into handle(handle_id, handle, resource_type_id, resource_id) values (:hi, :h, :rti, :ri);";
        Ebean.createSqlUpdate(query).
                setParameter("hi", hi).
                setParameter("h", h).
                setParameter("rti", rti).
                setParameter("ri", ri).
                execute();
    }

    public static List<SqlRow> view1(){
        String detalleSql = "select * from view1;";
        return Ebean.createSqlQuery(detalleSql).
                findList();
    }

    public static List<SqlRow> view2(){
        String detalleSql = "select * from view2;";
        return Ebean.createSqlQuery(detalleSql).
                findList();
    }

    public static List<SqlRow> view3(){
        String detalleSql = "select * from view3;";
        return Ebean.createSqlQuery(detalleSql).
                findList();
    }

    public static List<SqlRow> view4(){
        String detalleSql = "select * from view4;";
        return Ebean.createSqlQuery(detalleSql).
                findList();
    }

}
