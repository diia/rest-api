package models;

import com.avaje.ebean.Ebean;
import com.avaje.ebean.Model;

import javax.persistence.*;
import java.util.List;

@Entity
@Table(name="cursodocente")
public class CursoDocente {
    @Column(name = "id_curso")
    private Long idCurso;
    @Column(name = "id_docente")
    private Long idDocente;

    public static Model.Finder<Long, CursoDocente> find() {
        return new Model.Finder<>(CursoDocente.class);
    }

    public Long getIdCurso() {
        return idCurso;
    }

    public Long getIdDocente() {
        return idDocente;
    }
}
