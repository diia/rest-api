package models;

import com.avaje.ebean.Ebean;
import com.avaje.ebean.Model;
import com.avaje.ebean.SqlRow;

import javax.persistence.*;
import javax.validation.constraints.Size;
import java.util.List;

@Entity
public class Escuela extends play.db.ebean.Model {
    @Id
    @Column(name = "id_escuela")
    private Long idEscuela;
    @Size(max = 150)
    private String nombre;
    @Size(max = 150)
    private String direccion;
    @Size(max = 50)
    private String ciudad;
    @Size(max = 50)
    private String region;
    @Size(max = 30)
    private String latitude;
    @Size(max = 30)
    private String longitude;

    /**
     * The EBean ORM finder method for database queries.
     * @return The finder method for Escuela.
     */
    public static com.avaje.ebean.Model.Finder<Long, Escuela> find() {
        return new Finder<>(Escuela.class);
    }

    public static List<Escuela> escuelas(){
        List<Escuela> list =
                Ebean.find(Escuela.class)
                        .findList();
        return list;
    }

    public List<SqlRow> programas(){
        String cursosSql = "SELECT Programa.*, (SELECT COUNT(EstudianteCurso.*) FROM EstudianteCurso WHERE id_curso IN (SELECT id_curso FROM Programa WHERE id_programa = Programa.id_programa)) AS contador_estudiante, (SELECT COUNT(Material.*) FROM Material WHERE id_curso IN (SELECT id_curso FROM Programa WHERE id_programa = Programa.id_programa)) AS contador_recurso, (SELECT SUM(Calificacion.calificacion)/COUNT(Calificacion.*) FROM Calificacion WHERE id_curso IN (SELECT id_curso FROM Programa WHERE id_programa = Programa.id_programa)) AS promedio_general FROM Programa WHERE id_escuela = :id_escuela;";
        return Ebean.createSqlQuery(cursosSql)
                .setParameter("id_escuela", this.idEscuela)
                .findList();
    }

    public static void nuevaEscuela(String nombre, String direccion, String ciudad, String region, Double latitud, Double longitud){
        String query = "insert into escuela (nombre, direccion, ciudad, region, latitude, longitude) values (:nombre, :direccion, :ciudad, :region, :latitud, :longitud);";
        Ebean.createSqlUpdate(query).
                setParameter("nombre", nombre).
                setParameter("direccion", direccion).
                setParameter("ciudad", ciudad).
                setParameter("region", region).
                setParameter("latitud", latitud).
                setParameter("longitud", longitud)
        .execute();
    }


    public static void actualizarEscuela(Integer id_escuela, String nombre, String direccion, String ciudad, String region, Double latitud, Double longitud){
        String query = "update escuela set nombre=:nombre, direccion=:direccion, ciudad=:ciudad, region=:region, latitude=:latitud, longitude=:longitud where id_escuela=:id_escuela;";
        Ebean.createSqlUpdate(query).
                setParameter("id_escuela", id_escuela).
                setParameter("nombre", nombre).
                setParameter("direccion", direccion).
                setParameter("ciudad", ciudad).
                setParameter("region", region).
                setParameter("latitud", latitud).
                setParameter("longitud", longitud)
                .execute();
    }

    public static void eliminarEscuela(Integer id_escuela){
        String query = "delete from escuela where id_escuela=:id_escuela;";
        Ebean.createSqlUpdate(query).
                setParameter("id_escuela", id_escuela).
                execute();
    }

    public Long getIdEscuela() {
        return idEscuela;
    }

    public void setIdEscuela(Long idEscuela) {
        this.idEscuela = idEscuela;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getDireccion() {
        return direccion;
    }

    public void setDireccion(String direccion) {
        this.direccion = direccion;
    }

    public String getCiudad() {
        return ciudad;
    }

    public void setCiudad(String ciudad) {
        this.ciudad = ciudad;
    }

    public String getRegion() {
        return region;
    }

    public void setRegion(String region) {
        this.region = region;
    }

    public void setLatitude(String latitude) {
        this.latitude = latitude;
    }

    public void setLongitude(String longitude) {
        this.longitude = longitude;
    }

    public String getLatitude() {
        return latitude;
    }

    public String getLongitude() {
        return longitude;
    }
}
