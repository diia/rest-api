package models;

import com.avaje.ebean.Ebean;
import com.avaje.ebean.SqlRow;

import javax.persistence.*;
import java.util.List;

@Entity
@Table(name="EscuelaDocente")
public class EscuelaDocente {
    private Long idDocente;
    private Long id_escuela;

    public static List<SqlRow> docentesByEscuela(Long id_escuela){
        return Ebean.createSqlQuery("SELECT Docente.* FROM EscuelaDocente LEFT JOIN Docente ON EscuelaDocente.id_docente = Docente.id_docente WHERE id_escuela = :id_escuela")
                .setParameter("id_escuela", id_escuela)
                .findList();
    }

    public Long getIdDocente() {
        return idDocente;
    }

    public Long getId_escuela() {
        return id_escuela;
    }
}
