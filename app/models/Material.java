package models;

import com.avaje.ebean.Ebean;
import com.avaje.ebean.SqlRow;
import com.avaje.ebean.SqlUpdate;

import javax.persistence.Column;
import javax.persistence.Id;
import javax.persistence.Entity;
import javax.validation.constraints.Size;
import java.util.Date;
import java.util.List;

@Entity
public class Material extends play.db.ebean.Model{
    public static char tipoNodo = 'm';
    public static String tipoMaterial = "ri";

    @Id
    @Column(name = "id_material")
    private Long idMaterial;
    private Long idCurso;
    @Size(max = 2)
    private String plataforma;
    private String nombre;
    @Size(max = 2)
    private String tipo;
    @Size(max = 3)
    private String tipoContenido;
    private Date fechaPublicacion;
    private Integer numeroAccesos;
    private String urlUbicacion;

    public static com.avaje.ebean.Model.Finder<Long, Material> find() {
        return new Finder<>(Material.class);
    }

   /* public Long getIdMaterial() {
        return idMaterial;
    }
    */

    public void setIdMaterial(Long idMaterial) {
        this.idMaterial = idMaterial;
    }

    public String getPlataforma() {
        return plataforma;
    }

    public void setPlataforma(String plataforma) {
        this.plataforma = plataforma;
    }

    public Long getIdCurso() {
        return idCurso;
    }

    public void setIdCurso(Long idCurso) {
        this.idCurso = idCurso;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getTipo() {
        return tipo;
    }

    public void setTipo(String tipo) {
        this.tipo = tipo;
    }

    public String getTipoContenido() {
        return tipoContenido;
    }

    public void setTipoContenido(String tipoContenido) {
        this.tipoContenido = tipoContenido;
    }

    public Date getFechaPublicacion() {
        return fechaPublicacion;
    }

    public void setFechaPublicacion(Date fechaPublicacion) {
        this.fechaPublicacion = fechaPublicacion;
    }

    public Integer getNumeroAccesos() {
        return numeroAccesos;
    }

    public void setNumeroAccesos(Integer numeroAccesos) {
        this.numeroAccesos = numeroAccesos;
    }

    public String getUrlUbicacion() {
        return urlUbicacion;
    }

    public void setUrlUbicacion(String urlUbicacion) {
        this.urlUbicacion = urlUbicacion;
    }

    public SqlRow detalle(){
        String sql = "SELECT Material.* FROM Material WHERE id_material = :id_material AND tipo = 'ri'";
        return Ebean.createSqlQuery(sql)
                .setParameter("id_material", this.idMaterial)
                .findUnique();
    }

    public static void nuevo(Long id_curso, String plataforma, String nombre, String tipo, String tipo_contenido, String fecha_publicacion, Integer numero_accesos, String url_ubicacion){
        SqlUpdate sql = Ebean.createSqlUpdate("insert into material (plataforma, id_curso, nombre, tipo, tipo_contenido, fecha_publicacion, numero_accesos, url_ubicacion) values (:plataforma, :id_curso, :nombre, :tipo, :tipo_contenido, :fecha_publicacion::timestamp, :numero_accesos, :url_ubicacion);");
        sql.setParameter("plataforma", plataforma);
        sql.setParameter("id_curso", id_curso);
        sql.setParameter("nombre", nombre);
        sql.setParameter("tipo", tipo);
        sql.setParameter("tipo_contenido", tipo_contenido);
        sql.setParameter("fecha_publicacion", fecha_publicacion);
        sql.setParameter("numero_accesos", numero_accesos);
        sql.setParameter("url_ubicacion", url_ubicacion);
        sql.execute();
    }

    public static void actualizar(Long id_material, Long id_curso, String plataforma, String nombre, String tipo, String tipo_contenido, String fecha_publicacion, Integer numero_accesos, String url_ubicacion){
        String query = "update material set id_curso=:id_curso, plataforma=:plataforma, nombre=:nombre, tipo=:tipo, tipo_contenido=:tipo_contenido, fecha_publicacion=:fecha_publicacion, numero_accesos=:numero_accesos, url_ubicacion=:url_ubicacion where id_material=:id_material;";
        Ebean.createSqlUpdate(query).
                setParameter("id_material", id_material).
                setParameter("plataforma", plataforma).
                setParameter("id_curso", id_curso).
                setParameter("nombre", nombre).
                setParameter("tipo", tipo).
                setParameter("tipo_contenido", tipo_contenido).
                setParameter("fecha_publicacion", fecha_publicacion).
                setParameter("numero_accesos", numero_accesos).
                setParameter("url_ubicacion", url_ubicacion).
                execute();
    }

    public static void eliminar(Long id){
        String query = "delete from material where id_material=:id_material;";
        Ebean.createSqlUpdate(query).
                setParameter("id_material", id).
                execute();
    }
}
