package models;

import javax.persistence.*;
import javax.validation.Constraint;
import javax.validation.constraints.Size;

import com.avaje.ebean.Ebean;
import com.avaje.ebean.SqlRow;
import play.db.ebean.Model;

import java.util.List;

@Entity
//@Embeddable
//@Embeddable
public class Nodo extends Model {
    @Id
    @Column(name = "id_nodo")
    private Long idNodo;
    /*
        e = Estudiante
        m = Material
        d = Docente
     */
    private char tipo;
    /**
     * The EBean ORM finder method for database queries.
     * @return The finder method for Nodo.
     */
    public static com.avaje.ebean.Model.Finder<Long, Nodo> find() {
        return new Finder<>(Nodo.class);
    }

    public Long getIdNodo() {
        return idNodo;
    }

    public void setIdNodo(Long idNodo) {
        this.idNodo = idNodo;
    }

    public char getTipo() {
        return tipo;
    }

    public void setTipo(char tipo) {
        this.tipo = tipo;
    }

    /**
     * @return the timestamp associated with the page retrieval.
     */
    //public Long getTimestamp() {return timestamp;}

    /**
     * @param timestamp the timestamp to set
     */
    //public void setTimestamp(Long timestamp) {this.timestamp = timestamp;}

    /**
     * Interacciones que componen el grafo egocentrico de un nodo.
     * @return
     */
    public List<SqlRow> intEgoGraph(){
        String sql = "SELECT id_interaccion, nodo_origen AS origen, no.tipo AS origen_tipo, nodo_destino AS destino, nd.tipo AS destino_tipo, Interaccion.tipo_interaccion AS tipo FROM Interaccion INNER JOIN Nodo no ON Interaccion.nodo_origen = no.id_nodo INNER JOIN Nodo nd ON Interaccion.nodo_destino = nd.id_nodo WHERE nodo_origen = :id_nodo OR nodo_destino = :id_nodo";
        return Ebean.createSqlQuery(sql)
                .setParameter("id_nodo", this.idNodo)
                .findList();
    }

    /**
     * Nodos que componen el grafo egocentrico de un nodo.
     * @return
     */
    public List<SqlRow> nodosEgoGraph(){
        String sql = "SELECT subq.id_nodo AS id, subq.tipo, (CASE WHEN (subq.tipo = :estudiante_nodo_tipo) THEN Estudiante.nombre WHEN (subq.tipo = :material_nodo_tipo OR subq.tipo = :actividad_nodo_tipo) THEN Material.nombre WHEN (subq.tipo = :docente_nodo_tipo) THEN Docente.nombre END) AS nombre FROM (SELECT nodo_origen AS id_nodo, Nodo.tipo AS tipo FROM Interaccion INNER JOIN Nodo ON Nodo.id_nodo = Interaccion.nodo_origen WHERE nodo_origen = :id_nodo OR nodo_destino = :id_nodo UNION SELECT nodo_destino AS id_nodo, Nodo.tipo AS tipo FROM Interaccion INNER JOIN Nodo ON Nodo.id_nodo = Interaccion.nodo_destino WHERE nodo_origen = :id_nodo OR nodo_destino = :id_nodo) subq LEFT JOIN Material ON Material.id_material = subq.id_nodo LEFT JOIN Estudiante ON Estudiante.id_estudiante = subq.id_nodo LEFT JOIN Docente ON Docente.id_docente = subq.id_nodo";
        return Ebean.createSqlQuery(sql)
                .setParameter("estudiante_nodo_tipo", String.valueOf(Estudiante.tipoNodo))
                .setParameter("material_nodo_tipo", String.valueOf(Material.tipoNodo))
                .setParameter("actividad_nodo_tipo", String.valueOf(Actividad.tipoNodo))
                .setParameter("docente_nodo_tipo", String.valueOf(Docente.tipoNodo))
                .setParameter("id_nodo", this.idNodo)
                .findList();
    }

}
