package models;

import com.avaje.ebean.Model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;
import javax.validation.constraints.Size;

@Entity
@Table(name="estudiantecurso")
public class EstudianteCurso {
    @Column(name = "id_curso")
    private Long idCurso;
    @Column(name = "id_estudiante")
    private Long idEstudiante;
    @Size(max = 2)
    private String tipo;

    public static Model.Finder<Long, EstudianteCurso> find() {
        return new Model.Finder<>(EstudianteCurso.class);
    }

    public Long getIdCurso() {
        return idCurso;
    }

    public void setIdCurso(Long idCurso) {
        this.idCurso = idCurso;
    }

    public Long getIdEstudiante() {
        return idEstudiante;
    }

    public void setIdEstudiante(Long idEstudiante) {
        this.idEstudiante = idEstudiante;
    }

    public String getTipo() {
        return tipo;
    }

    public void setTipo(String tipo) {
        this.tipo = tipo;
    }
}
