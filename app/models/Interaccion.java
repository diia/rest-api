package models;

import com.avaje.ebean.Ebean;
import com.avaje.ebean.Model;
import com.avaje.ebean.SqlQuery;
import com.avaje.ebean.SqlRow;

import javax.persistence.*;
import javax.validation.constraints.Size;
import java.io.Serializable;
import java.sql.Timestamp;
import java.util.List;

@Entity
public class Interaccion extends play.db.ebean.Model{
    @Id @Column(name="id_interaccion")
    private Long id_interaccion;
    @OneToOne(targetEntity = Nodo.class)
    @Column(name = "nodo_origen")
    private Long nodo_origen;
    @OneToOne(targetEntity = Nodo.class)
    @Column(name = "nodo_destino")
    private Long nodo_destino;
    @Size(max = 3)
    @Column(name = "tipo_interaccion")
    private String tipo_interaccion;
    @Column(name = "id_curso_origen")
    private Long id_curso_origen;
    @Column(name = "id_curso_destino")
    private Long id_curso_destino;
    @Size(max = 3)
    private String tipoContenido;
    private String contenido;
    private Timestamp timestamp;
    @Size(max = 1)
    private String sentimiento;
    private String plataforma;
    private String id_origen;
    /**
     * The EBean ORM finder method for database queries.
     * @return The finder method for Escuela.
     */
    /*public static Finder<Long, Interaccion> find() {
        return new Finder<>(Interaccion.class);
    }*/
    public static Model.Finder<Long, Interaccion> find() {
        return new Model.Finder<>(Interaccion.class);
    }

    /**
     * Devuelve una lista de interaccions entre un par de nodos.
     * @param nodo_origen
     * @param nodo_destino
     * @param fecha_inicio
     * @param fecha_fin
     * @return
     */
    public static List<SqlRow> interaccionesOD(String nodo_origen, String nodo_destino, String fecha_inicio, String fecha_fin, List<String> intTypes){
        String sql = "SELECT Interaccion.id_interaccion, Interaccion.tipo_interaccion, Interaccion.id_curso_origen, Interaccion.id_curso_destino, Interaccion.tipo_contenido, Interaccion.contenido, Interaccion.timestamp, Interaccion.sentimiento, Interaccion.plataforma FROM Interaccion";// AND nodo_destino = :nodo_destino
        boolean and = false;
        if( !nodo_origen.equals("*") || !nodo_destino.equals("*") || !fecha_inicio.equals("*") || !fecha_fin.equals("*") ){
            sql += " WHERE";
        }
        if( !nodo_origen.equals("*") ){
            if(!and) and = !and; else sql += " AND";
            sql += " nodo_origen = :nodo_origen";
        }
        if ( !nodo_destino.equals("*") ){
            if(!and) and = !and; else sql += " AND";
            sql += " nodo_destino = :nodo_destino";
        }
        if( !fecha_inicio.equals("*") ){
            if(!and) and = !and; else sql += " AND";
            sql += " timestamp >= '" + fecha_inicio + " 00:00:00'";
        }
        if( !fecha_fin.equals("*") ){
            if(!and) and = !and; else sql += " AND";
            sql += " timestamp <= '" + fecha_fin + " 00:00:00'";
        }
        //Add interaction types
        if( intTypes.size()>0 && !intTypes.contains("*") ){
            sql += " AND (";
            for(int x=0;x < intTypes.size() ;x++){
                sql += " Interaccion.tipo_interaccion = '" + intTypes.get(x)+"'";
                if( x != intTypes.size()-1 ){
                    sql += " OR";
                }
            }
            sql += " )";
        }

        SqlQuery query = Ebean.createSqlQuery(sql);
        if( !nodo_origen.equals("*") ){
            query.setParameter("nodo_origen", Long.parseLong(nodo_origen));
        }
        if( !nodo_destino.equals("*") ){
            query.setParameter("nodo_destino", Long.parseLong(nodo_destino));
        }
        System.out.println(nodo_origen+":"+nodo_destino);
        System.out.println(query.toString());
        return query.findList();
    }

    public Long getId_interaccion() {
        return id_interaccion;
    }

    public void setId_interaccion(Long id_interaccion) {
        this.id_interaccion = id_interaccion;
    }

    public Long getNodo_origen() {
        return nodo_origen;
    }

    public void setNodo_origen(Long nodo_origen) {
        this.nodo_origen = nodo_origen;
    }

    public Long getNodo_destino() {
        return nodo_destino;
    }

    public void setNodo_destino(Long nodo_destino) {
        this.nodo_destino = nodo_destino;
    }

    public String getTipo_interaccion() {
        return tipo_interaccion;
    }

    public void setTipo_interaccion(String tipo_interaccion) {
        this.tipo_interaccion = tipo_interaccion;
    }

    public Long getId_curso_origen() {
        return id_curso_origen;
    }

    public void setId_curso_origen(Long id_curso_origen) {
        this.id_curso_origen = id_curso_origen;
    }

    public Long getId_curso_destino() {
        return id_curso_destino;
    }

    public void setId_curso_destino(Long id_curso_destino) {
        this.id_curso_destino = id_curso_destino;
    }

    public String getTipoContenido() {
        return tipoContenido;
    }

    public void setTipoContenido(String tipoContenido) {
        this.tipoContenido = tipoContenido;
    }

    public String getContenido() {
        return contenido;
    }

    public void setContenido(String contenido) {
        this.contenido = contenido;
    }

    public String getPlataforma() {
        return plataforma;
    }

    public void setPlataforma(String plataforma) {
        this.plataforma = plataforma;
    }

    public Timestamp getTimestamp() {
        return timestamp;
    }

    public void setTimestamp(Timestamp timestamp) {
        this.timestamp = timestamp;
    }

    public String getSentimiento() {
        return sentimiento;
    }

    public void setSentimiento(String sentimiento) {
        this.sentimiento = sentimiento;
    }


}
