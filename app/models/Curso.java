package models;

import com.avaje.ebean.*;
import com.avaje.ebean.annotation.Sql;
import controllers.calculo.CalculolController;
import controllers.calculo.CalculolController$;

import javax.persistence.*;
import javax.validation.constraints.Size;
import java.io.Serializable;
import java.util.*;

@Entity
public class Curso{
    @Id
    @Column(name = "id_curso")
    private Long id_curso;
    @Size(max = 100)
    private String titulo;
    @Size(max = 20)
    private String codigo;
    @Size(max = 20)
    private String seccion;
    @JoinColumn(name = "periodo_inicio")
    private Date periodo_inicio;
    @JoinColumn(name = "periodo_fin")
    private Date periodo_fin;
    @OneToOne
    @JoinColumn(name = "id_escuela")
    private Long id_escuela;
    @Size(max = 20)
    private String id_facebook;
    @Size(max = 20)
    private String id_pam;
    @Size(max = 20)
    private String id_crea;
    @Size(max = 20)
    private String id_moodle;

    public List<SqlRow> informacion(){//Información del curso con promedio_general
        String interaccionesSql = "SELECT Curso.*, (SELECT SUM(Calificacion.calificacion)/COUNT(Calificacion.*) FROM Calificacion WHERE id_curso = Curso.id_curso) AS promedio_general FROM Curso WHERE id_curso = :id_curso";
        List<SqlRow> interacciones = Ebean.createSqlQuery(interaccionesSql)
                .setParameter("id_curso", this.id_curso)
                .findList();
        return interacciones;
    }

    /**
     * Lista de interacciones entre estudiantes-materiales de un curso
     * @return
     */
    public List<SqlRow> estudiantes(){
        String sql = "SELECT Estudiante.* FROM Estudiante WHERE id_estudiante IN (SELECT id_estudiante FROM EstudianteCurso WHERE id_curso=:id_curso);";
        return Ebean.createSqlQuery(sql)
                .setParameter("id_curso", this.id_curso)
                .findList();
    }

    /**
     * Lista de actividades generadas en un curso
     * @return
     */
    public List<SqlRow> actividades(String plataforma){
        String sql = "SELECT Material.nombre, Material.plataforma, Material.tipo_contenido, Material.fecha_publicacion, Material.numero_accesos, Material.url_ubicacion, Actividad.* FROM Material RIGHT JOIN Actividad ON Material.id_material = Actividad.id_actividad WHERE id_curso = :id_curso";

        if(plataforma.length()>0 && !plataforma.equals("*")){
            sql += " AND plataforma = '"+plataforma+"'";
        }

        return Ebean.createSqlQuery(sql)
                .setParameter("id_curso", this.id_curso)
                .findList();
    }

    /**
     * Lista de materiales generadas en un curso
     * @return
     */
    public List<SqlRow> materiales(String plataforma){
        String sql = "SELECT Material.* FROM Material WHERE id_curso = :id_curso AND tipo='ri'";

        if(plataforma.length()>0 && !plataforma.equals("*")){
            sql += " AND plataforma = '"+plataforma+"'";
        }

        return Ebean.createSqlQuery(sql)
                .setParameter("id_curso", this.id_curso)
                .findList();
    }

    public List<SqlRow> estsDiasDesc(){
        String sql = "SELECT id_estudiante, ABS(CAST(MAX(timestamp) AS date) - CAST(now() AS date)) AS dias FROM conexion WHERE id_estudiante IN (SELECT id_estudiante FROM EstudianteCurso WHERE id_curso=:id_curso) GROUP BY id_estudiante;";
        return Ebean.createSqlQuery(sql)
                .setParameter("id_curso", this.id_curso)
                .findList();
    }

    public List<Docente> docentes(){
        return Ebean.find(Docente.class)
                .where().in("id_docente",
                        Ebean.createQuery(CursoDocente.class)
                                .where().eq("id_curso", this.id_curso)
                                .select("id_docente")
                )
                .findList();
    }

    public List<Nodo> nodos(){
        return Ebean.find(Nodo.class)
                .where().in("id_docente",
                        Ebean.createQuery(CursoDocente.class)
                                .where().eq("id_curso", this.id_curso)
                                .select("id_docente")
                )
                .findList();
    }

    public List<SqlRow> allNodo(){
        String materialesSql = "select nodo.id_nodo AS id, nodo.tipo, material.nombre from nodo left join material ON material.id_material = nodo.id_nodo where material.id_curso = :id_curso and (nodo.tipo = :tipo_material OR nodo.tipo = :tipo_actividad)";
        String estudiantesSql = "select nodo.id_nodo AS id, nodo.tipo, estudiante.nombre from nodo left join estudiante ON estudiante.id_estudiante = nodo.id_nodo left join estudiantecurso ON estudiante.id_estudiante = estudiantecurso.id_estudiante where estudiantecurso.id_curso = :id_curso and nodo.tipo = :tipo_estudiante";

        List<SqlRow> nodos = Ebean.createSqlQuery(materialesSql)
                .setParameter("tipo_material", String.valueOf(Material.tipoNodo))
                .setParameter("tipo_actividad", String.valueOf(Actividad.tipoNodo))
                .setParameter("tipo_estudiante", String.valueOf(Estudiante.tipoNodo))
                .setParameter("id_curso", this.id_curso)
                .findList();
        nodos.addAll(
                Ebean.createSqlQuery(estudiantesSql)
                        .setParameter("id_curso", this.id_curso)
                        .setParameter("tipo_estudiante", String.valueOf(Estudiante.tipoNodo))
                        .findList()
        );
        return nodos;
    }

    public static List<Interaccion> interaccionesa(Curso curso){
        List<Interaccion> list =
                Ebean.find(Interaccion.class)
                        .where().eq("idCursoOrigen", curso.id_curso)
                        .findList();
        return list;
    }



    /**
     * TEst
     * @return
     */
    public List<SqlRow> test(List<Long> ids){
        /*
        ExpressionList a = Ebean.find(Interaccion.class).where().in("id_interaccion", Arrays.asList(1, 2, 3));
        return a.findList();
         */
        String sql = "select i.id_interaccion from interaccion i where i.id_interaccion IN (:ids)";


        return Ebean.createSqlQuery(sql)
                .setParameter("ids", ids)
                .findList();
    }

    /**
     * Lista de interacciones entre estudiantes-materiales de un curso
     * @return
     */
    public List<SqlRow> interaccionesEM(){
        String interaccionesSql = "SELECT interaccion.id_interaccion as id_interaccion, interaccion.contenido, interaccion.tipo_interaccion, interaccion.nodo_origen AS nodo_origen, n.tipo AS tipo_origen, interaccion.nodo_destino AS nodo_destino, nn.tipo AS tipo_destino FROM interaccion LEFT JOIN nodo n ON interaccion.nodo_origen = n.id_nodo LEFT JOIN nodo nn ON interaccion.nodo_destino = nn.id_nodo WHERE interaccion.id_curso_origen = :id_curso AND n.tipo = :tipo_estudiante AND nn.tipo = :tipo_material";
        List<SqlRow> interacciones = Ebean.createSqlQuery(interaccionesSql)
                .setParameter("id_curso", this.id_curso)
                .setParameter("tipo_estudiante", String.valueOf(Estudiante.tipoNodo))
                .setParameter("tipo_material", String.valueOf(Material.tipoNodo))
                .findList();
        return interacciones;
    }

    /**
     * Lista de interacciones entre estudiantes-docente de un curso
     * @return
     */
    public List<SqlRow> interaccionesED(){
        String interaccionesSql = "SELECT interaccion.id_interaccion as id_interaccion, interaccion.contenido, interaccion.tipo_interaccion, interaccion.nodo_origen AS nodo_origen, n.tipo AS tipo_origen, interaccion.nodo_destino AS nodo_destino, nn.tipo AS tipo_destino FROM interaccion LEFT JOIN nodo n ON interaccion.nodo_origen = n.id_nodo LEFT JOIN nodo nn ON interaccion.nodo_destino = nn.id_nodo WHERE interaccion.id_curso_origen = :id_curso AND n.tipo = :tipo_estudiante AND nn.tipo = :tipo_docente";
        List<SqlRow> interacciones = Ebean.createSqlQuery(interaccionesSql)
                .setParameter("id_curso", this.id_curso)
                .setParameter("tipo_estudiante", String.valueOf(Estudiante.tipoNodo))
                .setParameter("tipo_docente", String.valueOf(Docente.tipoNodo))
                .findList();
        return interacciones;
    }

    public List<SqlRow> recursos(){
        String recursosSql = "SELECT Material.* FROM Material WHERE id_curso = :id_curso";
        return Ebean.createSqlQuery(recursosSql)
                .setParameter("id_curso", this.id_curso)
                .findList();
    }

    public Long id() {
        return id_curso;
    }

    private Long getId_curso() {
        return id_curso;
    }

    private void setId_curso(Long id_curso) {
        this.id_curso = id_curso;
    }

    public String getTitulo() {
        return titulo;
    }

    public void setTitulo(String titulo) {
        this.titulo = titulo;
    }


    public Date getPeriodo_inicio() {
        return periodo_inicio;
    }

    public void setPeriodo_inicio(Date periodo_inicio) {
        this.periodo_inicio = periodo_inicio;
    }


    public Date getPeriodo_fin() {
        return periodo_fin;
    }

    public void setPeriodo_fin(Date periodo_fin) {
        this.periodo_fin = periodo_fin;
    }

    public Long getId_escuela() {
        return id_escuela;
    }

    public void setId_escuela(Long id_escuela) {
        this.id_escuela = id_escuela;
    }

    public String getId_facebook() {
        return id_facebook;
    }

    public void setId_facebook(String id_facebook) {
        this.id_facebook = id_facebook;
    }

    public String getId_pam() {
        return id_pam;
    }

    public void setId_pam(String id_pam) {
        this.id_pam = id_pam;
    }

    public String getId_crea() {
        return id_crea;
    }

    public void setId_crea(String id_crea) {
        this.id_crea = id_crea;
    }

    public String getId_moodle() {
        return id_moodle;
    }

    public void setId_moodle(String id_moodle) {
        this.id_moodle = id_moodle;
    }

    public static Model.Finder<Long, Curso> find() {
        return new Model.Finder<>(Curso.class);
    }

    public String getCodigo() {
        return codigo;
    }

    public String getSeccion() {
        return seccion;
    }


    public static void nuevoCurso(String titulo , String codigo , String seccion , String periodo_inicio , String periodo_fin , Integer id_escuela , String id_facebook , String id_pam , String id_crea , String id_moodle){
        String query = "insert into curso (titulo, codigo, periodo_inicio, periodo_fin, id_escuela, id_facebook, id_pam, id_crea, id_moodle) values (:id_curso, :titulo, :codigo, :periodo_inicio::date, :periodo_fin::date, :id_escuela, :id_facebook, :id_pam, :id_crea, :id_moodle);";
        Ebean.createSqlUpdate(query).
                setParameter("titulo", titulo).
                setParameter("codigo", codigo).
                setParameter("periodo_inicio", periodo_inicio).
                setParameter("periodo_fin", periodo_fin).
                setParameter("id_escuela", id_escuela).
                setParameter("id_facebook", id_facebook).
                setParameter("id_pam", id_pam).
                setParameter("id_crea", id_crea).
                setParameter("id_moodle", id_moodle)
                .execute();
    }


    public static void actualizarCurso(Integer id_curso, String titulo , String codigo , String seccion , String periodo_inicio , String periodo_fin , Integer id_escuela , String id_facebook , String id_pam , String id_crea , String id_moodle  ){
        String query = "update curso set titulo=:titulo, codigo=:codigo, periodo_inicio=:periodo_inicio, periodo_fin=:periodo_fin, id_escuela=:id_escuela, id_facebook=:id_facebook id_pam=:id_pam, id_crea=:id_crea, id_moodle=:id_moodle where id_curso=:id_curso;";
        Ebean.createSqlUpdate(query).
                setParameter("titulo", titulo).
                setParameter("codigo", codigo).
                setParameter("periodo_inicio", periodo_inicio).
                setParameter("periodo_fin", periodo_fin).
                setParameter("id_escuela", id_escuela).
                setParameter("id_facebook", id_facebook).
                setParameter("id_pam", id_pam).
                setParameter("id_crea", id_crea).
                setParameter("id_moodle", id_moodle)
                .execute();
    }

    public static void eliminarCurso(Integer id_curso){
        String query = "delete from curso where id_curso=:id_curso;";
        Ebean.createSqlUpdate(query).
                setParameter("id_curso", id_curso).
                execute();
    }

    /**
     * Charts
     */

    /**
     * Nota general promedio
     * @return
     */
    public List<SqlRow> notaGenProm(){
        String sql = "SELECT AVG(calificacion) AS notaGenProm FROM Calificacion WHERE id_curso = :id_curso";
        return Ebean.createSqlQuery(sql)
                .setParameter("id_curso", this.id_curso)
                .findList();
    }

    /**
     * Cantidad de estudiantes
     * @return
     */
    public List<SqlRow> cantidadEstud(){
        String sql = "SELECT COUNT(*) AS cantidadEstud FROM EstudianteCurso WHERE id_curso = :id_curso";
        return Ebean.createSqlQuery(sql)
                .setParameter("id_curso", this.id_curso)
                .findList();
    }

    /**
     * Participación por plataforma.
     * @return
     */
    public List<SqlRow> partPlataforma(){
        String sql = "SELECT plataforma, COUNT(*) AS counter FROM Interaccion WHERE id_curso_origen = :id_curso GROUP BY plataforma";
        return Ebean.createSqlQuery(sql)
                .setParameter("id_curso", this.id_curso)
                .findList();
    }

    /**
     * Cantidad de actividades.
     * @return
     */
    public List<SqlRow> cantActividades(){
        String sql = "SELECT COUNT(*) AS counter FROM material INNER JOIN actividad ON material.id_material = actividad.id_actividad WHERE material.id_curso=:id_curso";
        return Ebean.createSqlQuery(sql)
                .setParameter("tipo_actividad", String.valueOf(Actividad.tipoMaterial))
                .setParameter("id_curso", this.id_curso)
                .findList();
    }

    /**
     * Cantidad de materiales.
     * @return
     */
    public List<SqlRow> cantMateriales(){
        String sql = "SELECT COUNT(*) AS counter FROM material WHERE material.id_curso=:id_curso AND material.tipo = :tipo_material";
        return Ebean.createSqlQuery(sql)
                .setParameter("tipo_material", String.valueOf(Material.tipoMaterial))
                .setParameter("id_curso", this.id_curso)
                .findList();
    }

    /**
     * Material con más interacciones.
     * @return
     */
    public List<SqlRow> materialMasInt(){
        String sql = "SELECT nodo_destino, material.nombre, count(nodo_destino) AS counter, material.fecha_publicacion, material.tipo_contenido FROM interaccion INNER JOIN material ON interaccion.nodo_destino = Material.id_material WHERE Material.tipo = :tipo_material AND material.id_curso = :id_curso GROUP BY nodo_destino, nombre, material.fecha_publicacion, material.tipo_contenido ORDER BY counter DESC LIMIT 1";
        return Ebean.createSqlQuery(sql)
                .setParameter("tipo_material", String.valueOf(Material.tipoMaterial))
                .setParameter("id_curso", this.id_curso)
                .findList();
    }

    /**
     * Actividad con más interacciones.
     * @return
     */
    public List<SqlRow> actividadMasInt(){
        String sql = "SELECT nodo_destino, material.nombre, count(nodo_destino) AS count, material.fecha_publicacion, actividad.fecha_limite FROM interaccion INNER JOIN material ON interaccion.nodo_destino = Material.id_material INNER JOIN actividad ON interaccion.nodo_destino = Actividad.id_actividad WHERE Material.id_curso = :id_curso GROUP BY nodo_destino, nombre, material.fecha_publicacion, actividad.fecha_limite ORDER BY count DESC LIMIT 1";
        return Ebean.createSqlQuery(sql)
                .setParameter("tipo_actividad", String.valueOf(Actividad.tipoMaterial))
                .setParameter("id_curso", this.id_curso)
                .findList();
    }

    /**
     * Materiales más consultados.
     * @return
     */
    public List<SqlRow> materialesMasCons(int n){
        String sql = "SELECT Material.nombre, sub.count FROM (SELECT nodo_destino, count(nodo_destino) AS count FROM interaccion INNER JOIN Material ON Interaccion.nodo_destino = Material.id_material WHERE Interaccion.tipo_interaccion = :tipo_visualizacion AND Material.tipo = :tipo_material AND Material.id_curso = :id_curso GROUP BY nodo_destino ORDER BY count DESC LIMIT :n) sub LEFT JOIN Material ON sub.nodo_destino = Material.id_material";
        return Ebean.createSqlQuery(sql)
                .setParameter("tipo_material", String.valueOf(Material.tipoMaterial))
                .setParameter("tipo_visualizacion", "vis" )
                .setParameter("n", n)
                .setParameter("id_curso", this.id_curso)
                .findList();
    }

    /**
     * Actividades más consultadas.
     * @return
     */
    public List<SqlRow> actividadesMasCons(int n){
        String sql = "SELECT Material.nombre, sub.count FROM (SELECT nodo_destino, count(nodo_destino) AS count FROM interaccion INNER JOIN Material ON Interaccion.nodo_destino = Material.id_material WHERE Interaccion.tipo_interaccion = :tipo_visualizacion AND Material.tipo = :tipo_material AND Material.id_curso = :id_curso GROUP BY nodo_destino ORDER BY count DESC LIMIT :n) sub LEFT JOIN Material ON sub.nodo_destino = Material.id_material";
        return Ebean.createSqlQuery(sql)
                .setParameter("tipo_material", String.valueOf(Actividad.tipoMaterial))
                .setParameter("tipo_visualizacion", "vis" )
                .setParameter("n", n)
                .setParameter("id_curso", this.id_curso)
                .findList();
    }



    /**
     * Chart Estudiantes
     */

    /**
     * Promedio de interacciones entre estudiantes (entre cada par único de estudiantes: a->b = b->a).
     * @return
     */
    public List<SqlRow> promIntEstutes(){
        String sql = "SELECT SUM(sub2.pair_interactions) / COUNT(sub2.unic_pair) AS prom FROM (SELECT COUNT(sub.b) as pair_interactions, sub.a as unic_pair FROM (SELECT greatest(nodo_origen, nodo_destino) as a, least(nodo_origen, nodo_destino) as b FROM Interaccion INNER JOIN Estudiante ON Interaccion.nodo_origen = Estudiante.id_estudiante OR Estudiante.id_estudiante = Interaccion.nodo_origen INNER JOIN EstudianteCurso ON estudiante.id_estudiante = estudiantecurso.id_estudiante WHERE estudiantecurso.id_curso = :id_curso ) sub GROUP BY sub.a, sub.b) sub2";
        return Ebean.createSqlQuery(sql)
                .setParameter("id_curso", this.id_curso)
                .findList();
    }

    /**
     * Promedio de conexiones.
     * @return
     */
    public List<SqlRow> promConexiones(){
        String sql = "SELECT SUM(sub.counter) / COUNT(sub.id_estudiante) AS prom FROM (SELECT conexion.id_estudiante, COUNT(conexion.id_conexion) as counter FROM conexion INNER JOIN estudiantecurso ON estudiantecurso.id_estudiante = conexion.id_estudiante WHERE estudiantecurso.id_curso=:id_curso GROUP BY conexion.id_estudiante) sub";
        return Ebean.createSqlQuery(sql)
                .setParameter("id_curso", this.id_curso)
                .findList();
    }

    /**
     * Promedio de nota.
     * @return
     */
    public List<SqlRow> promNota(){
        String sql = "SELECT SUM(sub.alumno_average) / COUNT(*) AS prom FROM (SELECT SUM(calificacion) / COUNT(calificacion) as alumno_average FROM calificacion WHERE id_curso = :id_curso GROUP BY id_estudiante ) sub";
        return Ebean.createSqlQuery(sql)
                .setParameter("id_curso", this.id_curso)
                .findList();
    }

    /**
     * Porcentaje de interacciones por plataforma.
     * @return
     */
    public List<SqlRow> porcentajeIntPlat(){
        String sql = "SELECT plataforma, COUNT(*) as count FROM Interaccion WHERE id_curso_origen = :id_curso GROUP BY plataforma";
        return Ebean.createSqlQuery(sql)
                .setParameter("id_curso", this.id_curso)
                .findList();
    }

    /**
     * Porcentaje de interacciones por plataforma tomando solo las interacciones de los estudiantes.
     * @return
     */
    public List<SqlRow> porcentajeIntPlatE(){
        String sql = "SELECT plataforma, COUNT(*) as count FROM Interaccion LEFT JOIN nodo ON nodo.id_nodo = interaccion.nodo_origen WHERE id_curso_origen = :id_curso AND nodo.tipo = 'e' GROUP BY plataforma";
        return Ebean.createSqlQuery(sql)
                .setParameter("id_curso", this.id_curso)
                .findList();
    }


    /**
     * Cantidad por tipo de interacción.
     * @return
     */
    public List<SqlRow> cantPTipoInt(){
        String sql = "SELECT tipo_interaccion, COUNT(*) AS count FROM Interaccion WHERE id_curso_origen = :id_curso GROUP BY tipo_interaccion";
        return Ebean.createSqlQuery(sql)
                .setParameter("id_curso", this.id_curso)
                .findList();
    }

    /**
     * Porcentaje de interacciones por polaridad.
     * @return
     */
    public List<SqlRow> cantPPolaridad(){
        String sql = "SELECT sentimiento, COUNT(*) as count FROM Interaccion INNER JOIN Nodo no ON Interaccion.nodo_origen = no.id_nodo WHERE id_curso_origen = :id_curso AND no.tipo = 'e' AND (tipo_contenido = 'tex') AND sentimiento IS NOT NULL GROUP BY sentimiento";
        return Ebean.createSqlQuery(sql)
                .setParameter("id_curso", this.id_curso)
                .findList();
    }

    /**
     * Estudiante más popular.
     * @return
     */
    public List<SqlRow> estudiantePopular(){
        String sql = "SELECT nodo_destino, concat(estudiante.nombre, ' ', estudiante.apellidos) as nombre, COUNT(*) as interactions FROM Interaccion INNER JOIN Estudiante ON Interaccion.nodo_destino = estudiante.id_estudiante WHERE id_curso_origen = :id_curso GROUP BY nodo_destino, Estudiante.nombre, Estudiante.apellidos ORDER BY interactions DESC LIMIT 1";
        return Ebean.createSqlQuery(sql)
                .setParameter("id_curso", this.id_curso)
                .findList();
    }

    /**
     * Porcentaje por tipo nodo para la interacción.
     * @return
     */
    public List<SqlRow> porcentajePTipoNodo(){
        String sql = "SELECT nd.tipo, COUNT(*) as count FROM Interaccion INNER JOIN Nodo no ON Interaccion.nodo_origen = no.id_nodo INNER JOIN Nodo nd ON Interaccion.nodo_destino = nd.id_nodo WHERE no.tipo = :tipo_estudiante AND Interaccion.id_curso_origen = :id_curso GROUP BY nd.tipo";
        return Ebean.createSqlQuery(sql)
                .setParameter("tipo_estudiante", String.valueOf(Estudiante.tipoNodo))
                .setParameter("id_curso", this.id_curso)
                .findList();
    }


    /**
     * Recursos
     */

    /**
     * Porcentaje por plataforma.
     * @return
     */
    public List<SqlRow> porcentajePPlataf(){
        String sql = " SELECT plataforma, COUNT(*) AS total FROM Material WHERE tipo = :tipo_material AND id_curso = :id_curso GROUP BY plataforma";
        return Ebean.createSqlQuery(sql)
                .setParameter("tipo_material", Material.tipoMaterial)
                .setParameter("id_curso", this.id_curso)
                .findList();
    }

    /**
     * Porcentaje por tipo de Material.
     * @return
     */
    public List<SqlRow> porcenPTipoMat(){
        String sql = "SELECT tipo_contenido, COUNT(*) as count FROM Material WHERE tipo = :tipo_material AND id_curso = :id_curso GROUP BY tipo_contenido";
        return Ebean.createSqlQuery(sql)
                .setParameter("tipo_material", Material.tipoMaterial)
                .setParameter("id_curso", this.id_curso)
                .findList();
    }

    /**
     * Promedio de cantidad de accesos.
     * @return
     */
    public List<SqlRow> matPromPCantAcce(){
        String sql = "SELECT SUM(sub.visualizations) / COUNT(*) as prom FROM (SELECT nodo_destino, COUNT(id_interaccion) AS visualizations FROM Interaccion INNER JOIN Material ON Interaccion.nodo_destino = Material.id_material WHERE id_curso_origen = :id_curso AND Material.tipo = :tipo_material AND Interaccion.tipo_interaccion = :tipo_visualizacion GROUP BY Interaccion.nodo_destino) sub";
        return Ebean.createSqlQuery(sql)
                .setParameter("tipo_material", Material.tipoMaterial)
                .setParameter("tipo_visualizacion", "vis")
                .setParameter("id_curso", this.id_curso)
                .findList();
    }

    /**
     * Actividades
     */

    /**
     * Promedio de cantidad de accesos.
     * @return
     */
    public List<SqlRow> actPromPCantAcce(){
        String sql = "SELECT SUM(sub.visualizations) / COUNT(*) as prom FROM (SELECT nodo_destino, COUNT(id_interaccion) AS visualizations FROM Interaccion INNER JOIN Material ON Interaccion.nodo_destino = Material.id_material WHERE id_curso_origen = :id_curso AND Material.tipo = :tipo_actividad AND Interaccion.tipo_interaccion = :tipo_visualizacion GROUP BY Interaccion.nodo_destino) sub";
        return Ebean.createSqlQuery(sql)
                .setParameter("tipo_actividad", Actividad.tipoMaterial)
                .setParameter("tipo_visualizacion", "vis")
                .setParameter("id_curso", this.id_curso)
                .findList();
    }

    /**
     * Promedio de nota.
     * @return
     */
    public List<SqlRow> actPromPNota(){
        String sql = "SELECT SUM(sub.average) / COUNT(*) as prom FROM (SELECT SUM(EstudianteActividad.calificacion) / COUNT(*) AS average FROM EstudianteActividad INNER JOIN Material ON estudianteactividad.id_actividad = Material.id_material WHERE Material.id_curso = :id_curso GROUP BY EstudianteActividad.id_estudiante, EstudianteActividad.id_actividad) sub";
        return Ebean.createSqlQuery(sql)
                .setParameter("id_curso", this.id_curso)
                .findList();
    }

    /**
     * Próxima actividad a entregar.
     * set lc_time to 'es_MX.utf8';
     * @return
     */
    public List<SqlRow> proxActAEnt(){
        String sql = "SELECT id_actividad, material.nombre, date(material.fecha_publicacion) as fecha_publicacion,  to_char(date(material.fecha_publicacion), 'DD/TMMonth/YYYY') as fecha_publicacion_leg, to_char(date(actividad.fecha_limite), 'DD/TMMonth/YYYY') as fecha_limite_leg, date(actividad.fecha_limite) as fecha_limite FROM Material INNER JOIN Actividad ON Material.id_material = Actividad.id_actividad WHERE Material.id_curso = :id_curso AND Actividad.fecha_limite <= NOW() ORDER BY Actividad.fecha_limite DESC";
        return Ebean.createSqlQuery(sql)
                .setParameter("id_curso", this.id_curso)
                .findList();
    }

    /**
     * Nivel de realización de actividades.
     * @return
     */
    public List<SqlRow> nivelRealAct(){
        String sql = "SELECT estado, COUNT(*) as count FROM (SELECT estudianteactividad.estado, MAX(estudianteactividad.fecha_entrega) FROM estudianteactividad INNER JOIN Material ON Material.id_material = estudianteactividad.id_actividad INNER JOIN actividad ON actividad.id_actividad = estudianteactividad.id_actividad WHERE material.id_curso = :id_curso GROUP BY estudianteactividad.id_estudiante, estudianteactividad.id_actividad) sub GROUP BY sub.estado";
        return Ebean.createSqlQuery(sql)
                .setParameter("id_curso", this.id_curso)
                .findList();
    }

    /**
     * Calificación máxima obtenida en las actividades.
     * @return
     */
    public List<SqlRow> actCalMax(){
        String sql = "SELECT MAX(estudianteactividad.calificacion) FROM estudianteactividad INNER JOIN Material ON Material.id_material = estudianteactividad.id_actividad INNER JOIN actividad ON actividad.id_actividad = estudianteactividad.id_actividad WHERE material.id_curso = :id_curso AND estudianteactividad.calificacion is not null";
        return Ebean.createSqlQuery(sql)
                .setParameter("id_curso", this.id_curso)
                .findList();
    }

    /**
     * Calificación mínima obtenida en las actividades.
     * @return
     */
    public List<SqlRow> actCalMin(){
        String sql = "SELECT MIN(estudianteactividad.calificacion) FROM estudianteactividad INNER JOIN Material ON Material.id_material = estudianteactividad.id_actividad INNER JOIN actividad ON actividad.id_actividad = estudianteactividad.id_actividad WHERE material.id_curso = :id_curso AND estudianteactividad.calificacion is not null";
        return Ebean.createSqlQuery(sql)
                .setParameter("id_curso", this.id_curso)
                .findList();
    }

}


