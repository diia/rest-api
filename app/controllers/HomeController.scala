package controllers

import java.util
import javax.inject._

import com.avaje.ebean.Ebean
import models._
import play.api._
import play.api.mvc._

/**
 * This controller creates an `Action` to handle HTTP requests to the
 * application's home page.
 */
@Singleton
class HomeController @Inject() extends Controller {

  /**
   * Create an Action to render an HTML page with a welcome message.
   * The configuration in the `routes` file means that this method
   * will be called when the application receives a `GET` request with
   * a path of `/`.
   */
  def index = Action {
    val docente = Docente.find.byId(25L)
    import scala.collection.JavaConversions._

    val tmp = "*".split("-")
    Ok("*".split("-").length + "|" + tmp.toList.get(0 ))
    //Ok(views.html.index("Your new application is ready."))
  }

  def graph = Action {

    import org.graphstream.graph.Graph
    import org.graphstream.graph.implementations.SingleGraph
    val graph = new SingleGraph("Tutorial 1")
    graph.setStrict(false)
    graph.setAutoCreate(true) //Infer graph

    /*
    graph.addNode("A")
    graph.addNode("B")
    graph.addNode("C")
   */
    graph.addEdge("AB", "A", "B")
    graph.addEdge("BC", "B", "C")
    graph.addEdge("CA", "C", "A")

    /*val n:Nodo = new Nodo()
    n.setTipo(2)
    Ebean.save(n)
    */
/*
    val c:Curso= new Curso()
    c.setTitulo("T")
    Ebean.save(c)
    val n:Nodo = new Nodo();
    Ebean.save(n);

   val d:Docente = new Docente(n)
    d.setNombre("D")

    import java.util
    val cursos = new util.ArrayList[Curso]
    cursos.add(c);
    d.setCursos(cursos)
    Ebean.save(d)
*/
    /*
        val deparment:Department = Department.find.byId(1)

        val student = Student.find.byId(1)
        student.setName("s")

        var list = new java.util.ArrayList[Department]()
        list.add(deparment)
        student.setDepartment(list)
        //student.getDepartments
        Ebean.update(student)
    */
   // var a = ""
    //val it = student.getDepartments.iterator()
  /*  while(it.hasNext){
        a += it
    }*/
    //import scala.collection.JavaConverters._
    //Ok(it.asScala.mkString)
    Ok(graph.getNodeCount.toString)
    //Ok(views.html.index("Your new application is ready."))
  }

}
