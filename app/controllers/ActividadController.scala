package controllers

import java.util
import javax.inject.{Inject, Singleton}

import com.avaje.ebean.Ebean
import com.fasterxml.jackson.databind.ObjectMapper
import models._
import play.api.mvc.{Action, Controller}

@Singleton
class ActividadController @Inject()() extends Controller {

  @throws[Exception]
  def detalle(id_actividad: Long) = Action {
    val objectMapper = new ObjectMapper
    val actividad = Actividad.find.byId(id_actividad)
    val result = new util.HashMap[String, Any]
    if(actividad != null && actividad.detalle() != null){
      //result.put("detalle", actividad.detalle())
      result.put("id_actividad", actividad.detalle().get("id"))
      result.put("nombre", actividad.detalle().get("nombre"))
      result.put("plataforma", actividad.detalle().get("plataforma"))
      result.put("tipo", actividad.detalle().get("tipo"))
      result.put("calificacionMaxima", actividad.detalle().get("calificacion_maxima"))
      result.put("calificacionMinima", actividad.detalle().get("calificacion_minima"))
      result.put("alumnos", actividad.estudiantes)
      Ok(objectMapper.writeValueAsString(result)).as("application/json")
    }else{
      result.put("code", 5)
      result.put("msg", "Actividad no encontrada.")
      NotFound(objectMapper.writeValueAsString(result)).as("application/json")
    }
  }

}
