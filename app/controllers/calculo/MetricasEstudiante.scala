package controllers.calculo

import java.lang.String
import java.util
import javax.inject.{Inject, Singleton}

import scala.collection.JavaConverters._
import org.graphstream.graph.Node
import org.graphstream.graph.implementations.SingleGraph
import org.graphstream.graph.Edge
import com.fasterxml.jackson.databind.ObjectMapper
import models.{Curso, Estudiante}
import org.graphstream.algorithm.measure.ClosenessCentrality
import org.graphstream.algorithm.{APSP, BetweennessCentrality, Eccentricity, PageRank}
import play.api.mvc.{Action, Controller}

import scala.collection.immutable.{HashMap, ListMap}
import scala.collection.{JavaConverters, SortedMap, mutable}

@Singleton
class MetricasEstudiante @Inject()() extends Controller {

  @throws[Exception]
  def outdegree(id_curso: Long) = Action {
    val objectMapper = new ObjectMapper
    val curso = Curso.find.byId(id_curso)
    val result = new util.HashMap[String, AnyRef]
    if(curso != null){
      val metricas = scala.collection.mutable.Map[String, Int]()
      //Obtener todas las interacciones alumno-alumno
      val interacciones = curso.interaccionesED()

      //Poblar el grafo
      var graph = new SingleGraph("Curso "+curso.getTitulo)
      graph.setStrict(false)
      graph.setAutoCreate(true)

      for(i <- 0 until interacciones.size()){
        graph.addEdge[Edge](interacciones.get(i).getString("id_interaccion"), interacciones.get(i).getString("nodo_origen"), interacciones.get(i).getString("nodo_destino"))//true
      }

      //Poblar la estructura de datos
      graph.getNodeSet[Node]
        .asScala.toArray.foreach(
        (node: Node) => {
          metricas(node.getId) = node.getOutDegree
        }
      )

      //Ordenar los resultados
      val metricasOrdered = Seq(metricas.toSeq.sortBy(_._2):_*).map(i => i._1 -> i._2)

      result.put("metricas", metricasOrdered.toMap.asJava);
      result.put("valorMaximo", metricasOrdered(metricasOrdered.length-1)._2.toString);
      result.put("valorMinimo", metricasOrdered(0)._2.toString)
    }else{
      result.put("status", "error");
      result.put("msg", "Curso no encontrado");
    }

    Ok(objectMapper.writeValueAsString(result)).as("application/json")
  }

  @throws[Exception]
  def indegree(id_curso: Long) = Action {
    val objectMapper = new ObjectMapper
    val curso = Curso.find.byId(id_curso)
    val result = new util.HashMap[String, AnyRef]
    if(curso != null){
      val metricas = scala.collection.mutable.Map[String, Int]()
      //Get all interacciones
      val interacciones = curso.interaccionesED()
      //Fill graph
      var graph = new SingleGraph("Curso "+curso.getTitulo)
      graph.setStrict(false)
      graph.setAutoCreate(true)

      for(i <- 0 until interacciones.size()){
        graph.addEdge[Edge](interacciones.get(i).getString("id_interaccion"), interacciones.get(i).getString("nodo_origen"), interacciones.get(i).getString("nodo_destino"))//true
      }

      //Fill datastructure
      graph.getNodeSet[Node]
        .asScala.toArray.foreach(
        (node: Node) => {
          metricas(node.getId) = node.getInDegree
        }
      )

      val metricasOrdered = Seq(metricas.toSeq.sortBy(_._2):_*).map(i => i._1 -> i._2)
      result.put("metricas", metricasOrdered.toMap.asJava);
      result.put("valorMaximo", metricasOrdered(metricasOrdered.length-1)._2.toString);
      result.put("valorMinimo", metricasOrdered(0)._2.toString)
    }else{
      result.put("status", "error");
      result.put("msg", "Curso no encontrado");
    }

    Ok(objectMapper.writeValueAsString(result)).as("application/json")
  }

  @throws[Exception]
  def betweenness(id_curso: Long) = Action {
    val objectMapper = new ObjectMapper
    val curso = Curso.find.byId(id_curso)
    val result = new util.HashMap[String, AnyRef]
    if(curso != null){
      val metricas = scala.collection.mutable.Map[String, Double]()

      //Get all interacciones
      val interacciones = curso.interaccionesED()
      //Fill graph
      var graph = new SingleGraph("Curso "+curso.getTitulo)
      graph.setStrict(false)
      graph.setAutoCreate(true)

      for(i <- 0 until interacciones.size()){
        graph.addEdge[Edge](interacciones.get(i).getString("id_interaccion"), interacciones.get(i).getString("nodo_origen"), interacciones.get(i).getString("nodo_destino"))//true
      }

      //Calculate Betweenness centrality
      val bcb: BetweennessCentrality = new BetweennessCentrality
      //Add weights
      /*bcb.setWeightAttributeName("weight")
      for(i <- 0 until interacciones.size()){
        val nodo_origen = graph.getNode[Node](interacciones.get(i).getString("nodo_origen"))
        val nodo_destino = graph.getNode[Node](interacciones.get(i).getString("nodo_destino"))
        val tipo_interaccion = interacciones.get(i).getString("tipo_interaccion")

        if(tipo_interaccion.equalsIgnoreCase("rea")){
          val tipo_reaccion = interacciones.get(i).getString("contenido")
          controllers.Constants.weights.foreach(
            kv => {
              if(tipo_reaccion.equalsIgnoreCase(kv._1)){
                System.out.println(nodo_origen, nodo_destino, kv._2)
                bcb.setWeight(nodo_origen, nodo_destino, kv._2)
              }
            }
          )
        }
      }
      */

      bcb.betweennessCentrality(graph)

      var resultados = scala.collection.mutable.Map[String, Double]()

      //Fill datastructure
      graph.getNodeSet[Node]
        .asScala.toArray.foreach(
        (node: Node) => {
          metricas(node.getId) = node.getAttribute[Double]("Cb")
        }
      )

      val metricasOrdered = Seq(metricas.toSeq.sortBy(_._2):_*)
      result.put("metricas", metricasOrdered.toMap.asJava);
      result.put("valorMaximo", metricasOrdered(metricasOrdered.length-1)._2.toString);
      result.put("valorMinimo", metricasOrdered(0)._2.toString)
    }else{
      result.put("status", "error");
      result.put("msg", "Curso no encontrado");
    }

    Ok(objectMapper.writeValueAsString(result)).as("application/json")
  }

  @throws[Exception]
  def pagerank(id_curso: Long) = Action {
    val objectMapper = new ObjectMapper
    val curso = Curso.find.byId(id_curso)
    val result = new util.HashMap[String, AnyRef]
    if(curso != null){
      val metricas = scala.collection.mutable.Map[String, Double]()
      //Get all interacciones
      val interacciones = curso.interaccionesED()
      //Fill graph
      var graph = new SingleGraph("Curso "+curso.getTitulo)
      graph.setStrict(false)
      graph.setAutoCreate(true)

      for(i <- 0 until interacciones.size()){
        graph.addEdge[Edge](interacciones.get(i).getString("id_interaccion"), interacciones.get(i).getString("nodo_origen"), interacciones.get(i).getString("nodo_destino"))//true
      }

      //Calculate Pagerank
      val pagerank: PageRank = new PageRank()
      pagerank.init(graph)
      pagerank.compute()

      var suma = 0.0
      //Fill datastructure
      graph.getNodeSet[Node]
        .asScala.toArray.foreach(
        (node: Node) => {
          suma += node.getAttribute[Double]("PageRank")
          metricas(node.getId) = node.getAttribute[Double]("PageRank")
        }
      )
      System.out.println("Total pagerank:"+suma)

      val metricasOrdered = Seq(metricas.toSeq.sortBy(_._2):_*).map(i => i._1 -> i._2)

      result.put("metricas", metricasOrdered.toMap.asJava);
      result.put("valorMaximo", metricasOrdered(metricasOrdered.length-1)._2.toString);
      result.put("valorMinimo", metricasOrdered(0)._2.toString)
    }else{
      result.put("status", "error");
      result.put("msg", "Curso no encontrado");
    }

    Ok(objectMapper.writeValueAsString(result)).as("application/json")
  }

  @throws[Exception]
  def closeness(id_curso: Long) = Action {
    val objectMapper = new ObjectMapper
    val curso = Curso.find.byId(id_curso)
    val result = new util.HashMap[String, AnyRef]
    if(curso != null){
      val metricas = scala.collection.mutable.Map[String, Double]()
      //Get all interacciones
      val interacciones = curso.interaccionesED()
      //Fill graph
      var graph = new SingleGraph("Curso "+curso.getTitulo)
      graph.setStrict(false)
      graph.setAutoCreate(true)

      for(i <- 0 until interacciones.size()){
        graph.addEdge[Edge](interacciones.get(i).getString("id_interaccion"), interacciones.get(i).getString("nodo_origen"), interacciones.get(i).getString("nodo_destino"))//true
      }

      //Calculate Closeness
      val closeness: ClosenessCentrality = new ClosenessCentrality ()
      closeness.init(graph)
      closeness.compute()

      var suma =  0.0
      //Fill datastructure
      graph.getNodeSet[Node]
        .asScala.toArray.foreach(
        (node: Node) => {
          suma += node.getAttribute[Double]("closeness")
          metricas(node.getId) = node.getAttribute[Double]("closeness")
        }
      )
      System.out.println("clo:"+suma)

      val metricasOrdered = Seq(metricas.toSeq.sortBy(_._2):_*).map(i => i._1 -> i._2)
      result.put("metricas", metricasOrdered.toMap.asJava);
      result.put("valorMaximo", metricasOrdered(metricasOrdered.length-1)._2.toString);
      result.put("valorMinimo", metricasOrdered(0)._2.toString)
    }else{
      result.put("status", "error");
      result.put("msg", "Curso no encontrado");
    }

    Ok(objectMapper.writeValueAsString(result)).as("application/json")
  }

  @throws[Exception]
  def prueba(id_curso: Long) = Action {
    val objectMapper = new ObjectMapper
    val curso = Curso.find.byId(id_curso)
    val result = new util.HashMap[String, AnyRef]
    if(curso != null){
      val metricas = new util.HashMap[String, AnyRef]
      //Get all interacciones
      val interacciones = curso.interaccionesED()
      //Fill graph
      var graph = new SingleGraph("Curso "+curso.getTitulo)
      graph.setStrict(false)
      graph.setAutoCreate(true)

      for(i <- 0 until interacciones.size()){
        graph.addEdge[Edge](interacciones.get(i).getString("id_interaccion"), interacciones.get(i).getString("nodo_origen"), interacciones.get(i).getString("nodo_destino"))//true
      }

      import org.graphstream.algorithm.APSP
      //Before calculate the Eccentricity algorithm -> Calculate All Pair Shortest Path (APSP algorithm)//Before calculate the Eccentricity algorithm -> Calculate All Pair Shortest Path (APSP algorithm)

      val apsp: APSP = new APSP
      apsp.init(graph)
      //apsp.setWeightAttributeName("weight"); // ensure that the attribute name used is "weight"
      apsp.setDirected(true)

      //Calculate Closeness
      val closeness: ClosenessCentrality = new ClosenessCentrality ()
      closeness.init(graph)
      closeness.compute()


      var suma =  0.0
      //Fill datastructure
      graph.getNodeSet[Node]
        .asScala.toArray.foreach(
        (nodeX: Node) => {
          import org.graphstream.algorithm.APSP
          var info:APSP.APSPInfo = null
          var sum = 0.0
          graph.getNodeSet[Node]
            .asScala.toArray.foreach(
            (nodeY: Node) => {
              if(!nodeX.getId.toString.equals(nodeY.getId.toString)){
                import org.graphstream.algorithm.APSP
                info = nodeX.getAttribute(APSP.APSPInfo.ATTRIBUTE_NAME)
                sum += info.getShortestPathTo(nodeY.getId).getEdgeCount
              }
            }
          )
          val closenness = (1 / sum) * (graph.getNodeCount - 1)
          metricas.put(nodeX.getId.toString, closenness.toString)
          System.out.println(nodeX.getId.toString + "->" + closenness)
          suma += closenness
        }
      )
      System.out.println("clo:"+suma)

      val nodesOrdered = org.graphstream.algorithm.Toolkit.degreeMap(graph)

      result.put("metricas", metricas);
      result.put("valorMaximo", nodesOrdered.get(0).getDegree.toString);
      result.put("valorMinimo", nodesOrdered.get(nodesOrdered.size()-1).getDegree.toString)
    }else{
      result.put("status", "error");
      result.put("msg", "Curso no encontrado");
    }

    Ok(objectMapper.writeValueAsString(result)).as("application/json")
  }

}