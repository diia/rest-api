package controllers.calculo

import com.fasterxml.jackson.databind.ObjectMapper
import javax.inject.{Inject, Singleton}
import models.{Curso, Docente}
import org.graphstream.graph.implementations.{AbstractGraph, MultiGraph}
import play.api.mvc.{Action, Controller}
import java.util

import org.graphstream.algorithm.{BetweennessCentrality, PageRank}
import org.graphstream.algorithm.measure.ClosenessCentrality
import org.graphstream.graph.{Edge, Node}
import com.avaje.ebean.SqlRow
import com.google.gson.Gson
import com.google.gson.reflect.TypeToken
import com.sun.org.apache.xalan.internal.xsltc.compiler.util.ErrorMsg
import javax.swing.SpringLayout.Constraints
import play.api.libs.json.Json.obj
import play.api.libs.json.{JsDefined, JsError, JsSuccess, JsUndefined, JsValue, Json, Writes}

import scala.collection.MapLike
import scala.collection.immutable.HashMap
import scala.util.parsing.json.JSONObject


@Singleton
class CalculolController @Inject()() extends Controller{

  case class GetParameters(id_docente: Long, id_curso: Long, tipo_metrica: String, interaccion: Option[String], interaccion_tipo: String, nodos_resultado: String, fecha_inicio: String, fecha_fin: String)

  import play.api.data.Form
  import play.api.data.Forms._
  import play.api.data.validation.Constraints._
  import play.api.data._
  import play.api.data.Forms._
  val getParameters: Form[GetParameters] = Form {
    mapping(
      "id_docente" -> longNumber.verifying("21", n => n != null && n>0),
      "id_curso" -> default[Long](longNumber, -1).verifying("Número positivo", _>0).verifying("Número positivos", _>0),
      "tipo_metrica" -> text.verifying ("21", _.grouped(3) forall( CalculolController.validMetrics.values.map(a => a.toString) contains )),
      "interaccion" -> optional(text),
      "interaccion_tipo" -> text.verifying ("21", _.grouped(3) forall( CalculolController.validIntTypes contains ) ),
      "nodos_resultado" -> text.verifying("", _.grouped(1) forall( CalculolController.validNodes contains ) ),
      "fecha_inicio" -> text.verifying ( pattern("([0-9]{4,4}-[0-9]{2,2}-[0-9]{2,2})|\\*".r, "pattern", "Formato incorrecto.")),
      "fecha_fin" -> text.verifying( pattern("([0-9]{4,4}-[0-9]{2,2}-[0-9]{2,2})|\\*".r, "patter", "Formato incorrecto."))
    )(GetParameters.apply)(GetParameters.unapply)
      .verifying("interaccion required", fields =>
        (
          ((fields.interaccion.isEmpty ) || (fields.interaccion.isDefined) )
        )
      )
      .verifying( fields =>
        if (fields.interaccion.isDefined)
          ((fields.interaccion.get.grouped(2) forall( CalculolController.validInteractions contains )) )
        else true
      )
  }
  //      "interaccion" -> text.verifying ("21", _.grouped(2) forall( CalculolController.validInteractions contains ) ),
  //      "nodos_resultado" -> text.verifying ("21", _.grouped(3) forall( CalculolController.validNodes contains ) ),


//var id_curso: Long
  //var interactions_type

  case class ids(id: Long)

  implicit val userJsonWrites = Json.writes[ids]
  implicit val userJsonReads = Json.reads[ids]

  @throws[Exception]
  def test() = Action { request =>

    request.body.toString().length

    request.body.asJson.map { json =>
      json.validate[List[Long]] match {
        case success: JsSuccess[List[Long]] => {

          //Here can be recovered the interactions

          Ok("Validation passed! Username is "+success)
        }
        case JsError(error) => BadRequest("Validation failed!")
      }
    }.getOrElse {
      BadRequest("Expecting Json data")
    }

/*
    request.body.validate[List[Long]] match {
      case success: JsSuccess[List[Long]] => {

        //Here can be recovered the interactions

        Ok("Validation passed! Username is "+success)
      }
      case JsError(error) => BadRequest("Validation failed!")
    }
    */



    /*
    val b= request.body.validate[List[Long]].get

    request.body.validate[List[Long]].map{
      case (ids) => {
        val ta = Curso.find.byId(1L)//Del dep
        import scala.collection.JavaConverters._

        //Here can be recovered the interactions

        val a = ta.test(ids.map(java.lang.Long.valueOf).asJava)
        //Ok(Json.obj("status" ->"ok", "message" -> Json.toJson(a) ))
      }
    }
    */
    /*.recoverTotal{
    e => BadRequest(Json.obj("status" -> "error", "msg" -> JsError.toJson(e) ) )
    }*/

    //Ok(Json.obj("status" -> Json.toJson("")  ))
  }


case class NotFoundError(code: Int, msg: String)

object NotFoundError{
  import play.api.libs.json._
  implicit val errorMsg = Json.writes[NotFoundError]
}


  trait Loo extends Map[String, Map[String, Number]]{


  }


  case class NodeMetrics(var nodes: Map[String, Map[String, Number]] = Map.empty){
    def ++(tuple: (String, Map[String, Number])) = {
      nodes = nodes + (tuple._1 -> (nodes.getOrElse(tuple._1, Map.empty) ++ tuple._2))
    }
  }


  case class OneMetric(nodes:Seq[(String, BigDecimal)], min: BigDecimal, max: BigDecimal)


  object Serializators{
    import play.api.libs.json._
    import play.api.libs.json.Json._

    /*
    nodes: {
      "nodeId": value,
        ...
    },
    max: value,
    min: value
     */
    implicit val oneMetric = new Writes[OneMetric] {
      def writes(m: OneMetric): JsValue =
        Json.toJson(
          obj(
            "nodes" ->   m.nodes.foldLeft(Json.obj())((ob, node) =>
              ob ++ obj(node._1->node._2)
            ),
            "max"->m.max,
            "min"->m.min
            )
        )
    }


    /*
      {
        "idNode": {
          "metricType": value,
            ...
        },
          ...
      }
     */
    implicit val innerMap = new Writes[Map[String, Map[String, Number]]] {
      def writes(m: Map[String, Map[String, Number]]): JsValue =
        Json.toJson(
          m.map(k =>
            k._1 ->
              k._2.map(
                p =>{
                  p._1.toString -> BigDecimal(p._2.toString)
                }
              )

          )
        )
    }
    import play.api.libs.json._


    implicit val map = new Writes[Map[String, Number]] {
      def writes(m: Map[String, Number]): JsValue =
        Json.toJson(m.map(k => {
          obj(
            k._1.toString -> BigDecimal(k._2.toString)
            //"" -> ""
          )
        }
          )
        )
    }

    implicit val errors = new Writes[Seq[FormError]] {
      def writes(m: Seq[FormError]): JsValue =
        Json.toJson(
          obj("errors" -> m.groupBy(_.key).map(formError => {
            obj(
              (if (formError._1.length>0) formError._1 else "general" ) -> formError._2.map(
                error => {
                  error.messages.mkString(",")
                }
              )
            )
          }
          )
        ))
    }

    //BadRequest(Json.toJson(ErrorMsg(1,errorForm.errors.map
    // (f=>f.key+":"+f.messages.mkString(",")).mkString(","))))
    /*
    {
      errors: [
        {errorKey:
          [
            "",""
          ]
        }
      ]
    }
     */
  }

  implicit class StringInterpolations(sc: StringContext) {
    def ci = new {
      def unapply(other: String) = sc.parts.mkString.equalsIgnoreCase(other)
    }
  }



    /**
    *
    * @param idCurso Identificador del curso
    * @param metricType Tipo de metrica a calcular
    * @param int Interacciones que se considerarán en el análisis
    * @param intTypes Tipos de interaccion que se considerarán en el análisis
    * @param nTypes Tipos de nodos que serán devueltos con su resultado
    * @throws Exception
    * @return //Identificadores de nodos con resultados
    */
  @throws[Exception]
  def calculate(idDocente: Long, idCurso: Long, metricType: String, intType: String="", intTypes:String, nodeTypes: String="", fecha_inicio:String, fecha_fin:String) = Action { request =>
    getParameters.fillAndValidate( GetParameters(idDocente, idCurso, metricType, Option(intType), intTypes, nodeTypes, fecha_inicio, fecha_fin)).fold(
      errorForm => {
        BadRequest(Json.toJson(errorForm.errors)(Serializators.errors))
      },
      parameters => {
        Option(Docente.find.byId(parameters.id_docente)) map {
          docente =>
            import scala.collection.JavaConverters._
            import scala.collection.JavaConversions._

            (Option(parameters.id_curso) match {
              case Some(id_curso) => id_curso == -1 || docente.cursos().exists(s => s.getLong("id_curso") == id_curso)
              case None => true
            }) match {
              case true => {
                val ids = (
                  request.body.asJson.map { json =>
                    json.validate[List[Long]] match {
                      case success: JsSuccess[List[Long]] => {
                        success.value.map(java.lang.Long.valueOf)
                      }
                    }
                  }
                )

                //Get interactions
                var interacciones = docente.interacciones(parameters.id_curso, parameters.interaccion.getOrElse("").grouped(2).toList.asJava, parameters.interaccion_tipo.grouped(3).toList.asJava, parameters.fecha_inicio, parameters.fecha_fin, ids.getOrElse(List()).asJava)

                //Create graph and fill with interactions
                var graph = new MultiGraph("DIIA")
                graph = fillGraph(graph, interacciones, parameters.tipo_metrica.grouped(3).toSet)

                //Calculate metrics
                computeGraph(graph, parameters.tipo_metrica.grouped(3).toSet)

                //Fill resultSet
                var resultSet =  Map[String, Map[String, Number]]()
                resultSet = graphResults(graph, parameters.nodos_resultado.grouped(1).toSet, parameters.tipo_metrica.grouped(3).toSet)

                /*
                //Avoid serialization errors
                val javaMap = resultSet.map {
                  case (k, v) => k -> v.asJava
                }.asJava
                */
                if(parameters.tipo_metrica.grouped(3).length>1){
                  Ok(Json.toJson(resultSet)(Serializators.innerMap))
                }else{
                  val sortedNodes = resultSet.map(node=>{(node._1->BigDecimal(node._2.head._2.toString))}).toSeq.sortBy(_._2)
                  val max = sortedNodes.headOption
                  val min = sortedNodes.lastOption

                  Ok(Json.toJson(OneMetric(sortedNodes, max.getOrElse("",BigDecimal("0"))._2, min.getOrElse("",BigDecimal("0"))._2))(Serializators.oneMetric))
                }
              }
              case _ => NotFound(Json.toJson(NotFoundError(1,"El curso no pertenece al docente")))
            }

        }getOrElse{
          NotFound(Json.toJson(NotFoundError(1,"El docente no existe")))
        }

      }
    )

  }

  def fillGraph(graph: MultiGraph, interacciones: util.List[SqlRow], metricTypes:Set[String]): MultiGraph ={
    val undirectedGraph = metricTypes.forall (
          CalculolController.validMetrics.values.filter(_==CalculolController.validMetrics.Betweenness)
          .map(metric => metric.toString)
        contains )

    graph.setStrict(false)
    graph.setAutoCreate(true)

    for(i <- 0 until interacciones.size()){
      graph.addEdge[Edge](interacciones.get(i).getString("id_interaccion"), interacciones.get(i).getString("tipo_origen")+interacciones.get(i).getString("nodo_origen"), interacciones.get(i).getString("tipo_destino")+interacciones.get(i).getString("nodo_destino"), !undirectedGraph)
    }
    graph
  }

  def mergeUpdates[K1, K2, V](base: Map[K1, Map[K2, V]], tuple: (K1, Map[K2, V])) = {
    base + (tuple._1 -> (base.getOrElse(tuple._1, Map.empty) ++ tuple._2))
  }

  def computeGraph(graph: MultiGraph,  metricTypes:Set[String]) ={
    metricTypes.foreach(
      metric => {
        metric match {
          case CalculolController.validMetrics.Betweenness.shortName => {
            val bcb: BetweennessCentrality = new BetweennessCentrality
            bcb.betweennessCentrality(graph)
          }
          case CalculolController.validMetrics.Closeness.shortName => {
            val closeness: ClosenessCentrality = new ClosenessCentrality()
            closeness.init(graph)
            closeness.compute()
          }
          case CalculolController.validMetrics.PageRank.shortName => {
            val pagerank: PageRank = new PageRank()
            pagerank.init(graph)
            pagerank.compute()
          }
          case CalculolController.validMetrics.Eccentricity.shortName => {
            import org.graphstream.algorithm.APSP
            val apsp = new APSP
            apsp.init(graph)
            apsp.compute

            import org.graphstream.algorithm.Eccentricity
            val eccentricity: Eccentricity = new Eccentricity
            eccentricity.init(graph)
            eccentricity.compute
          }
          case _ =>
        }
      }
    )
  }

  def graphResults(graph: MultiGraph, nodeTypes:Set[String], metricTypes:Set[String]) ={
    var resultSet =  Map[String, Map[String, Number]]()

    import scala.collection.JavaConverters._
    graph.getNodeSet[Node]
      .asScala.toArray.foreach(
      (node: Node) => {
        nodeTypes.foreach(
          nodeType => {
            if (nodeType.length==0 || nodeType.equals("*") || node.getId.startsWith(nodeType)) {
              metricTypes.foreach(
                metric => {
                  var metricResult:Number = 0.0
                  metric match {
                    case CalculolController.validMetrics.Degree.shortName => {
                      metricResult = node.getDegree.asInstanceOf[Integer]
                    }
                    case CalculolController.validMetrics.InDegree.shortName => {
                      metricResult = node.getInDegree.asInstanceOf[Integer]
                    }
                    case CalculolController.validMetrics.OutDegree.shortName => {
                      metricResult = node.getOutDegree.asInstanceOf[Integer]
                    }
                    case CalculolController.validMetrics.Betweenness.shortName => {
                      metricResult = node.getAttribute[Double]("Cb")
                    }
                    case CalculolController.validMetrics.Closeness.shortName => {
                      metricResult = node.getAttribute[Double]("closeness")
                    }
                    case CalculolController.validMetrics.PageRank.shortName => {
                      metricResult = node.getAttribute[Double]("PageRank")
                    }
                    case CalculolController.validMetrics.Eccentricity.shortName => {
                      metricResult = if (node.getAttribute[Boolean]("eccentricity")) 1 else 0
                    }
                    case _ =>
                  }

                  if(metricResult == Double.PositiveInfinity || metricResult == Double.NegativeInfinity || metricResult == Double.NaN){
                    metricResult = 0.0
                  }
                  resultSet = mergeUpdates(resultSet, (node.getId.drop(1) -> Map(metric -> metricResult)))
                }
              )
            }
          }
        )
      }
    )
    resultSet
  }
}

// companion object
object CalculolController {
  val validInteractions = Seq[String]("ee", "ed", "de", "em", "ea", "dm", "da", "e*", "*e", "d*", "*m", "*a", "**")
  val validIntTypes = Seq[String]("msj", "pub", "rea", "com", "vis", "men", "*")
  val validNodes = Seq[String]("e", "d", "m", "a", "*")
  val validPlatforms = Seq[String]("f", "m", "p", "c", "*")
  val actividadStatuses = Seq[String]("ec", "pe", "co", "fi", "*")

  object validMetrics extends Enumeration {
    sealed case class Metric private[Metric](shortName: String) extends Val(shortName)

    val InDegree = Metric("idc")
    val OutDegree = Metric("odc")
    val Degree = Metric("dec")
    val Betweenness = Metric("bec")
    val Closeness = Metric("clc")
    val Eccentricity = Metric("ecc")
    val PageRank = Metric("prc")
    //val _ = Metric("*")
  }
}


