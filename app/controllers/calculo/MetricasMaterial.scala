package controllers.calculo

import java.util
import javax.inject.{Inject, Singleton}

import scala.collection.JavaConverters._
import com.fasterxml.jackson.databind.ObjectMapper
import models.Curso
import org.graphstream.algorithm.PageRank
import org.graphstream.graph.{Edge, Node}
import org.graphstream.graph.implementations.SingleGraph
import play.api.mvc.{Action, Controller}

@Singleton
class MetricasMaterial @Inject()() extends Controller {

  @throws[Exception]
  def indegree(id_curso: Long) = Action {
    val objectMapper = new ObjectMapper
    val curso = Curso.find.byId(id_curso)
    val result = new util.HashMap[String, Any]
    if(curso != null){
      val metricas = scala.collection.mutable.Map[String, Int]()
      //Obtener todas las interacciones alumno-alumno
      val interacciones = curso.interaccionesEM()

      //Poblar el grafo
      var graph = new SingleGraph("Curso "+curso.getTitulo)
      graph.setStrict(false)
      graph.setAutoCreate(true)

      for(i <- 0 until interacciones.size()){
        graph.addEdge[Edge](interacciones.get(i).getString("id_interaccion"), interacciones.get(i).getString("tipo_origen")+interacciones.get(i).getString("nodo_origen"), interacciones.get(i).getString("tipo_destino")+interacciones.get(i).getString("nodo_destino"))//true
      }

      //Poblar la estructura de datos
      graph.getNodeSet[Node]
        .asScala.toArray.foreach(
        (node: Node) => {
          if(node.getId.startsWith("m")){//Solo materiales
            metricas(node.getId.drop(1)) = node.getOutDegree
          }
        }
      )

      //Ordenar los resultados
      val metricasOrdered = Seq(metricas.toSeq.sortBy(_._2):_*).map(i => i._1 -> i._2)

      result.put("metricas", metricasOrdered.toMap.asJava);
      result.put("valorMaximo", metricasOrdered(metricasOrdered.length-1)._2.toString);
      result.put("valorMinimo", metricasOrdered(0)._2.toString)
    }else{
      result.put("status", "error");
      result.put("msg", "Curso no encontrado");
    }

    Ok(objectMapper.writeValueAsString(result)).as("application/json")
  }

  @throws[Exception]
  def pagerank(id_curso: Long) = Action {
    val objectMapper = new ObjectMapper
    val curso = Curso.find.byId(id_curso)
    val result = new util.HashMap[String, AnyRef]
    if(curso != null){
      val metricas = scala.collection.mutable.Map[String, Double]()
      //Get all interacciones
      val interacciones = curso.interaccionesEM()
      //Fill graph
      var graph = new SingleGraph("Curso "+curso.getTitulo)
      graph.setStrict(false)
      graph.setAutoCreate(true)

      for(i <- 0 until interacciones.size()){
        graph.addEdge[Edge](interacciones.get(i).getString("id_interaccion"), interacciones.get(i).getString("tipo_origen")+interacciones.get(i).getString("nodo_origen"), interacciones.get(i).getString("tipo_destino")+interacciones.get(i).getString("nodo_destino"))//true
      }

      //Calculate Pagerank
      val pagerank: PageRank = new PageRank()
      pagerank.init(graph)
      pagerank.compute()

      var suma = 0.0
      //Fill datastructure
      graph.getNodeSet[Node]
        .asScala.toArray.foreach(
        (node: Node) => {
          suma += node.getAttribute[Double]("PageRank")
          if(node.getId.startsWith("m")){//Solo materiales
            metricas(node.getId.drop(1)) = node.getAttribute[Double]("PageRank")
          }
        }
      )
      System.out.println("Total pagerank:"+suma)

      val metricasOrdered = Seq(metricas.toSeq.sortBy(_._2):_*).map(i => i._1 -> i._2)

      result.put("metricas", metricasOrdered.toMap.asJava);
      result.put("valorMaximo", metricasOrdered(metricasOrdered.length-1)._2.toString);
      result.put("valorMinimo", metricasOrdered(0)._2.toString)
    }else{
      result.put("status", "error");
      result.put("msg", "Curso no encontrado");
    }

    Ok(objectMapper.writeValueAsString(result)).as("application/json")
  }

}
