package controllers

import java.util

import com.avaje.ebean.SqlRow
import com.fasterxml.jackson.databind.ObjectMapper
import controllers.calculo.CalculolController
import javax.inject.{Inject, Singleton}
import models._
import org.graphstream.graph.implementations.MultiGraph
import play.api.mvc.{Action, Controller}

import scala.concurrent.ExecutionContext.Implicits.global


@Singleton
class DocenteController @Inject()() extends Controller {

  def addUaHeader[A](action: Action[A]) = Action.async(action.parser) { request =>
    System.out.println("!!!!")
    action(request).map(_.withHeaders("X-UA-Compatibleaaaaa" -> "Chrome=1"))
  }

  @throws[Exception]
  def grafoDocente(id: Long, id_curso:  java.lang.Long = null) = Action { request =>

    val objectMapper = new ObjectMapper
    val docente = Docente.find.byId(id)
    val result = new util.HashMap[String, Any]

    if(docente != null){
      import scala.collection.JavaConverters._
      import scala.collection.JavaConversions._

      if(id_curso == null || (docente.cursos() exists (_.getLong("id_curso") == id_curso)) ) {
          var curso:Curso = null
          if (id_curso != null) curso = Curso.find.byId(id_curso)

          if(id_curso != null && curso == null ){
            result.put("code", 4)
            result.put("msg", "Curso no encontrado")
            NotFound(objectMapper.writeValueAsString(result))
          }else{
            if( id_curso != null && curso != null ){
              result.put("curso", curso)
            }else{
              result.put("cursos", docente.cursos)
            }

            var graphInt: util.List[SqlRow] = new util.ArrayList[SqlRow]()
            graphInt = docente.interacciones(id_curso, Set().toList.asJava, Set().toList.asJava, "*", "*", Set().toList.asJava)
            val cc = new CalculolController()
            val metricTypes = CalculolController.validMetrics.values.map(_.toString)
            var graph = new MultiGraph(s"DIIA")
            graph = cc.fillGraph(graph, graphInt, metricTypes)

            //Calculate metrics
            cc.computeGraph(graph, metricTypes)

            //Fill resultSet
            var resultSet = cc.graphResults(graph, Set("*"), metricTypes)

            //Delete empty values
            var nodes = docente.allNodo(id_curso)
            nodes = remBlankProp(nodes, (node) => {
              // Append metric data
              if (!resultSet.get(node.get("id").toString).isEmpty) {
                resultSet.get(node.get("id").toString).get foreach {
                  case (k, v) => node.put(k.toString, v)
                }
              }
            })

            var interacciones = docente.allInteraccion(id_curso)
            interacciones = remBlankProp(interacciones)

            result.put("nodos", nodes)
            result.put("interacciones", interacciones)

            Ok(objectMapper.writeValueAsString(result))
          }

      }else{
        result.put("code", "???")
        result.put("msg", "El curso no pertenece al docente indicado.")
        NotFound(objectMapper.writeValueAsString(result))
      }
    }else{
      result.put("code", 1)
      result.put("msg", "Docente no encontrado")
      NotFound(objectMapper.writeValueAsString(result))
    }

  }


  @throws[Exception]
  def prueba(id: Long, id_curso:  java.lang.Long = null) = Action(parse.json) { request =>

    val objectMapper = new ObjectMapper
    val docente = Docente.find.byId(id)
    val result = new util.HashMap[String, Any]

    Ok("")



  }

  /**
    * Remove rows with empty values from node list and apply a functor on each node
    * @param nodos Nodes list
    * @param fn Functor
    * @return util.List[SqlRow]
    */
  def remBlankProp(nodes: util.List[SqlRow], fn: (SqlRow) => Unit = (node) => {}) = {
    import scala.collection.JavaConversions._
    nodes.map(
      node => {
        node filter{ property => property._2==null || property._2.toString.length==0 } foreach {
          property => {
            node.remove(property._1)
          }
        }
        fn(node)
        node
      }
    )
  }



  @throws[Exception]
  def materiales(id_docente:Long) = Action {
    val objectMapper = new ObjectMapper
    val result = new util.HashMap[String, Any]

    val docente = Docente.find().byId(id_docente)
    if(docente != null){
      val materiales = docente.recursos(null)
      result.put("materiales", materiales)
      Ok(objectMapper.writeValueAsString(result)).as("application/json")
    }else{
      result.put("code", 2)
      result.put("msj", "Material no encontrado.")
      NotFound(objectMapper.writeValueAsString(result)).as("application/json")
    }

  }

  @throws[Exception]
  def materialesCurso(id_docente:Long, id_curso:Long) = Action {
    val objectMapper = new ObjectMapper
    val result = new util.HashMap[String, Any]

    val docente = Docente.find().byId(id_docente)
    if(docente != null){
      val materiales = docente.recursos(id_curso)
      result.put("materiales", materiales)
      Ok(objectMapper.writeValueAsString(result)).as("application/json")
    }else{
      result.put("code", 2)
      result.put("msj", "Material no encontrado.")
      NotFound(objectMapper.writeValueAsString(result)).as("application/json")
    }

  }
}
