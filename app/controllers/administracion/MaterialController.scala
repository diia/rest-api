package controllers.administracion

import java.lang.Number
import java.util

import com.avaje.ebean.SqlRow
import com.fasterxml.jackson.databind.ObjectMapper
import javax.inject.{Inject, Singleton}
import models._
import play.api.libs.json.{JsError, JsSuccess, Json}
import play.api.mvc.{Action, Controller}

import scala.collection.Map

@Singleton
class MaterialController @Inject()() extends Controller {

  @throws[Exception]
  def recuperar(id_material:Long) = Action {
    val objectMapper = new ObjectMapper
    val result = new util.HashMap[String, Any]

    Option(Material.find().byId(id_material)) map {
      material =>
        Option(Nodo.find.byId(id_material)) map {
          nodo =>
            import scala.collection.JavaConverters._

            val e = new EgoGraph(nodo.nodosEgoGraph().asScala.toList, nodo.intEgoGraph().asScala.toList, material.detalle().asScala.map(
              property => (property._1, property._2)
            ))

            Ok(Json.toJson(e)(EgoGraph.EgoGraph))
        }getOrElse{
          result.put("code", 2)
          result.put("msg", "Material no encontrado.")
          NotFound(objectMapper.writeValueAsString(result)).as("application/json")
        }
    }getOrElse{
      result.put("code", 2)
      result.put("msg", "Material no encontrado.")
      NotFound(objectMapper.writeValueAsString(result)).as("application/json")
    }


  }

  @throws[Exception]
  def crear() = Action { request =>
    val bodyAsJson = request.body.asJson.get
    val objectMapper = new ObjectMapper
    val result = new util.HashMap[String, Any]

    bodyAsJson.validate[NuevoMaterial] match {
      case success: JsSuccess[NuevoMaterial] => {
        Material.nuevo(success.get.id_curso, success.get.plataforma, success.get.nombre, success.get.tipo, success.get.tipo_contenido, success.get.fecha_publicacion, success.get.numero_accesos, success.get.url_ubicacion)
        result.put("estado", "ok")
        Ok(objectMapper.writeValueAsString(result)).as("application/json")
      }
      case JsError(error) => {
        result.put("code", 20)
        result.put("msg", "Petición inválida.")
        BadRequest(objectMapper.writeValueAsString(result)).as("application/json")
      }
    }
  }

  @throws[Exception]
  def actualizar() = Action { request =>
    val bodyAsJson = request.body.asJson.get
    val objectMapper = new ObjectMapper
    val result = new util.HashMap[String, Any]

    bodyAsJson.validate[ActualizarMaterial] match {
      case success: JsSuccess[ActualizarMaterial] => {
        Material.actualizar(success.get.id_material, success.get.id_curso, success.get.plataforma, success.get.nombre, success.get.tipo, success.get.tipo_contenido, success.get.fecha_publicacion, success.get.numero_accesos, success.get.url_ubicacion)
        result.put("estado", "ok")
        Ok(objectMapper.writeValueAsString(result)).as("application/json")
      }
      case JsError(error) => {
        result.put("code", 20)
        result.put("msg", "Petición inválida.")
        BadRequest(objectMapper.writeValueAsString(result)).as("application/json")
      }
    }
  }

  @throws[Exception]
  def eliminar(id:Long) = Action { request =>
    val objectMapper = new ObjectMapper
    val result = new util.HashMap[String, Any]
    val obj = Material.find().byId(id)
    if(obj != null){
      Material.eliminar(id)
      result.put("estado", "ok")
      Ok(objectMapper.writeValueAsString(result)).as("application/json")
    }else{
      result.put("code", 2)
      result.put("msg", "Material no encontrado.")
      NotFound(objectMapper.writeValueAsString(result)).as("application/json")
    }
  }


  import play.api.libs.functional.syntax._
  import play.api.libs.json.Reads._
  import play.api.libs.json._

  case class NuevoMaterial(id_curso: Long,
                           plataforma: String,
                           nombre: String,
                           tipo: String,
                           tipo_contenido: String,
                           fecha_publicacion: String,
                           numero_accesos: Int,
                           url_ubicacion: String)
  object NuevoMaterial {
    implicit val reads: Reads[NuevoMaterial] = (
      (JsPath \ "id_curso").read[Long] and
        (JsPath \ "plataforma").read[String](minLength[String](1) andKeep maxLength[String](2)) and
        (JsPath \ "nombre").read[String](minLength[String](1) andKeep maxLength[String](150)) and
        (JsPath \ "tipo").read[String](minLength[String](1) andKeep maxLength[String](2)) and
        (JsPath \ "tipo_contenido").read[String](minLength[String](1) andKeep maxLength[String](3)) and
        (JsPath \ "fecha_publicacion").read[String](pattern("""[0-9]{4,4}-[0-9]{2,2}-[0-9]{2,2}"""r)) and
        (JsPath \ "numero_accesos").read[Int] and
        (JsPath \ "url_ubicacion").read[String])(NuevoMaterial.apply _)
  }

  case class ActualizarMaterial( id_material: Long,
                                 id_curso: Long,
                                 plataforma: String,
                                 nombre: String,
                                 tipo: String,
                                 tipo_contenido: String,
                                 fecha_publicacion: String,
                                 numero_accesos: Int,
                                 url_ubicacion: String)
  object ActualizarMaterial {
    implicit val reads: Reads[ActualizarMaterial] = (
      (JsPath \ "id_material").read[Long] and
      (JsPath \ "id_curso").read[Long] and
        (JsPath \ "plataforma").read[String](minLength[String](1) andKeep maxLength[String](2)) and
        (JsPath \ "nombre").read[String](minLength[String](1) andKeep maxLength[String](150)) and
        (JsPath \ "tipo").read[String](minLength[String](1) andKeep maxLength[String](2)) and
        (JsPath \ "tipo_contenido").read[String](minLength[String](1) andKeep maxLength[String](3)) and
        (JsPath \ "fecha_publicacion").read[String](pattern("""[0-9]{4,4}-[0-9]{2,2}-[0-9]{2,2}"""r)) and
        (JsPath \ "numero_accesos").read[Int] and
        (JsPath \ "url_ubicacion").read[String])(ActualizarMaterial.apply _)
  }

  case class EgoGraph(nodes: List[SqlRow], interactions: List[SqlRow], properties: Map[String, AnyRef])

  import play.api.libs.json._
  import play.api.libs.json.Json._

  object EgoGraph{
    import play.api.libs.json._
    import scala.collection.JavaConverters._

    implicit val EgoGraph = new Writes[EgoGraph] {
      def writes(eg: EgoGraph): JsValue = {
        Json.toJson(
          eg.properties.foldLeft(Json.obj())((obj, row) =>
            obj ++ Json.obj(row._1 -> (row._2 match {
              case n:Number => BigDecimal(row._2.toString)
              case _ => row._2.toString
            })
            )
              //(Json.obj(row._1 -> row._2))
          ).++(
            obj(
              "egoGrafo" -> obj(
                "nodos" -> eg.nodes.map(
                  row =>
                    obj(
                      "id" -> BigDecimal(row.getLong("id")),
                      "tipo" -> row.getString("tipo"),
                      "nombre" -> row.getString("nombre")
                    )
                ),
                "interacciones" -> eg.interactions.map(
                  row =>
                    obj(
                      "id_interaccion" -> BigDecimal(row.getLong("id_interaccion")),
                      "origen" -> row.getString("origen"),
                      "origen_tipo" -> row.getString("origen_tipo"),
                      "destino" -> row.getString("destino"),
                      "destino_tipo" -> row.getString("destino_tipo"),
                      "tipo" -> row.getString("tipo")
                    )
                )
              )
            )
          )
        )

      }
    }
  }
}
