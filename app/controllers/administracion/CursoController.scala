package controllers.administracion

import java.util

import com.fasterxml.jackson.databind.ObjectMapper
import javax.inject.{Inject, Singleton}
import models._
import play.api.libs.json.{JsError, JsSuccess}
import play.api.mvc.{Action, Controller}


@Singleton
class CursoController @Inject()() extends Controller {
  @throws[Exception]
  def recuperar(id:Long) = Action {
    val objectMapper = new ObjectMapper
    val curso = Curso.find().byId(id)
    val result = new util.HashMap[String, Any]

    if(curso != null){
      Ok(objectMapper.writeValueAsString(curso)).as("application/json")
    }else{
      result.put("code", 4)
      result.put("msg", "Curso no encontrado.")
      NotFound(objectMapper.writeValueAsString(result)).as("application/json")
    }
  }

  @throws[Exception]
  def crear() = Action { request =>
    val bodyAsJson = request.body.asJson.get
    val objectMapper = new ObjectMapper
    val result = new util.HashMap[String, Any]

    bodyAsJson.validate[NuevoCurso] match {
      case success: JsSuccess[NuevoCurso] => {
        Curso.nuevoCurso(success.get.titulo, success.get.codigo, success.get.seccion, success.get.periodo_inicio, success.get.periodo_fin, success.get.id_escuela, success.get.id_facebook, success.get.id_pam, success.get.id_crea, success.get.id_moodle)
        result.put("estado", "ok")
        Ok(objectMapper.writeValueAsString(result)).as("application/json")
      }
      case JsError(error) => {
        result.put("code", 20)
        result.put("msg", "Petición inválida.")
        BadRequest(objectMapper.writeValueAsString(result)).as("application/json")
      }
    }
  }

  @throws[Exception]
  def actualizar() = Action { request =>
    val bodyAsJson = request.body.asJson.get
    val objectMapper = new ObjectMapper
    val result = new util.HashMap[String, Any]

    bodyAsJson.validate[ActualizarCurso] match {
      case success: JsSuccess[ActualizarCurso] => {
        Curso.actualizarCurso(success.get.id_curso, success.get.titulo, success.get.codigo, success.get.seccion, success.get.periodo_inicio, success.get.periodo_fin, success.get.id_escuela, success.get.id_facebook, success.get.id_pam, success.get.id_crea, success.get.id_moodle)
        result.put("estado", "ok")
        Ok(objectMapper.writeValueAsString(result)).as("application/json")
      }
      case JsError(error) => {
        result.put("code", 20)
        result.put("msg", "Petición inválida.")
        BadRequest(objectMapper.writeValueAsString(result)).as("application/json")
      }
    }
  }

  @throws[Exception]
  def eliminar(id:Int) = Action { request =>
    val objectMapper = new ObjectMapper
    val result = new util.HashMap[String, Any]
    val obj = Curso.find().byId(id.asInstanceOf[Long])
    if(obj != null){
      Curso.eliminarCurso(id)
      result.put("estado", "ok")
      Ok(objectMapper.writeValueAsString(result)).as("application/json")
    }else{
      result.put("code", 4)
      result.put("msg", "Curso no encontrado.")
      NotFound(objectMapper.writeValueAsString(result)).as("application/json")
    }
  }


  import play.api.libs.functional.syntax._
  import play.api.libs.json.Reads._
  import play.api.libs.json._

  case class NuevoCurso(titulo: String,
                        codigo: String,
                        seccion: String,
                        periodo_inicio: String,
                        periodo_fin: String,
                        id_escuela: Int,
                        id_facebook: String,
                        id_pam: String,
                        id_crea: String,
                        id_moodle: String)
  object NuevoCurso {
    implicit val reads: Reads[NuevoCurso] = (
      (JsPath \ "titulo").read[String](minLength[String](1) andKeep maxLength[String](100)) and
        (JsPath \ "codigo").read[String](minLength[String](1) andKeep maxLength[String](20)) and
        (JsPath \ "seccion").read[String](minLength[String](1) andKeep maxLength[String](20)) and
        (JsPath \ "periodo_inicio").read[String](pattern("""[0-9]{4,4}-[0-9]{2,2}-[0-9]{2,2}"""r)) and
        (JsPath \ "periodo_fin").read[String](pattern("""[0-9]{4,4}-[0-9]{2,2}-[0-9]{2,2}"""r)) and
        (JsPath \ "id_escuela").read[Int] and
        (JsPath \ "id_facebook").read[String](minLength[String](1) andKeep maxLength[String](20)) and
        (JsPath \ "id_pam").read[String](minLength[String](1) andKeep maxLength[String](20)) and
        (JsPath \ "id_crea").read[String](minLength[String](1) andKeep maxLength[String](20)) and
        (JsPath \ "id_moodle").read[String](minLength[String](1) andKeep maxLength[String](20)))(NuevoCurso.apply _)
  }

  case class ActualizarCurso( id_curso: Int,
                              titulo: String,
                               codigo: String,
                               seccion: String,
                               periodo_inicio: String,
                               periodo_fin: String,
                               id_escuela: Int,
                               id_facebook: String,
                               id_pam: String,
                               id_crea: String,
                               id_moodle: String)
  object ActualizarCurso {
    implicit val reads: Reads[ActualizarCurso] = (
      (JsPath \ "id_curso").read[Int] and
        (JsPath \ "titulo").read[String](minLength[String](1) andKeep maxLength[String](100)) and
        (JsPath \ "codigo").read[String](minLength[String](1) andKeep maxLength[String](20)) and
        (JsPath \ "seccion").read[String](minLength[String](1) andKeep maxLength[String](20)) and
        (JsPath \ "periodo_inicio").read[String](pattern("""[0-9]{4,4}-[0-9]{2,2}-[0-9]{2,2}"""r)) and
        (JsPath \ "periodo_fin").read[String](pattern("""[0-9]{4,4}-[0-9]{2,2}-[0-9]{2,2}"""r)) and
        (JsPath \ "id_escuela").read[Int] and
        (JsPath \ "id_facebook").read[String](minLength[String](1) andKeep maxLength[String](20)) and
        (JsPath \ "id_pam").read[String](minLength[String](1) andKeep maxLength[String](20)) and
        (JsPath \ "id_crea").read[String](minLength[String](1) andKeep maxLength[String](20)) and
        (JsPath \ "id_moodle").read[String](minLength[String](1) andKeep maxLength[String](20)))(ActualizarCurso.apply _)
  }
}




