package controllers.administracion

import java.util

import com.fasterxml.jackson.databind.ObjectMapper
import javax.inject.{Inject, Singleton}
import models._
import play.api.data.validation.ValidationError
import play.api.libs.json.{JsError, JsPath, JsSuccess, Reads}
import play.api.mvc.{Action, Controller}



@Singleton
class EscuelaController @Inject()() extends Controller {

  @throws[Exception]
  def recuperar(id_escuela:Long) = Action {
    val objectMapper = new ObjectMapper
    val escuela = Escuela.find().byId(id_escuela)
    val result = new util.HashMap[String, Any]

    if(escuela != null){
      Ok(objectMapper.writeValueAsString(escuela)).as("application/json")
    }else{
      result.put("code", 6)
      result.put("msg", "Escuela no encontrada")
      NotFound(objectMapper.writeValueAsString(result)).as("application/json")
    }
  }

  @throws[Exception]
  def crear() = Action { request =>
    val bodyAsJson = request.body.asJson.get
    val objectMapper = new ObjectMapper
    val result = new util.HashMap[String, Any]

    bodyAsJson.validate[NuevaEscuela] match {
      case success: JsSuccess[NuevaEscuela] => {
        Escuela.nuevaEscuela(success.get.nombre, success.get.direccion, success.get.ciudad, success.get.region, success.get.latitud, success.get.longitud)
        result.put("estado", "ok")
        Ok(objectMapper.writeValueAsString(result)).as("application/json")
      }
      case JsError(error) => {
        result.put("code", 20)
        result.put("msg", "Petición inválida.")
        BadRequest(objectMapper.writeValueAsString(result)).as("application/json")
      }
    }
  }

  @throws[Exception]
  def actualizar() = Action { request =>
    val bodyAsJson = request.body.asJson.get
    val objectMapper = new ObjectMapper
    val result = new util.HashMap[String, Any]

    bodyAsJson.validate[ActualizarEscuela] match {
      case success: JsSuccess[ActualizarEscuela] => {
        Escuela.actualizarEscuela(success.get.id_escuela, success.get.nombre, success.get.direccion, success.get.ciudad, success.get.region, success.get.latitud, success.get.longitud)
        result.put("estado", "ok")
        Ok(objectMapper.writeValueAsString(result)).as("application/json")
      }
      case JsError(error) => {
        result.put("code", 20)
        result.put("msg", "Petición inválida.")
        BadRequest(objectMapper.writeValueAsString(result)).as("application/json")
      }
    }
  }

  @throws[Exception]
  def eliminar(id_escuela:Int) = Action { request =>
    val objectMapper = new ObjectMapper
    val result = new util.HashMap[String, Any]
    //val id_escuela = request.body.asFormUrlEncoded.get.get("id_escuela").mkString
    val escuela = Escuela.find().byId(id_escuela.asInstanceOf[Long])
    if(escuela != null){
      Escuela.eliminarEscuela(id_escuela)
      result.put("estado", "ok")
      Ok(objectMapper.writeValueAsString(result)).as("application/json")
    }else{
      result.put("code", 6)
      result.put("msg", "Escuela no encontrada")
      NotFound(objectMapper.writeValueAsString(result)).as("application/json")
    }
  }


  /*
      if(escuela != null){
        Ok(objectMapper.writeValueAsString(escuela))
      }else{
        result.put("code", 1)
        result.put("msg", "Identificador de escuela no válido")
        NotFound(objectMapper.writeValueAsString(result))
      }
  */

  import play.api.libs.json._
  import play.api.libs.functional.syntax._
  import play.api.libs.json.Reads._

  case class NuevaEscuela(nombre: String,
                          direccion: String,
                          ciudad: String,
                          region: String,
                          latitud: Double,
                          longitud: Double)
  object NuevaEscuela {
    implicit val reads: Reads[NuevaEscuela] = (
      (JsPath \ "nombre").read[String](minLength[String](1) andKeep maxLength[String](150)) and
        (JsPath \ "direccion").read[String](minLength[String](1) andKeep maxLength[String](150)) and
        (JsPath \ "ciudad").read[String](minLength[String](1) andKeep maxLength[String](50)) and
        (JsPath \ "region").read[String](minLength[String](1) andKeep maxLength[String](50)) and
        (JsPath \ "latitud").read[Double] and
        (JsPath \ "longitud").read[Double])(NuevaEscuela.apply _)
  }

  case class ActualizarEscuela(id_escuela: Int, nombre: String,
                               direccion: String,
                               ciudad: String,
                               region: String,
                               latitud: Double,
                               longitud: Double)
  object ActualizarEscuela {
    implicit val reads: Reads[ActualizarEscuela] = (
      (JsPath \ "id_escuela").read[Int] and
        (JsPath \ "nombre").read[String](minLength[String](1) andKeep maxLength[String](150)) and
        (JsPath \ "direccion").read[String](minLength[String](1) andKeep maxLength[String](150)) and
        (JsPath \ "ciudad").read[String](minLength[String](1) andKeep maxLength[String](50)) and
        (JsPath \ "region").read[String](minLength[String](1) andKeep maxLength[String](50)) and
        (JsPath \ "latitud").read[Double] and
        (JsPath \ "longitud").read[Double])(ActualizarEscuela.apply _)
  }
}




