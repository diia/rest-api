package controllers.administracion

import java.util

import com.avaje.ebean.SqlRow
import com.fasterxml.jackson.databind.ObjectMapper
import javax.inject.{Inject, Singleton}
import models._
import play.api.libs.json.{JsError, JsSuccess, Json}
import play.api.mvc.{Action, Controller}

import scala.collection.Map

@Singleton
class ActividadController @Inject()() extends Controller {

  @throws[Exception]
  def recuperar(id_actividad:Long) = Action {
    val objectMapper = new ObjectMapper
    val actividad = Actividad.find().byId(id_actividad)
    val result = new util.HashMap[String, Any]

    Option(Actividad.find().byId(id_actividad)) map {
      actividad =>
        Option(Nodo.find.byId(id_actividad)) map {
          nodo =>
            import scala.collection.JavaConverters._

            val e = new EgoGraph(nodo.nodosEgoGraph().asScala.toList, nodo.intEgoGraph().asScala.toList, actividad.detalle().asScala.map(
              property => (property._1, property._2)
            ), actividad.resultados.asScala.toList)

            Ok(Json.toJson(e)(EgoGraph.EgoGraph))
        }getOrElse{
          result.put("code", 5)
          result.put("msg", "Actividad no encontrada.")
          NotFound(objectMapper.writeValueAsString(result)).as("application/json")
        }
    }getOrElse{
      result.put("code", 5)
      result.put("msg", "Actividad no encontrada.")
      NotFound(objectMapper.writeValueAsString(result)).as("application/json")
    }
  }

  @throws[Exception]
  def crear() = Action { request =>
    val bodyAsJson = request.body.asJson.get
    val objectMapper = new ObjectMapper
    val result = new util.HashMap[String, Any]

    bodyAsJson.validate[NuevaActividad] match {
      case success: JsSuccess[NuevaActividad] => {
        Actividad.nuevo(success.get.tipo, success.get.fecha_limite, success.get.calificacion_minima, success.get.calificacion_maxima)
        result.put("estado", "ok")
        Ok(objectMapper.writeValueAsString(result)).as("application/json")
      }
      case JsError(error) => {
        result.put("code", 20)
        result.put("msg", "Petición inválida.")
        BadRequest(objectMapper.writeValueAsString(result)).as("application/json")
      }
    }
  }

  @throws[Exception]
  def actualizar() = Action { request =>
    val bodyAsJson = request.body.asJson.get
    val objectMapper = new ObjectMapper
    val result = new util.HashMap[String, Any]

    bodyAsJson.validate[ActualizarActividad] match {
      case success: JsSuccess[ActualizarActividad] => {
        Actividad.actualizar(success.get.id_actividad, success.get.tipo, success.get.fecha_limite, success.get.calificacion_minima, success.get.calificacion_maxima)
        result.put("estado", "ok")
        Ok(objectMapper.writeValueAsString(result)).as("application/json")
      }
      case JsError(error) => {
        result.put("code", 20)
        result.put("msg", "Petición inválida.")
        BadRequest(objectMapper.writeValueAsString(result)).as("application/json")
      }
    }
  }

  @throws[Exception]
  def eliminar(id:Long) = Action { request =>
    val objectMapper = new ObjectMapper
    val result = new util.HashMap[String, Any]
    val obj = Actividad.find().byId(id)
    if(obj != null){
      Actividad.eliminar(id)
      result.put("estado", "ok")
      Ok(objectMapper.writeValueAsString(result)).as("application/json")
    }else{
      result.put("code", 5)
      result.put("msg", "Actividad no encontrada.")
      NotFound(objectMapper.writeValueAsString(result)).as("application/json")
    }
  }


  import play.api.libs.functional.syntax._
  import play.api.libs.json.Reads._
  import play.api.libs.json._

  case class NuevaActividad(tipo: String,
                            fecha_limite: String,
                            calificacion_minima: Double,
                            calificacion_maxima: Double)

  object NuevaActividad {
    implicit val reads: Reads[NuevaActividad] = (
      (JsPath \ "tipo").read[String](minLength[String](1) andKeep maxLength[String](2)) and
        (JsPath \ "fecha_limite").read[String](pattern("""[0-9]{4,4}-[0-9]{2,2}-[0-9]{2,2}"""r)) and
        (JsPath \ "calificacion_minima").read[Double] and
        (JsPath \ "calificacion_maxima").read[Double])(NuevaActividad.apply _)
  }

  case class ActualizarActividad( id_actividad: Long,
                                  tipo: String,
                                  fecha_limite: String,
                                  calificacion_minima: Double,
                                  calificacion_maxima: Double)
  object ActualizarActividad {
    implicit val reads: Reads[ActualizarActividad] = (
      (JsPath \ "id_actividad").read[Long] and
      (JsPath \ "tipo").read[String](minLength[String](1) andKeep maxLength[String](2)) and
        (JsPath \ "fecha_limite").read[String](pattern("""[0-9]{4,4}-[0-9]{2,2}-[0-9]{2,2}"""r)) and
        (JsPath \ "calificacion_minima").read[Double] and
        (JsPath \ "calificacion_maxima").read[Double])(ActualizarActividad.apply _)
  }

  case class EgoGraph(nodes: List[SqlRow], interactions: List[SqlRow], properties: Map[String, AnyRef],  resultados: List[SqlRow])

  import play.api.libs.json._
  import play.api.libs.json.Json._

  object EgoGraph{
    import play.api.libs.json._
    import scala.collection.JavaConverters._

    implicit val EgoGraph = new Writes[EgoGraph] {
      def writes(eg: EgoGraph): JsValue =

        Json.toJson(
          eg.properties.foldLeft(Json.obj())((obj, row) =>
            obj ++ Json.obj(row._1 -> (row._2 match {
              case n:Number => BigDecimal(n.toString)
              case _ => row._2.toString
            })
            )
          ).++(
            obj(
              "egoGrafo" -> obj(
                "nodos" -> eg.nodes.toList.map(
                  row =>
                    obj(
                      "id" -> BigDecimal(row.getLong("id")),
                      "tipo" -> row.getString("tipo"),
                      "nombre" -> row.getString("nombre")
                    )
                ),
                "interacciones" -> eg.interactions.toList.map(
                  row =>
                    obj(
                      "id_interaccion" -> BigDecimal(row.getLong("id_interaccion")),
                      "origen" -> row.getString("origen"),
                      "origen_tipo" -> row.getString("origen_tipo"),
                      "destino" -> row.getString("destino"),
                      "destino_tipo" -> row.getString("destino_tipo"),
                      "tipo" -> row.getString("tipo")
                    )
                ),
                "resultados" -> eg.resultados.toList.map(
                  row =>
                    obj(
                      "id_estudiante" -> BigDecimal(row.getLong("id_estudiante")),
                      "nombre" -> row.getString("nombre"),
                      "nota" -> row.getString("nota"),
                      "fecha_entrega" -> row.getString("fecha_entrega")
                    )
                )
              )
            )
          )
        )



      /*
      "nodos" -> eg.nodes.toList.map(
              row =>
                obj(
                  "id" -> BigDecimal(row.getLong("id")),
                  "tipo" -> row.getString("tipo"),
                  "nombre" -> row.getString("nombre")
                )
            ),
       */
    }


    implicit val prob = new Writes[Map[String, AnyVal]] {
      def writes(eg: Map[String, AnyVal]): JsValue =

          Json.toJson(
            obj(
              "fields" -> ""
            ).++(
            obj("field2"->""
            )
            )
          )

    }
  }
}
