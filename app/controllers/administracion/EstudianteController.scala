package controllers.administracion

import java.util

import com.avaje.ebean.SqlRow
import com.fasterxml.jackson.databind.ObjectMapper
import javax.inject.{Inject, Singleton}
import models._
import play.api.libs.json.{JsError, JsSuccess, Json}
import play.api.mvc.{Action, Controller}

@Singleton
class EstudianteController @Inject()() extends Controller {
  @throws[Exception]
  def recuperar(id:Long) = Action {
    val objectMapper = new ObjectMapper
    val obj = Estudiante.find().byId(id)
    val result = new util.HashMap[String, Any]

    Option(Estudiante.find().byId(id)) map {
      estudiante =>
        Option(Nodo.find.byId(id)) map {
          nodo =>
            import scala.collection.JavaConverters._

            var prop = ( estudiante.detalle().asScala.map(
              property => (property._1, property._2)
            ) + ("trabaja"-> (estudiante.trabajos().size() > 0) ) )
            prop.remove("localidad")

            val e = new EgoGraph(nodo.nodosEgoGraph().asScala.toList, nodo.intEgoGraph().asScala.toList, prop)

            Ok(Json.toJson(e)(EgoGraph.EgoGraph))
        }getOrElse{
          result.put("code", 3)
          result.put("msj", "Estudiante no encontrado.")
          NotFound(objectMapper.writeValueAsString(result)).as("application/json")
        }
    }getOrElse{
      result.put("code", 3)
      result.put("msj", "Estudiante no encontrado.")
      NotFound(objectMapper.writeValueAsString(result)).as("application/json")
    }

  }

  @throws[Exception]
  def crear() = Action { request =>
    val bodyAsJson = request.body.asJson.get
    val objectMapper = new ObjectMapper
    val result = new util.HashMap[String, Any]

    bodyAsJson.validate[NuevoEstudiante] match {
      case success: JsSuccess[NuevoEstudiante] => {
        Estudiante.nuevoEstudiante(success.get.cedula, success.get.nombre, success.get.apellidos, success.get.fecha_nacimiento, success.get.sexo, success.get.promedio_vigente, success.get.promedio_acumulado, success.get.correo, success.get.telefono, success.get.institucion, success.get.id_facebook,success.get.id_pam,success.get.id_crea,success.get.id_moodle,success.get.ciudad_uno,success.get.localidad_uno,success.get.domicilio_uno,success.get.ciudad_dos,success.get.localidad_dos,success.get.domicilio_dos)
        result.put("estado", "ok")
        Ok(objectMapper.writeValueAsString(result)).as("application/json")
      }
      case JsError(error) => {
        result.put("code", 20)
        result.put("msg", "Petición inválida.")
        BadRequest(objectMapper.writeValueAsString(result)).as("application/json")
      }
    }
  }

  @throws[Exception]
  def actualizar() = Action { request =>
    val bodyAsJson = request.body.asJson.get
    val objectMapper = new ObjectMapper
    val result = new util.HashMap[String, Any]

    bodyAsJson.validate[ActualizarEstudiante] match {
      case success: JsSuccess[ActualizarEstudiante] => {
        Estudiante.actualizarEstudiante(success.get.id_estudiante, success.get.cedula, success.get.nombre, success.get.apellidos, success.get.fecha_nacimiento, success.get.sexo, success.get.promedio_vigente, success.get.promedio_acumulado, success.get.correo, success.get.telefono, success.get.institucion, success.get.id_facebook,success.get.id_pam,success.get.id_crea,success.get.id_moodle,success.get.ciudad_uno,success.get.localidad_uno,success.get.domicilio_uno,success.get.ciudad_dos,success.get.localidad_dos,success.get.domicilio_dos)
        result.put("estado", "ok")
        Ok(objectMapper.writeValueAsString(result)).as("application/json")
      }
      case JsError(error) => {
        result.put("code", 20)
        result.put("msg", "Petición inválida.")
        BadRequest(objectMapper.writeValueAsString(result)).as("application/json")
      }
    }
  }

  @throws[Exception]
  def eliminar(id:Int) = Action { request =>
    val objectMapper = new ObjectMapper
    val result = new util.HashMap[String, Any]
    val obj = Estudiante.find().byId(id.asInstanceOf[Long])
    if(obj != null){
      Estudiante.eliminarEstudiante(id)
      result.put("estado", "ok")
      Ok(objectMapper.writeValueAsString(result)).as("application/json")
    }else{
      result.put("code", 3)
      result.put("msj", "Estudiante no encontrado.")
      NotFound(objectMapper.writeValueAsString(result)).as("application/json")
    }
  }


  import play.api.libs.functional.syntax._
  import play.api.libs.json.Reads._
  import play.api.libs.json._

  case class NuevoEstudiante(cedula: String,
                             nombre: String,
                             apellidos: String,
                             fecha_nacimiento: String,
                             sexo: String,
                             promedio_vigente: Double,
                             promedio_acumulado: Double,
                             correo: String,
                             telefono: String,
                             institucion: String,
                             id_facebook: String,
                             id_pam: String,
                             id_crea: String,
                             id_moodle: String,
                             ciudad_uno: String,
                             localidad_uno: String,
                             domicilio_uno: String,
                             ciudad_dos: String,
                             localidad_dos: String,
                             domicilio_dos: String)
  object NuevoEstudiante {
    implicit val reads: Reads[NuevoEstudiante] = (
      (JsPath \ "cedula").read[String](minLength[String](1) andKeep maxLength[String](10)) and
        (JsPath \ "nombre").read[String](minLength[String](1) andKeep maxLength[String](150)) and
        (JsPath \ "apellidos").read[String](minLength[String](1) andKeep maxLength[String](150)) and
        (JsPath \ "fecha_nacimiento").read[String](pattern("""[0-9]{4,4}-[0-9]{2,2}-[0-9]{2,2}"""r)) and
        (JsPath \ "sexo").read[String](minLength[String](1) andKeep maxLength[String](1)) and
        (JsPath \ "promedio_vigente").read[Double] and
        (JsPath \ "promedio_acumulado").read[Double] and
        (JsPath \ "correo").read[String](minLength[String](1) andKeep maxLength[String](100)) and
        (JsPath \ "telefono").read[String](minLength[String](1) andKeep maxLength[String](13)) and
        (JsPath \ "institucion").read[String](minLength[String](1) andKeep maxLength[String](100)) and
        (JsPath \ "id_facebook").read[String](minLength[String](1) andKeep maxLength[String](20)) and
        (JsPath \ "id_pam").read[String](minLength[String](1) andKeep maxLength[String](20)) and
        (JsPath \ "id_crea").read[String](minLength[String](1) andKeep maxLength[String](20)) and
        (JsPath \ "id_moodle").read[String](minLength[String](1) andKeep maxLength[String](20)) and
        (JsPath \ "ciudad_uno").read[String](minLength[String](1) andKeep maxLength[String](150)) and
        (JsPath \ "localidad_uno").read[String](minLength[String](1) andKeep maxLength[String](150)) and
        (JsPath \ "domicilio_uno").read[String](minLength[String](1) andKeep maxLength[String](150)) and
        (JsPath \ "ciudad_dos").read[String](minLength[String](1) andKeep maxLength[String](150)) and
        (JsPath \ "localidad_dos").read[String](minLength[String](1) andKeep maxLength[String](150)) and
        (JsPath \ "domicilio_dos").read[String](minLength[String](1) andKeep maxLength[String](150)))(NuevoEstudiante.apply _)
  }

  case class ActualizarEstudiante( id_estudiante: Long,
                                   cedula: String,
                                   nombre: String,
                                   apellidos: String,
                                   fecha_nacimiento: String,
                                   sexo: String,
                                   promedio_vigente: Double,
                                   promedio_acumulado: Double,
                                   correo: String,
                                   telefono: String,
                                   institucion: String,
                                   id_facebook: String,
                                   id_pam: String,
                                   id_crea: String,
                                   id_moodle: String,
                                   ciudad_uno: String,
                                   localidad_uno: String,
                                   domicilio_uno: String,
                                   ciudad_dos: String,
                                   localidad_dos: String,
                                   domicilio_dos: String)
  object ActualizarEstudiante {
    implicit val reads: Reads[ActualizarEstudiante] = (
      (JsPath \ "id_estudiante").read[Long] and
      (JsPath \ "cedula").read[String](minLength[String](1) andKeep maxLength[String](10)) and
        (JsPath \ "nombre").read[String](minLength[String](1) andKeep maxLength[String](150)) and
        (JsPath \ "apellidos").read[String](minLength[String](1) andKeep maxLength[String](150)) and
        (JsPath \ "fecha_nacimiento").read[String](pattern("""[0-9]{4,4}-[0-9]{2,2}-[0-9]{2,2}"""r)) and
        (JsPath \ "sexo").read[String](minLength[String](1) andKeep maxLength[String](1)) and
        (JsPath \ "promedio_vigente").read[Double] and
        (JsPath \ "promedio_acumulado").read[Double] and
        (JsPath \ "correo").read[String](minLength[String](1) andKeep maxLength[String](100)) and
        (JsPath \ "telefono").read[String](minLength[String](1) andKeep maxLength[String](13)) and
        (JsPath \ "institucion").read[String](minLength[String](1) andKeep maxLength[String](100)) and
        (JsPath \ "id_facebook").read[String](minLength[String](1) andKeep maxLength[String](20)) and
        (JsPath \ "id_pam").read[String](minLength[String](1) andKeep maxLength[String](20)) and
        (JsPath \ "id_crea").read[String](minLength[String](1) andKeep maxLength[String](20)) and
        (JsPath \ "id_moodle").read[String](minLength[String](1) andKeep maxLength[String](20)) and
        (JsPath \ "ciudad_uno").read[String](minLength[String](1) andKeep maxLength[String](150)) and
        (JsPath \ "localidad_uno").read[String](minLength[String](1) andKeep maxLength[String](150)) and
        (JsPath \ "domicilio_uno").read[String](minLength[String](1) andKeep maxLength[String](150)) and
        (JsPath \ "ciudad_dos").read[String](minLength[String](1) andKeep maxLength[String](150)) and
        (JsPath \ "localidad_dos").read[String](minLength[String](1) andKeep maxLength[String](150)) and
        (JsPath \ "domicilio_dos").read[String](minLength[String](1) andKeep maxLength[String](150)))(ActualizarEstudiante.apply _)
  }

  import com.avaje.ebean.SqlRow
  import scala.collection.Map

  case class EgoGraph(nodes: List[SqlRow], interactions: List[SqlRow], properties: Map[String, Any])

  import play.api.libs.json._
  import play.api.libs.json.Json._

  object EgoGraph{
    import play.api.libs.json._
    import scala.collection.JavaConverters._

    //writter of Anyval

    implicit val EgoGraph = new Writes[EgoGraph] {
      def writes(eg: EgoGraph): JsValue = {
        Json.toJson(
            eg.properties.foldLeft(Json.obj())((obj, row) =>
              obj ++ Json.obj(row._1 -> (row._2 match {
                case n:Number => BigDecimal(n.toString)
                case b:Boolean => b
                case _ => row._2.toString
              })
              )
            ).++(
              obj(
                "egoGrafo" -> obj(
                  "nodos" -> eg.nodes.map(
                    row =>
                      obj(
                        "id" -> BigDecimal(row.getLong("id")),
                        "tipo" -> row.getString("tipo"),
                        "nombre" -> row.getString("nombre")
                      )
                  ),
                  "interacciones" -> eg.interactions.map(
                    row =>
                      obj(
                        "id_interaccion" -> BigDecimal(row.getLong("id_interaccion")),
                        "origen" -> row.getString("origen"),
                        "origen_tipo" -> row.getString("origen_tipo"),
                        "destino" -> row.getString("destino"),
                        "destino_tipo" -> row.getString("destino_tipo"),
                        "tipo" -> row.getString("tipo")
                      )
                  )
                )
              )
          )
        )

      }
    }


  }
}




