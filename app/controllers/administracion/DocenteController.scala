package controllers.administracion

import java.util

import com.fasterxml.jackson.databind.ObjectMapper
import javax.inject.{Inject, Singleton}
import models._
import play.api.libs.json.{JsError, JsSuccess, Json}
import play.api.mvc.{Action, Controller}


@Singleton
class DocenteController @Inject()() extends Controller {


  @throws[Exception]
  def recuperar(id:Long) = Action {
    val objectMapper = new ObjectMapper
    val result = new util.HashMap[String, Any]

    Option(Docente.find().byId(id)) map {
      docente =>
        Option(Nodo.find.byId(id)) map {
          nodo =>
            import scala.collection.JavaConverters._

            val e = new EgoGraph(nodo.nodosEgoGraph().asScala.toList, nodo.intEgoGraph().asScala.toList, docente.detalle().asScala.map(
              property => (property._1, property._2)
            ))

            Ok(Json.toJson(e)(EgoGraph.EgoGraph))
        }getOrElse{
          result.put("code", 1)
          result.put("msg", "Docente no encontrado")
          NotFound(objectMapper.writeValueAsString(result)).as("application/json")
        }
    }getOrElse{
      result.put("code", 1)
      result.put("msg", "Docente no encontrado")
      NotFound(objectMapper.writeValueAsString(result)).as("application/json")
    }
  }

  @throws[Exception]
  def crear() = Action { request =>
    val bodyAsJson = request.body.asJson.get
    val objectMapper = new ObjectMapper
    val result = new util.HashMap[String, Any]

    bodyAsJson.validate[NuevoDocente] match {
      case success: JsSuccess[NuevoDocente] => {
        Docente.nuevo(success.get.nombre, success.get.apellidos, success.get.sexo, success.get.fecha_nacimiento, success.get.correo, success.get.area, success.get.id_facebook, success.get.id_pam, success.get.id_crea, success.get.id_moodle)
        result.put("estado", "ok")
        Ok(objectMapper.writeValueAsString(result)).as("application/json")
      }
      case JsError(error) => {
        result.put("code", 20)
        result.put("msg", "Petición inválida.")
        BadRequest(objectMapper.writeValueAsString(result)).as("application/json")
      }
    }
  }

  @throws[Exception]
  def actualizar() = Action { request =>
    val bodyAsJson = request.body.asJson.get
    val objectMapper = new ObjectMapper
    val result = new util.HashMap[String, Any]

    bodyAsJson.validate[ActualizarDocente] match {
      case success: JsSuccess[ActualizarDocente] => {
        Docente.actualizar(success.get.id_docente, success.get.nombre, success.get.apellidos, success.get.sexo, success.get.fecha_nacimiento, success.get.correo, success.get.area, success.get.id_facebook, success.get.id_pam, success.get.id_crea, success.get.id_moodle)
        result.put("estado", "ok")
        Ok(objectMapper.writeValueAsString(result)).as("application/json")
      }
      case JsError(error) => {
        result.put("code", 20)
        result.put("msg", "Petición inválida.")
        BadRequest(objectMapper.writeValueAsString(result)).as("application/json")
      }
    }
  }

  @throws[Exception]
  def eliminar(id_docente:Long) = Action { request =>
    val objectMapper = new ObjectMapper
    val result = new util.HashMap[String, Any]

    val docente = Docente.find().byId(id_docente)
    if(docente != null){
      Docente.eliminar(id_docente)
      result.put("estado", "ok")
      Ok(objectMapper.writeValueAsString(result)).as("application/json")
    }else{
      result.put("code", 1)
      result.put("msg", "Docente no encontrado")
      NotFound(objectMapper.writeValueAsString(result)).as("application/json")
    }
  }


  /*
      if(escuela != null){
        Ok(objectMapper.writeValueAsString(escuela))
      }else{
        result.put("code", 1)
        result.put("msg", "Identificador de escuela no válido")
        NotFound(objectMapper.writeValueAsString(result))
      }
  */

  import play.api.libs.functional.syntax._
  import play.api.libs.json.Reads._
  import play.api.libs.json._

  case class NuevoDocente(nombre: String,
                          apellidos: String,
                          sexo: String,
                          fecha_nacimiento: String,
                          correo: String,
                          area: String,
                          id_facebook: String,
                          id_pam: String,
                          id_crea: String,
                          id_moodle: String)
  object NuevoDocente {
    implicit val reads: Reads[NuevoDocente] = (
        (JsPath \ "nombre").read[String](minLength[String](1) andKeep maxLength[String](150)) and
        (JsPath \ "apellidos").read[String](minLength[String](1) andKeep maxLength[String](150)) and
        (JsPath \ "sexo").read[String](minLength[String](1) andKeep maxLength[String](1)) and
        (JsPath \ "fecha_nacimiento").read[String](pattern("""[0-9]{4,4}-[0-9]{2,2}-[0-9]{2,2}"""r)) and
        (JsPath \ "correo").read[String](minLength[String](1) andKeep maxLength[String](100)) and
        (JsPath \ "area").read[String](minLength[String](1) andKeep maxLength[String](30)) and
        (JsPath \ "id_facebook").read[String](minLength[String](1) andKeep maxLength[String](20)) and
        (JsPath \ "id_pam").read[String](minLength[String](1) andKeep maxLength[String](20)) and
        (JsPath \ "id_crea").read[String](minLength[String](1) andKeep maxLength[String](20)) and
        (JsPath \ "id_moodle").read[String](minLength[String](1) andKeep maxLength[String](20)))(NuevoDocente.apply _)
  }

  case class ActualizarDocente(id_docente: Long,
                               nombre: String,
                               apellidos: String,
                               sexo: String,
                               fecha_nacimiento: String,
                               correo: String,
                               area: String,
                               id_facebook: String,
                               id_pam: String,
                               id_crea: String,
                               id_moodle: String)
  object ActualizarDocente{
    implicit val reads: Reads[ActualizarDocente] = (
      (JsPath \ "id_docente").read[Long] and
      (JsPath \ "nombre").read[String](minLength[String](1) andKeep maxLength[String](150)) and
        (JsPath \ "apellidos").read[String](minLength[String](1) andKeep maxLength[String](150)) and
        (JsPath \ "sexo").read[String](minLength[String](1) andKeep maxLength[String](1)) and
        (JsPath \ "fecha_nacimiento").read[String](pattern("""[0-9]{4,4}-[0-9]{2,2}-[0-9]{2,2}"""r)) and
        (JsPath \ "correo").read[String](minLength[String](1) andKeep maxLength[String](100)) and
        (JsPath \ "area").read[String](minLength[String](1) andKeep maxLength[String](30)) and
        (JsPath \ "id_facebook").read[String](minLength[String](1) andKeep maxLength[String](20)) and
        (JsPath \ "id_pam").read[String](minLength[String](1) andKeep maxLength[String](20)) and
        (JsPath \ "id_crea").read[String](minLength[String](1) andKeep maxLength[String](20)) and
        (JsPath \ "id_moodle").read[String](minLength[String](1) andKeep maxLength[String](20)))(ActualizarDocente.apply _)
  }

  import com.avaje.ebean.SqlRow
  import scala.collection.Map

  case class EgoGraph(nodes: List[SqlRow], interactions: List[SqlRow], properties: Map[String, AnyRef])

  import play.api.libs.json._
  import play.api.libs.json.Json._

  object EgoGraph{
    import play.api.libs.json._
    import scala.collection.JavaConverters._

    implicit val EgoGraph = new Writes[EgoGraph] {
      def writes(eg: EgoGraph): JsValue = {
        Json.toJson(
          eg.properties.foldLeft(Json.obj())((obj, row) =>
            obj ++ Json.obj(row._1 -> (row._2 match {
              case n:Number => BigDecimal(n.toString)
              case _ => row._2.toString
            })
            )
          ).++(
            obj(
              "egoGrafo" -> obj(
                "nodos" -> eg.nodes.map(
                  row =>
                    obj(
                      "id" -> BigDecimal(row.getLong("id")),
                      "tipo" -> row.getString("tipo"),
                      "nombre" -> row.getString("nombre")
                    )
                ),
                "interacciones" -> eg.interactions.map(
                  row =>
                    obj(
                      "id_interaccion" -> BigDecimal(row.getLong("id_interaccion")),
                      "origen" -> row.getString("origen"),
                      "origen_tipo" -> row.getString("origen_tipo"),
                      "destino" -> row.getString("destino"),
                      "destino_tipo" -> row.getString("destino_tipo"),
                      "tipo" -> row.getString("tipo")
                    )
                )
              )
            )
          )
        )

      }
    }
  }
}




