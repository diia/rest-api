package controllers.administracion

import java.util

import com.fasterxml.jackson.databind.ObjectMapper
import javax.inject.{Inject, Singleton}
import models._
import play.api.libs.json.{JsError, JsSuccess}
import play.api.mvc.{Action, Controller}

@Singleton
class ProgramaController @Inject()() extends Controller {

  @throws[Exception]
  def recuperar(id:Long) = Action {
    val objectMapper = new ObjectMapper
    val obj = Programa.find().byId(id)
    val result = new util.HashMap[String, Any]

    if(obj != null){
      Ok(objectMapper.writeValueAsString(obj)).as("application/json")
    }else{
      result.put("code", 7)
      result.put("msg", "Programa no encontrado")
      NotFound(objectMapper.writeValueAsString(result)).as("application/json")
    }
  }

  @throws[Exception]
  def crear() = Action { request =>
    val bodyAsJson = request.body.asJson.get
    val objectMapper = new ObjectMapper
    val result = new util.HashMap[String, Any]

    bodyAsJson.validate[NuevoPrograma] match {
      case success: JsSuccess[NuevoPrograma] => {
        Programa.nuevoPrograma(success.get.id_escuela, success.get.nombre)
        result.put("estado", "ok")
        Ok(objectMapper.writeValueAsString(result)).as("application/json")
      }
      case JsError(error) => {
        result.put("code", 20)
        result.put("msg", "Petición inválida.")
        BadRequest(objectMapper.writeValueAsString(result)).as("application/json")
      }
    }
  }

  @throws[Exception]
  def actualizar() = Action { request =>
    val bodyAsJson = request.body.asJson.get
    val objectMapper = new ObjectMapper
    val result = new util.HashMap[String, Any]

    bodyAsJson.validate[ActualizarPrograma] match {
      case success: JsSuccess[ActualizarPrograma] => {
        Programa.actualizarPrograma(success.get.id_programa, success.get.id_escuela, success.get.nombre)
        result.put("estado", "ok")
        Ok(objectMapper.writeValueAsString(result)).as("application/json")
      }
      case JsError(error) => {
        result.put("code", 20)
        result.put("msg", "Petición inválida.")
        BadRequest(objectMapper.writeValueAsString(result)).as("application/json")
      }
    }
  }

  @throws[Exception]
  def eliminar(id:Long) = Action { request =>
    val objectMapper = new ObjectMapper
    val result = new util.HashMap[String, Any]
    val obj = Programa.find().byId(id.asInstanceOf[Long])
    if(obj != null){
      Programa.eliminarPrograma(id)
      result.put("estado", "ok")
      Ok(objectMapper.writeValueAsString(result)).as("application/json")
    }else{
      result.put("code", 7)
      result.put("msg", "Programa no encontrado")
      NotFound(objectMapper.writeValueAsString(result)).as("application/json")
    }
  }


  import play.api.libs.functional.syntax._
  import play.api.libs.json.Reads._
  import play.api.libs.json._

  case class NuevoPrograma(id_escuela: Long,
                             nombre: String)
  object NuevoPrograma {
    implicit val reads: Reads[NuevoPrograma] = (
        (JsPath \ "id_escuela").read[Long] and
        (JsPath \ "nombre").read[String](minLength[String](1) andKeep maxLength[String](150)))(NuevoPrograma.apply _)
  }

  case class ActualizarPrograma( id_programa: Long,
                                 id_escuela: Long,
                                   nombre: String)
  object ActualizarPrograma {
    implicit val reads: Reads[ActualizarPrograma] = (
      (JsPath \ "id_programa").read[Long] and
        (JsPath \ "id_escuela").read[Long] and
        (JsPath \ "nombre").read[String](minLength[String](1) andKeep maxLength[String](150)))(ActualizarPrograma.apply _)
  }
}
