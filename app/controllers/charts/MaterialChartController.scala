package controllers.charts

import javax.inject.Inject
import play.api.mvc.{Action, Controller}
import javax.inject.{Inject, Singleton}
import models.{Curso, Docente}
import play.api.i18n.{I18nSupport, Messages, MessagesApi}
import play.api.libs.json.Json

@Singleton
class MaterialChartController @Inject()(val messagesApi: MessagesApi) extends Controller with I18nSupport{

  case class GetParameters(id_docente: Long, id_curso: Long, fecha_inicio: Option[String], fecha_fin: Option[String], nodos: Option[Set[String]], interacciones: Option[Set[String]], tipo_interacciones: Option[Set[String]], plataformas: Option[Set[String]], seleccion_estudiantes: Option[Set[Long]], seleccion_actividades: Option[Set[Long]], seleccion_recursos: Option[Set[Long]])

  import play.api.data.Form
  import play.api.data.Forms._
  import play.api.data.validation.Constraints._
  import play.api.data._
  import play.api.data.Forms._
  val getParameters: Form[GetParameters] = Form {
    mapping(
      "id_docente" -> longNumber.verifying("erorr format", n => n != null && n>0),
      "id_curso" -> default[Long](longNumber, -1).verifying("Número positivo", _>0).verifying("Número positivos", _>0),
      "fecha_inicio" -> optional(text),
      "fecha_fin" -> optional(text),
      "nodos" -> optional(set[String](text)),
      "interacciones" -> optional(set[String](text)),
      "tipo_interacciones" -> optional(set[String](text)),
      "plataformas" -> optional(set[String](text)),
      "seleccion_estudiantes" -> optional(set[Long](longNumber)),
      "seleccion_actividades" -> optional(set[Long](longNumber)),
      "seleccion_recursos" -> optional(set[Long](longNumber))
    )(GetParameters.apply)(GetParameters.unapply)
  }

  object Serializators{
    import play.api.libs.json._
    import play.api.libs.json.Json._

    implicit val graficoList = new Writes[Seq[Grafico]] {
      def writes(graficoL: Seq[Grafico]): JsValue =
        Json.toJson(
          graficoL.map(
            grafico => {
              grafico match {
                case numero: Numero => {
                  obj(
                    "tipo" -> "numero",
                    "titulo"->numero.titulo,
                    "datos" -> BigDecimal(numero.dato.dato.toString),
                    "color" -> numero.dato.color
                  )
                }
                case nodo: Nodo => {
                  obj(
                    "tipo" -> "nodo",
                    "titulo"->nodo.titulo,
                    "datos" -> BigDecimal(nodo.dato.dato.toString),
                    "color" -> nodo.dato.color,
                    "nombre" -> nodo.nombre,
                    "tipoNodo" -> nodo.tipoNodo

                  )
                }
                case torta: Torta => {
                  obj(
                    "tipo" -> "torta",
                    "titulo"->torta.titulo,
                    "datos" -> torta.datos.map(
                      dato =>
                        obj(
                          "nombre"->dato._1,
                          "valor"->BigDecimal(dato._2.dato.toString),
                          "color"->dato._2.color
                        )
                    )
                  )
                }
                case barra: Barra => {
                  obj(
                    "tipo" -> "barra",
                    "titulo"->barra.titulo,
                    "datos" -> barra.datos.map(
                      dato =>
                        obj(
                          "nombre"->dato._1,
                          "valor"->BigDecimal(dato._2.dato.toString),
                          "color"->dato._2.color
                        )
                    ),
                    "nombre_eje_x"->barra.ejeX,
                    "nombre_eje_y"->barra.ejeY
                  )
                }
              }
            }
          )
        )
    }

  }

  case class NotFoundError(code: Int, msg: String)

  object NotFoundError{
    import play.api.libs.json._
    implicit val errorMsg = Json.writes[NotFoundError]
  }


  @throws[Exception]
  def chart() = Action { implicit request =>
    getParameters.bindFromRequest.fold(
      formWithErrors => {
        BadRequest("Mal")
      },
      parameters => {

        Option(Docente.find.byId(parameters.id_docente)) map {
          docente =>
            import scala.collection.JavaConverters._
            import scala.collection.JavaConversions._

            (Option(parameters.id_curso) match {
              case Some(id_curso) => id_curso == -1 || docente.cursos().exists(s => s.getLong("id_curso") == id_curso)
              case None => true
            }) match {
              case true => {
                Option(Curso.find.byId(parameters.id_curso)) map {
                  curso =>

                    val messages: Messages = messagesApi.preferred(request) // get the messages for the given request

                    val matPromPCantAcce = new Numero("Promedio de consultas de recursos educativos", Dato(curso.matPromPCantAcce.get(0).getBigDecimal("prom"), messages("color.indigo")))

                    val porcentajePPlataf = new Torta("Recursos educativos disponibles por plataforma", curso.porcentajePPlataf.toList.map(
                      data => (data.getString("plataforma") match {
                        case "c" => "Crea 2"
                        case "p" => "Pam"
                        case "m" => messages("plataforma.uruguayeduca")
                        case "f" => "Facebook"
                      }) -> Dato(data.getInteger("total").asInstanceOf[Number],
                        (data.getString("plataforma") match {
                          case "c" => messages("color.red")
                          case "p" => messages("color.green")
                          case "m" => messages("color.orange")
                          case "f" => messages("color.indigo")
                        })
                      ) ) toMap)

                    val porcenPTipoMat = new Torta("Distribución de recursos educativos por tipo", curso.porcenPTipoMat.toList.map(
                      data => (data.getString("tipo_contenido") match {
                        case "tex" => "Texto"
                        case "img" => "Imagen"
                        case "lik" => "Link"
                        case "vid" => "Video"
                        case "gif" => "Gif"
                        case "sck" => "Sticker"
                        case "des" => "Otro"
                      }) -> Dato(data.getInteger("count").asInstanceOf[Number],
                        (data.getString("tipo_contenido") match {
                          case "tex" => messages("color.red")
                          case "img" => messages("color.green")
                          case "lik" => messages("color.orange")
                          case "vid" => messages("color.indigo")
                          case "gif" => messages("color.amber")
                          case "sck" => messages("color.purple")
                          case _ => messages("color.pink")
                        })
                      )) toMap)

                    Ok(Json.toJson(List(matPromPCantAcce, porcentajePPlataf, porcenPTipoMat))(Serializators.graficoList))
                }getOrElse{
                  NotFound(Json.toJson(NotFoundError(1,"El curso no esta disponible")))
                }
              }
              case _ => NotFound(Json.toJson(NotFoundError(1,"El curso no pertenece al docente")))
            }

        }getOrElse{
          NotFound(Json.toJson(NotFoundError(1,"El docente no existe")))
        }
      }
    )

  }

  class Grafico(titulo: String, datos: Any)

  case class Dato(dato: Number, color: String)

  case class Numero(titulo: String, dato: Dato) extends Grafico(titulo, dato)

  case class Nodo(titulo: String, dato: Dato, nombre: String, tipoNodo: String) extends Grafico(titulo, dato)

  import scala.collection.Map
  case class Torta(titulo: String, datos: Map[String, Dato]) extends Grafico(titulo, datos)

  case class Barra(titulo: String, datos: Map[String, Dato], ejeX: String, ejeY: String) extends Grafico(titulo, datos)

}
