package controllers.charts

import com.avaje.ebean.SqlRow
import controllers.calculo.CalculolController
import javax.inject.Inject
import play.api.libs.json.Json
import play.api.mvc.{Action, Controller}
import javax.inject.{Inject, Singleton}
import models.{Curso, Docente, Nodo}

import scala.collection.Map


@Singleton
class EgoGraphController @Inject()() extends Controller {
  case class GetParameters(id_nodo: Long, fecha_inicio: Option[String], fecha_fin: Option[String], id_curso: Option[Long], nodos: Option[Set[String]], interacciones: Option[Set[String]], tipo_interacciones: Option[Set[String]], plataformas: Option[Set[String]])

  import play.api.data.Form
  import play.api.data.Forms._
  import play.api.data.validation.Constraints._
  import play.api.data._
  import play.api.data.Forms._
  val getParameters: Form[GetParameters] = Form {
    mapping(
      "id_nodo" -> longNumber.verifying("erorr format", n => n != null && n>0),
      "fecha_inicio" -> optional(text),
      "fecha_fin" -> optional(text),
      "id_curso" -> optional(longNumber),
      "nodos" -> optional(set[String](text)),
      "interacciones" -> optional(set[String](text)),
      "tipo_interacciones" -> optional(set[String](text)),
      "plataformas" -> optional(set[String](text))
    )(GetParameters.apply)(GetParameters.unapply)
  }

  case class NotFoundError(code: Int, msg: String)

  object NotFoundError{
    import play.api.libs.json._
    implicit val errorMsg = Json.writes[NotFoundError]
  }


  case class egoGraph(nodes: List[SqlRow], interactions: List[SqlRow], properties: Map[String, Number])

  import play.api.libs.json._
  import play.api.libs.json.Json._

  object egoGraph{
    import play.api.libs.json._
    import scala.collection.JavaConverters._

    implicit val egoGraph = new Writes[egoGraph] {
      def writes(eg: egoGraph): JsValue =
        Json.toJson(
          obj(
            "propiedades" -> eg.properties.map{prop =>
              obj(
                prop._1 -> BigDecimal(prop._2.toString)
              )
            },
          "nodos" -> eg.nodes.toList.map(
            row =>
              obj(
                "id" -> BigDecimal(row.getLong("id")),
                "tipo" -> row.getString("tipo"),
                "nombre" -> row.getString("nombre")
              )
          ),
            "interacciones" -> eg.interactions.toList.map(
              row =>
                obj(
                  "id_interaccion" -> BigDecimal(row.getLong("id_interaccion")),
                  "origen" -> row.getString("origen"),
                  "origen_tipo" -> row.getString("origen_tipo"),
                  "destino" -> row.getString("destino"),
                  "destino_tipo" -> row.getString("destino_tipo"),
                  "tipo" -> row.getString("tipo")
                )
            )

          )
        )
    }
  }


  @throws[Exception]
  def chart(id_nodo: Long) = Action { implicit request =>
    //class GetParameters(id_nodo: Long,fecha_inicio: Option[String],fecha_fin: Option[String],
    // id_curso: Option[Long],nodos: Option[Set[String]],
    // interacciones: Option[Set[String]],tipo_interacciones: Option[Set[String]],
    // plataformas: Option[Set[String]])
    getParameters.fillAndValidate(new GetParameters(id_nodo, Option(""), Option(""), Option(0), Option(Set()), Option(Set()), Option(Set()), Option(Set()))).fold(
      formWithErrors => {
        BadRequest("Mal")
      },
      parameters => {

        Option(Nodo.find.byId(parameters.id_nodo)) map {
          nodo =>
            import scala.collection.JavaConverters._
            import scala.collection.JavaConversions._
            /*
            get interactions and nodes

            structure:
              interactions:[
                id_interaccion
                origen
                origen_tipo
                destino
                destino_tipo
                tipo
              ], node: [
                id
                tipo
                nombre
              ]

              *Estudiante & Docente -> "cursos": "2",
              *Actividad & Material -> id_curso

             */
            //
            val nodes = nodo.nodosEgoGraph()
            val interactions = nodo.intEgoGraph()
            val a = new egoGraph(nodes.asScala.toList, interactions.asScala.toList, Map(""->1.asInstanceOf[Number]))
            Ok(Json.toJson(a))
        }getOrElse{
          NotFound(Json.toJson(NotFoundError(1,"El docente no existe")))
        }
      }
    )

  }


}
