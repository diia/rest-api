package controllers.charts

import javax.inject.{Inject, Singleton}
import models.{Curso, Docente, Estudiante}
import play.api.i18n._
import play.api.libs.json.Json
import play.api.mvc.{Action, Controller}
//import play.i18n.MessagesApi

import javax.inject.Inject
import play.api.i18n.I18nSupport

@Singleton
class EstudianteChartController @Inject()(val messagesApi: MessagesApi) extends Controller with I18nSupport{
  import javax.inject._
  import play.api.mvc._

  case class GetParameters(id_docente: Long, id_curso: Long, id_estudiante:Option[Long], fecha_inicio: Option[String], fecha_fin: Option[String], nodos: Option[Set[String]], interacciones: Option[Set[String]], tipo_interacciones: Option[Set[String]], plataformas: Option[Set[String]], seleccion_estudiantes: Option[Set[Long]], seleccion_actividades: Option[Set[Long]], seleccion_recursos: Option[Set[Long]])

  import play.api.data.Form
  import play.api.data.Forms._
  val getParameters: Form[GetParameters] = Form {
    mapping(
      "id_docente" -> longNumber.verifying("erorr format", n => n != null && n>0),
      "id_curso" -> longNumber.verifying("Número positivos", _>0),
      "id_estudiante" -> optional(longNumber),
      "fecha_inicio" -> optional(text),
      "fecha_fin" -> optional(text),
      "nodos" -> optional(set[String](text)),
      "interacciones" -> optional(set[String](text)),
      "tipo_interacciones" -> optional(set[String](text)),
      "plataformas" -> optional(set[String](text)),
      "seleccion_estudiantes" -> optional(set[Long](longNumber)),
      "seleccion_actividades" -> optional(set[Long](longNumber)),
      "seleccion_recursos" -> optional(set[Long](longNumber))
    )(GetParameters.apply)(GetParameters.unapply)
  }

  object Serializators{
    import play.api.libs.json.Json._
    import play.api.libs.json._

    implicit val graficoList = new Writes[Seq[Grafico]] {
      def writes(graficoL: Seq[Grafico]): JsValue =
        Json.toJson(
          graficoL.map(
            grafico => {
              grafico match {
                case numero: Numero => {
                  obj(
                    "tipo" -> "numero",
                    "titulo"->numero.titulo,
                    "datos" -> BigDecimal(numero.dato.dato.toString),
                  "color" -> numero.dato.color
                  )
                }
                case nodo: Nodo => {
                  obj(
                    "tipo" -> "nodo",
                    "titulo"->nodo.titulo,
                    "datos" -> BigDecimal(nodo.dato.dato.toString),
                    "color" -> nodo.dato.color,
                    "nombre" -> nodo.nombre,
                    "tipoNodo" -> nodo.tipoNodo

                  )
                }
                case torta: Torta => {
                  obj(
                    "tipo" -> "torta",
                    "titulo"->torta.titulo,
                    "datos" -> torta.datos.map(
                      dato =>
                        obj(
                          "nombre"->dato._1,
                          "valor"->BigDecimal(dato._2.dato.toString),
                          "color"->dato._2.color
                        )
                    )
                  )
                }
                case barra: Barra => {
                  obj(
                    "tipo" -> "barra",
                    "titulo"->barra.titulo,
                    "datos" -> barra.datos.map(
                      dato =>
                        obj(
                          "nombre"->dato._1,
                          "valor"->BigDecimal(dato._2.dato.toString),
                          "color"->dato._2.color
                        )
                    ),
                    "nombre_eje_x"->barra.ejeX,
                    "nombre_eje_y"->barra.ejeY
                  )
                }
              }
            }
          )
        )
    }

  }

  case class NotFoundError(code: Int, msg: String)

  object NotFoundError{
    import play.api.libs.json._
    implicit val errorMsg = Json.writes[NotFoundError]
  }

  @throws[Exception]
  def chart() = Action { implicit request =>
    getParameters.bindFromRequest.fold(
      formWithErrors => {
        BadRequest("Mal")
      },
      parameters => {

        Option(Docente.find.byId(parameters.id_docente)) map {
          docente =>
            import scala.collection.JavaConversions._

            docente.cursos().exists(s => s.getLong("id_curso") == parameters.id_curso)
             match {
              case true => {
                Option(Curso.find.byId(parameters.id_curso)) map {
                  curso =>
                    if(parameters.id_estudiante.isDefined){
                      (parameters.id_estudiante map { id_estudiante => Option(Estudiante.find().byId(id_estudiante))}
                        ).get map {
                        estudiante =>
                          estudiante.actsEntreAntFL(curso.id())
                        Ok("gráficas")
                      }getOrElse{
                        NotFound(Json.toJson(NotFoundError(1,"El estudiante no existe.")))
                      }
                    }else{

                      val messages: Messages = messagesApi.preferred(request) // get the messages for the given request

                      val promNotaNum = BigDecimal(curso.promNota.get(0).getBigDecimal("prom"))
                      val promNota = new Numero("Promedio general del grupo", Dato(promNotaNum, promNotaNum match {
                        case n if n < 6  =>  messages("color.red")
                        case n if n >= 6 && n <= 8  =>  messages("color.amber")
                        case n if n > 8  =>  messages("color.green")
                      } ))


                      var tmpUnique = curso.estudiantePopular.get(0)
                      val estudiantePopular = new Nodo("Estudiante más popular ("+tmpUnique.getString("interactions")+" interacciones)", Dato(tmpUnique.getLong("nodo_destino"), messages("color.deeppurple")), tmpUnique.getString("nombre"), "Estudiante")

                      val promIntEstutes = new Numero("Promedio de interacciones entre estudiantes", Dato(curso.promIntEstutes.get(0).getBigDecimal("prom"), messages("color.lightblue")))
                      val promConexiones = new Numero("Promedio de conexiones", Dato(curso.promConexiones.get(0).getBigDecimal("prom"), messages("color.orange")))


                      val porcentajeIntPlat = new Barra("Número de interacciones por plataforma", curso.porcentajeIntPlatE.toList.map(
                        data => (data.getString("plataforma") match {
                          case "c" => "Crea 2"
                          case "p" => "Pam"
                          case "m" => messages("plataforma.uruguayeduca")
                          case "f" => "Facebook"
                        }) -> Dato(data.getInteger("count").asInstanceOf[Number],
                          (data.getString("plataforma") match {
                            case "c" => messages("color.red")
                            case "p" => messages("color.green")
                            case "m" => messages("color.orange")
                            case "f" => messages("color.indigo")
                          })
                        )) toMap, "Plataforma", "Número de interacciones")

                      val porcentajePTipoNodo = new Torta("Porcentaje de interacciones por tipo de nodo", curso.porcentajePTipoNodo.toList.map(
                        data => (data.getString("tipo") match {
                          case "e" => "Estudiante"
                          case "d" => "Docente"
                          case "m" => "Material"
                          case "a" => "Actividad"
                        }) -> Dato(data.getInteger("count").asInstanceOf[Number],
                          (data.getString("tipo") match {
                            case "e" => messages("color.orange")
                            case "d" => messages("color.red")
                            case "m" => messages("color.green")
                            case "a" => messages("color.yellow")
                          })
                        )) toMap)

                      val cantPTipoInt = new Barra("Cantidad de interacciones por tipo", curso.cantPTipoInt.toList.map(
                        data => (data.getString("tipo_interaccion") match {
                          case "rea" => "Reacción"
                          case "msj" => "Mensajes"
                          case "vis" => "Visualización"
                          case "men" => "Mención"
                          case "pub" => "Publicación"
                          case "com" => "Comentario"
                        }) -> Dato(data.getInteger("count").asInstanceOf[Number],
                          (data.getString("tipo_interaccion") match {
                            case "rea" => messages("color.red")
                            case "msj" => messages("color.green")
                            case "vis" => messages("color.orange")
                            case "men" => messages("color.indigo")
                            case "pub" => messages("color.amber")
                            case "com" => messages("color.purple")
                          })
                        )) toMap, "Tipo interacción", "Número de interacciones")

                      val cantPPolaridad = new Torta("Porcentaje de interacciones por polaridad", curso.cantPPolaridad.toList.map(
                        data => (data.getString("sentimiento") match {
                          case "n" => "Negativo"
                          case "e" => "Neutro"
                          case "p" => "Positivo"
                        }) -> Dato(data.getInteger("count").asInstanceOf[Number],
                          (data.getString("sentimiento") match {
                            case "n" => messages("color.red")
                            case "e" => messages("color.amber")
                            case "p" => messages("color.green")
                          })
                        )) toMap)

                      Ok(Json.toJson(List(promNota, estudiantePopular, promIntEstutes, promConexiones, porcentajeIntPlat, porcentajePTipoNodo, cantPTipoInt, cantPPolaridad))(Serializators.graficoList))
                    }

                }getOrElse{
                  NotFound(Json.toJson(NotFoundError(1,"El curso no esta disponible")))
                }
              }
              case _ => NotFound(Json.toJson(NotFoundError(1,"El curso no pertenece al docente")))
            }

        }getOrElse{
          NotFound(Json.toJson(NotFoundError(1,"El docente no existe")))
        }
      }
    )

  }



  class Grafico(titulo: String, datos: Any)

  case class Dato(dato: Number, color: String)

  case class Numero(titulo: String, dato: Dato) extends Grafico(titulo, dato)

  case class Nodo(titulo: String, dato: Dato, nombre: String, tipoNodo: String) extends Grafico(titulo, dato)

  import scala.collection.Map
  case class Torta(titulo: String, datos: Map[String, Dato]) extends Grafico(titulo, datos)

  case class Barra(titulo: String, datos: Map[String, Dato], ejeX: String, ejeY: String) extends Grafico(titulo, datos)


}
