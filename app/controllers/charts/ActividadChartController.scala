package controllers.charts

import play.api.mvc.{Action, Controller}
import javax.inject.{Inject, Singleton}
import models.{Curso, Docente}
import play.api.i18n.{I18nSupport, Messages, MessagesApi}
import play.api.libs.json.Json

@Singleton
class ActividadChartController @Inject()(val messagesApi: MessagesApi) extends Controller with I18nSupport{


  case class GetParameters(id_docente: Long, id_curso: Long, fecha_inicio: Option[String], fecha_fin: Option[String], nodos: Option[Set[String]], interacciones: Option[Set[String]], tipo_interacciones: Option[Set[String]], plataformas: Option[Set[String]], seleccion_estudiantes: Option[Set[Long]], seleccion_actividades: Option[Set[Long]], seleccion_recursos: Option[Set[Long]])

  import play.api.data.Form
  import play.api.data.Forms._
  import play.api.data.validation.Constraints._
  import play.api.data._
  import play.api.data.Forms._
  val getParameters: Form[GetParameters] = Form {
    mapping(
      "id_docente" -> longNumber.verifying("erorr format", n => n != null && n>0),
      "id_curso" -> default[Long](longNumber, -1).verifying("Número positivo", _>0).verifying("Número positivos", _>0),
      "fecha_inicio" -> optional(text),
      "fecha_fin" -> optional(text),
      "nodos" -> optional(set[String](text)),
      "interacciones" -> optional(set[String](text)),
      "tipo_interacciones" -> optional(set[String](text)),
      "plataformas" -> optional(set[String](text)),
      "seleccion_estudiantes" -> optional(set[Long](longNumber)),
      "seleccion_actividades" -> optional(set[Long](longNumber)),
      "seleccion_recursos" -> optional(set[Long](longNumber))
    )(GetParameters.apply)(GetParameters.unapply)
  }

  object Serializators{
    import play.api.libs.json._
    import play.api.libs.json.Json._

    implicit val graficoList = new Writes[Seq[Grafico]] {
      def writes(graficoL: Seq[Grafico]): JsValue =
        Json.toJson(
          graficoL.map(
            grafico => {
              grafico match {
                case numero: Numero => {
                  obj(
                    "tipo" -> "numero",
                    "titulo"->numero.titulo,
                    "datos" -> BigDecimal(numero.dato.dato.toString),
                    "color" -> numero.dato.color
                  )
                }
                case nodo: Nodo => {
                  obj(
                    "tipo" -> "nodo",
                    "titulo"->nodo.titulo,
                    "datos" -> BigDecimal(nodo.dato.dato.toString),
                    "color" -> nodo.dato.color,
                    "nombre" -> nodo.nombre,
                    "tipoNodo" -> nodo.tipoNodo,
                    ( if (nodo.fecha_publicacion.isDefined) "fecha_publicacion" else "" )->( if (nodo.fecha_publicacion.isDefined) nodo.fecha_publicacion.get else "" ),
                    ( if (nodo.fecha_limite.isDefined) "fecha_limite" else "" )-> ( if (nodo.fecha_limite.isDefined) nodo.fecha_limite.get else "" )

                  )
                }
                case torta: Torta => {
                  obj(
                    "tipo" -> "torta",
                    "titulo"->torta.titulo,
                    "datos" -> torta.datos.map(
                      dato =>
                        obj(
                          "nombre"->dato._1,
                          "valor"->BigDecimal(dato._2.dato.toString),
                          "color"->dato._2.color
                        )
                    )
                  )
                }
                case barra: Barra => {
                  obj(
                    "tipo" -> "barra",
                    "titulo"->barra.titulo,
                    "datos" -> barra.datos.map(
                      dato =>
                        obj(
                          "nombre"->dato._1,
                          "valor"->BigDecimal(dato._2.dato.toString),
                          "color"->dato._2.color
                        )
                    ),
                    "nombre_eje_x"->barra.ejeX,
                    "nombre_eje_y"->barra.ejeY
                  )
                }
              }
            }
          )
        )
    }

  }

  case class NotFoundError(code: Int, msg: String)

  object NotFoundError{
    import play.api.libs.json._
    implicit val errorMsg = Json.writes[NotFoundError]
  }

  @throws[Exception]
  def chart() = Action { implicit request =>
    getParameters.bindFromRequest.fold(
      formWithErrors => {
        BadRequest("Mal")
      },
      parameters => {

        Option(Docente.find.byId(parameters.id_docente)) map {
          docente =>
            import scala.collection.JavaConverters._
            import scala.collection.JavaConversions._

            (Option(parameters.id_curso) match {
              case Some(id_curso) => id_curso == -1 || docente.cursos().exists(s => s.getLong("id_curso") == id_curso)
              case None => true
            }) match {
              case true => {
                Option(Curso.find.byId(parameters.id_curso)) map {
                  curso =>
                    val messages: Messages = messagesApi.preferred(request) // get the messages for the given request

                    val calMaxObt = BigDecimal(curso.actCalMax.get(0).getBigDecimal("max"))
                    val actCalMax = new Numero("Calificación MÁXIMA obtenida en las actividades", Dato(calMaxObt, calMaxObt match {
                      case n if n < 6  =>  messages("color.red")
                      case n if n >= 6 && n <= 8  =>  messages("color.amber")
                      case n if n > 8  =>  messages("color.green")
                    })
                    )

                    val calMinObt = BigDecimal(curso.actCalMin.get(0).getBigDecimal("min"))
                    val actCalMin = new Numero("Calificación MÍNIMA obtenida en las actividades", Dato(calMinObt, calMinObt match {
                      case n if n < 6  =>  messages("color.red")
                      case n if n >= 6 && n <= 8  =>  messages("color.amber")
                      case n if n > 8  =>  messages("color.green")
                    })
                    )

                    val promNota = BigDecimal(curso.actPromPNota.get(0).getBigDecimal("prom"))
                    val actPromPNota = new Numero("Promedio de nota", Dato(promNota, promNota match {
                      case n if n < 6  =>  messages("color.red")
                      case n if n >= 6 && n <= 8  =>  messages("color.amber")
                      case n if n > 8  =>  messages("color.green")
                    }) )

                    var graficos : Seq[Grafico] = Seq(actCalMax, actCalMin, actPromPNota)


                    var tmpUnique = curso.proxActAEnt

                    if(tmpUnique.size()>0){
                      val tmp= tmpUnique.get(0)
                      val materialMasInt = new Nodo("Próxima actividad a entregar ("+tmp.getString("fecha_limite_leg")+")", Dato(tmp.getLong("id_actividad"), messages("color.indigo")), tmp.getString("nombre"), "Actividad", Option(tmp.getString("fecha_publicacion")), Option(tmp.getString("fecha_limite")))
                      graficos =graficos :+ materialMasInt

                    }

                    //val actPromPCantAcce = new Numero("Promedio de accesos por estudiante", Dato(curso.actPromPCantAcce.get(0).getBigDecimal("prom"), messages("color.green")) )


                    val nivelRealAct = new Torta("Nivel de realización de actividades", curso.nivelRealAct.toList.map(
                      data => (data.getString("estado") match {
                        case "ec" => "En curso"
                        case "pe" => "Pendiente de corrección"
                        case "co" => "Corregida"
                        case "fi" => "Finalizada"
                      }) -> Dato(data.getInteger("count").asInstanceOf[Number],
                        (data.getString("estado") match {
                          case "ec" => messages("color.amber")
                          case "pe" => messages("color.indigo")
                          case "co" => messages("color.orange")
                          case "fi" => messages("color.green")
                        })
                      )) toMap)

                    graficos =graficos :+ nivelRealAct

                    Ok(Json.toJson(graficos)(Serializators.graficoList))
                }getOrElse{
                  NotFound(Json.toJson(NotFoundError(1,"El curso no esta disponible")))
                }
              }
              case _ => NotFound(Json.toJson(NotFoundError(1,"El curso no pertenece al docente")))
            }
        }getOrElse{
          NotFound(Json.toJson(NotFoundError(1,"El docente no existe")))
        }
      }
    )

  }

  class Grafico(titulo: String, datos: Any)

  case class Dato(dato: Number, color: String)

  case class Numero(titulo: String, dato: Dato) extends Grafico(titulo, dato)

  case class Nodo(titulo: String, dato: Dato, nombre: String, tipoNodo: String, fecha_publicacion: Option[String], fecha_limite: Option[String]) extends Grafico(titulo, dato)

  import scala.collection.Map
  case class Torta(titulo: String, datos: Map[String, Dato]) extends Grafico(titulo, datos)

  case class Barra(titulo: String, datos: Map[String, Dato], ejeX: String, ejeY: String) extends Grafico(titulo, datos)

}
