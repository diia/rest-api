package controllers.consulta

import java.util

import com.fasterxml.jackson.databind.ObjectMapper
import javax.inject.{Inject, Singleton}
import models.{Curso, Escuela, EscuelaDocente}
import play.api.mvc.{Action, Controller}

@Singleton
class EscuelaController @Inject()() extends Controller {
  @throws[Exception]
  def escuelas() = Action {
    val objectMapper = new ObjectMapper
    val result = new util.HashMap[String, Any]

    val escuelas = Escuela.escuelas()
    result.put("escuelas", escuelas)
    Ok(objectMapper.writeValueAsString(result)).as("application/json")
  }

  @throws[Exception]
  def programas(id_escuela:Long) = Action {
    val objectMapper = new ObjectMapper
    val result = new util.HashMap[String, Any]

    val escuela = Escuela.find().byId(id_escuela)

    if(escuela != null){
      val programas = escuela.programas()
      result.put("programas", programas)
      Ok(objectMapper.writeValueAsString(result)).as("application/json")
    }else{
      result.put("code", 6)
      result.put("msg", "Escuela no encontrada")
      NotFound(objectMapper.writeValueAsString(result)).as("application/json")
    }
  }

  @throws[Exception]
  def docentes(id_escuela:Long) = Action {
    val objectMapper = new ObjectMapper
    val result = new util.HashMap[String, Any]

    val escuela = Escuela.find.byId(id_escuela)

    if(escuela != null){
      val docentes = EscuelaDocente.docentesByEscuela(id_escuela)
      result.put("docentes", docentes)
      Ok(objectMapper.writeValueAsString(result)).as("application/json")
    }else{
      result.put("code", 6)
      result.put("msg", "Escuela no encontrada")
      NotFound(objectMapper.writeValueAsString(result)).as("application/json")
    }
  }
}
