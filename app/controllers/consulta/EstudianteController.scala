package controllers.consulta

import java.util

import com.fasterxml.jackson.databind.ObjectMapper
import controllers.calculo.{ CalculolController}
import javax.inject.{Inject, Singleton}
import models.{Actividad, Curso, Escuela, Estudiante}
import play.api.mvc.{Action, Controller}

@Singleton
class EstudianteController @Inject()() extends Controller {

  @throws[Exception]
  def materiales(id_estudiante:Long) = Action {
    val objectMapper = new ObjectMapper
    val result = new util.HashMap[String, Any]

    val estudiante = Estudiante.find().byId(id_estudiante)
    if(estudiante != null){
      val materiales = estudiante.materiales()
      result.put("materiales", materiales)
      Ok(objectMapper.writeValueAsString(result)).as("application/json")
    }else{
      result.put("code", 3)
      result.put("msj", "Estudiante no encontrado.")
      NotFound(objectMapper.writeValueAsString(result)).as("application/json")
    }
  }

  @throws[Exception]
  def actividades(id_estudiante:Long, estado:String) = Action {
    val objectMapper = new ObjectMapper
    val result = new util.HashMap[String, Any]

    val estudiante = Estudiante.find().byId(id_estudiante)
    if(estudiante == null){
      result.put("code", 3)
      result.put("msj", "Estudiante no encontrado.")
      NotFound(objectMapper.writeValueAsString(result)).as("application/json")
    }else if( !(CalculolController.actividadStatuses contains estado)  ){
      result.put("code", 18)
      result.put("msj", "Estado de actividad invalido.")
      BadRequest(objectMapper.writeValueAsString(result)).as("application/json")
    }else{
      val actividades = estudiante.actividades(estado, null)
      result.put("actividades", actividades)
      Ok(objectMapper.writeValueAsString(result)).as("application/json")
    }
  }

  @throws[Exception]
  def actividadesByCurso(id_estudiante:Long, id_curso:Long, estado_actividad:String) = Action {
    val objectMapper = new ObjectMapper
    val result = new util.HashMap[String, Any]

    val estudiante = Estudiante.find().byId(id_estudiante)
    val curso = Curso.find().byId(id_curso)

    if( !(CalculolController.actividadStatuses contains estado_actividad)  ){
      result.put("code", 18)
      result.put("msj", "Estado de actividad invalido.")
      BadRequest(objectMapper.writeValueAsString(result)).as("application/json")
    }else if(estudiante == null){
      result.put("code", 3)
      result.put("msj", "Estudiante no encontrado.")
      NotFound(objectMapper.writeValueAsString(result)).as("application/json")
    }else if(curso == null){
      result.put("code", 4)
      result.put("msj", "Curso no encontrado.")
      NotFound(objectMapper.writeValueAsString(result)).as("application/json")
    }else{
      val actividades = Estudiante.actividadesByCurso(id_estudiante, id_curso, estado_actividad)
      result.put("actividades", actividades)
      Ok(objectMapper.writeValueAsString(result)).as("application/json")
    }
  }

  @throws[Exception]
  def informacion(id_estudiante:Long) = Action {
    val objectMapper = new ObjectMapper
    val result = new util.HashMap[String, Any]
    val estudiante = Estudiante.find.byId(id_estudiante)

    if(estudiante != null){
      val informacion = estudiante.informacion
      result.put("informacion", informacion)
      Ok(objectMapper.writeValueAsString(result)).as("application/json")
    }else{
      result.put("code", 3)
      result.put("msg", "Estudiante no encontrado")
      NotFound(objectMapper.writeValueAsString(result)).as("application/json")
    }
  }
}
