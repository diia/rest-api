package controllers.consulta

import java.util

import com.avaje.ebean.SqlRow
import com.fasterxml.jackson.databind.ObjectMapper
import controllers.calculo.CalculolController
import javax.inject.{Inject, Singleton}
import models.{Curso, Docente, Escuela, EscuelaDocente}
import play.api.mvc.{Action, Controller}

@Singleton
class DocenteController @Inject()() extends Controller {
  @throws[Exception]
  def cursos(id_docente:Long) = Action {
    val objectMapper = new ObjectMapper
    val result = new util.HashMap[String, Any]

    val docente = Docente.find.byId(id_docente)

    if(docente != null){
      val cursos = docente.cursos()
      result.put("cursos", cursos)
      Ok(objectMapper.writeValueAsString(result)).as("application/json")
    }else{
      result.put("code", 1)
      result.put("msg", "Docente no encontrado")
      NotFound(objectMapper.writeValueAsString(result)).as("application/json")
    }
  }

  @throws[Exception]
  def cursosPlataforma(id_docente: Long, plataforma: String) = Action {
    val objectMapper = new ObjectMapper
    val result = new util.HashMap[String, Any]

    val docente = Docente.find.byId(id_docente)

    if(docente != null){
      if( Set(plataforma) forall (CalculolController.validPlatforms contains) ) {
        val cursosSql = docente.cursos()
        var cursos: List[util.HashMap[String, Any]] = List()

        for (i <- 0 until cursosSql.size()) {
          val cursoHTmp = new util.HashMap[String, Any]
          val cursoTmp = Curso.find.byId(cursosSql.get(i).getLong("id_curso"))

          cursoHTmp.put("id_curso", cursosSql.get(i).getOrDefault("id_curso", ""))
          cursoHTmp.put("titulo", cursosSql.get(i).getOrDefault("titulo", ""))
          cursoHTmp.put("listaUsuarios", cursoTmp.allNodo())
          cursoHTmp.put("actividades", cursoTmp.actividades(plataforma))
          cursos ::= cursoHTmp
        }
        import scala.collection.JavaConverters._

        result.put("cursos", cursos.asJava)
        Ok(objectMapper.writeValueAsString(result)).as("application/json")
      }else{
        result.put("code", 9)
        result.put("msg", "Plataforma no válida")
        BadRequest(objectMapper.writeValueAsString(result)).as("application/json")
      }
    }else{
      result.put("code", 1)
      result.put("msg", "Docente no encontrado")
      NotFound(objectMapper.writeValueAsString(result)).as("application/json")
    }

  }

  @throws[Exception]
  def estudiantes(id_docente:Long) = Action {
    val objectMapper = new ObjectMapper
    val result = new util.HashMap[String, Any]

    val docente = Docente.find.byId(id_docente)

    if(docente != null){
      val estudiantes = docente.estudiantes
      result.put("estudiantes", estudiantes)
      Ok(objectMapper.writeValueAsString(result)).as("application/json")
    }else{
      result.put("code", 1)
      result.put("msg", "Docente no encontrado")
      NotFound(objectMapper.writeValueAsString(result)).as("application/json")
    }
  }

  @throws[Exception]
  def recursos(id_docente:Long) = Action {
    val objectMapper = new ObjectMapper
    val result = new util.HashMap[String, Any]

    val docente = Docente.find().byId(id_docente)
    if(docente != null){
      val recursos = docente.recursos(null)
      result.put("recursos", recursos)
      Ok(objectMapper.writeValueAsString(result)).as("application/json")
    }else{
      result.put("code", 1)
      result.put("msj", "Docente no encontrado.")
      NotFound(objectMapper.writeValueAsString(result)).as("application/json")
    }

  }

  @throws[Exception]
  def recursosByCurso(id_docente:Long, id_curso:Long) = Action {
    val objectMapper = new ObjectMapper
    val result = new util.HashMap[String, Any]

    val docente = Docente.find().byId(id_docente)
    val curso = Curso.find().byId(id_curso)
    if(docente != null && curso != null){
      val recursos = docente.recursos(id_curso)
      result.put("recursos", recursos)
      Ok(objectMapper.writeValueAsString(result)).as("application/json")
    }else if(docente == null){
      result.put("code", 1)
      result.put("msj", "Docente no encontrado.")
      NotFound(objectMapper.writeValueAsString(result)).as("application/json")
    }else{
      result.put("code", 4)
      result.put("msj", "Curso no encontrado.")
      NotFound(objectMapper.writeValueAsString(result)).as("application/json")
    }

  }
}
