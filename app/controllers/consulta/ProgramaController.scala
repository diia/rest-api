package controllers.consulta

import java.util

import com.fasterxml.jackson.databind.ObjectMapper
import javax.inject.{Inject, Singleton}
import models.{Docente, Escuela, EscuelaDocente, Programa}
import play.api.mvc.{Action, Controller}

@Singleton
class ProgramaController @Inject()() extends Controller {
  @throws[Exception]
  def cursos(id_programa:Long) = Action {
    val objectMapper = new ObjectMapper
    val result = new util.HashMap[String, Any]

    val programa = Programa.find.byId(id_programa)

    if(programa != null){
      val cursos = programa.cursos()
      result.put("cursos", cursos)
      Ok(objectMapper.writeValueAsString(result)).as("application/json")
    }else{
      result.put("code", 7)
      result.put("msg", "Programa no encontrado")
      NotFound(objectMapper.writeValueAsString(result)).as("application/json")
    }
  }
}
