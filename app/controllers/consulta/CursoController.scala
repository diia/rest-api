package controllers.consulta

import java.util
import java.util.stream.Collectors

import com.avaje.ebean.SqlRow
import com.avaje.ebeaninternal.server.query.DefaultSqlRow
import com.fasterxml.jackson.databind.ObjectMapper
import javax.inject.{Inject, Singleton}
import models.{Curso, Docente, Escuela, EscuelaDocente}
import play.api.mvc.{Action, Controller}

@Singleton
class CursoController @Inject()() extends Controller {

  @throws[Exception]
  def informacion(id_curso:Long) = Action {
    val objectMapper = new ObjectMapper
    val result = new util.HashMap[String, Any]
    val curso = Curso.find.byId(id_curso)

    if(curso != null){
      val informacion = curso.informacion
      //result.put("id_curso", informacion.get(0).get("id_curso"))
      result.put("titulo", informacion.get(0).get("titulo"))
      result.put("codigo", informacion.get(0).get("codigo"))
      result.put("seccion", informacion.get(0).get("seccion"))
      result.put("periodo_inicio", informacion.get(0).get("periodo_inicio"))
      result.put("periodo_fin", informacion.get(0).get("periodo_fin"))
      result.put("id_facebook", informacion.get(0).get("id_facebook"))
      result.put("id_pam", informacion.get(0).get("id_pam"))
      result.put("id_crea", informacion.get(0).get("id_crea"))
      result.put("id_moodle", informacion.get(0).get("id_moodle"))
      result.put("promedio_general", informacion.get(0).get("promedio_general"))
      Ok(objectMapper.writeValueAsString(result)).as("application/json")
    }else{
      result.put("code", 4)
      result.put("msg", "Curso no encontrado")
      NotFound(objectMapper.writeValueAsString(result)).as("application/json")
    }

  }

  @throws[Exception]
  def nDiasDesconectados(id_curso:Long, n_dias:Int) = Action {
    val objectMapper = new ObjectMapper
    val result = new util.HashMap[String, Any]
    val curso = Curso.find.byId(id_curso)

    if(n_dias <= 0){
      result.put("code", 19)
      result.put("msg", "Dato invalido.")
      BadRequest(objectMapper.writeValueAsString(result)).as("application/json")
    }else if(curso == null){
      result.put("code", 4)
      result.put("msg", "Curso no encontrado.")
      NotFound(objectMapper.writeValueAsString(result)).as("application/json")
    }else{
      val estudiantes = curso.estsDiasDesc
      val estsNDias = estudiantes.toArray.filter(
        fila => fila.asInstanceOf[SqlRow].get("dias").asInstanceOf[Int] < n_dias
      )
      result.put("estudiantes", estsNDias)
      Ok(objectMapper.writeValueAsString(result)).as("application/json")
    }
  }

  @throws[Exception]
  def actividades(id_curso:Long) = Action {
    val objectMapper = new ObjectMapper
    val result = new util.HashMap[String, Any]
    val curso = Curso.find.byId(id_curso)

    if(curso != null){
      val actividades = curso.actividades("*")
      result.put("actividades", actividades)
      Ok(objectMapper.writeValueAsString(result)).as("application/json")
    }else{
      result.put("code", 4)
      result.put("msg", "Curso no encontrado")
      NotFound(objectMapper.writeValueAsString(result)).as("application/json")
    }

  }

  @throws[Exception]
  def materiales(id_curso:Long) = Action {
    val objectMapper = new ObjectMapper
    val result = new util.HashMap[String, Any]
    val curso = Curso.find.byId(id_curso)

    if(curso != null){
      var materiales = curso.materiales("*")
      import scala.collection.JavaConversions._
      //remove some values
      materiales = materiales.map(
        m => {
          m.remove("id_curso")
          m
        }
      )
      result.put("materiales", materiales)
      Ok(objectMapper.writeValueAsString(result)).as("application/json")
    }else{
      result.put("code", 4)
      result.put("msg", "Curso no encontrado")
      NotFound(objectMapper.writeValueAsString(result)).as("application/json")
    }

  }

}
