package controllers.consulta

import java.util

import com.fasterxml.jackson.databind.ObjectMapper
import controllers.calculo.CalculolController
import javax.inject.{Inject, Singleton}
import models._
import play.api.mvc.{Action, Controller}

@Singleton
class InteraccionController @Inject()() extends Controller {

  @throws[Exception]
  def interacciones(idNodoOrigen:String, idNodoDestino:String, fecha_inicio:String, fecha_fin:String, intTypes:String) = Action {
    val objectMapper = new ObjectMapper
    val result = new util.HashMap[String, Any]

    val interactionTypes = intTypes.split("(?<=\\G.{3})").toSet[String]//Split into groups of three characters
    if( interactionTypes forall (CalculolController.validIntTypes contains)  ) {//All interaction types are valid
      if( !idNodoOrigen.equals("*") && Nodo.find().byId(idNodoOrigen.toLong) == null ){
        result.put("code", 11)
        result.put("msj", "Nodo origen no encontrado.")
        NotFound(objectMapper.writeValueAsString(result)).as("application/json")
      }else if( !idNodoDestino.equals("*") && Nodo.find().byId(idNodoDestino.toLong) == null ){
        result.put("code", 12)
        result.put("msj", "Nodo destino no encontrado.")
        NotFound(objectMapper.writeValueAsString(result)).as("application/json")
      }else {
        import scala.collection.JavaConverters._
        val interacciones = Interaccion.interaccionesOD(idNodoOrigen, idNodoDestino, fecha_inicio, fecha_fin, interactionTypes.toList.asJava)
        result.put("interacciones", interacciones)
        Ok(objectMapper.writeValueAsString(result)).as("application/json")
      }
    }else{
      result.put("code", 10)
      result.put("msg", s"Tipo de interacción no válida")
      BadRequest(objectMapper.writeValueAsString(result)).as("application/json")
    }

  }

  @throws[Exception]
  def interaccion(id_interaccion:Long) = Action {
    val objectMapper = new ObjectMapper
    val result = new util.HashMap[String, Any]

    val interaccion = Interaccion.find.byId(id_interaccion)

    if(interaccion != null){
      Ok(objectMapper.writeValueAsString(interaccion)).as("application/json")
    }else{
      result.put("code", 20)
      result.put("msg", "Interacción no encontrada")
      NotFound(objectMapper.writeValueAsString(result)).as("application/json")
    }
  }

}
