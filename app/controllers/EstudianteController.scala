package controllers

import java.util
import javax.inject.{Inject, Singleton}
import javax.print.DocFlavor.STRING
import com.avaje.ebean.{Ebean, SqlRow}
import play.api.libs.json.{JsError, JsSuccess, Json}
import com.fasterxml.jackson.databind.ObjectMapper
import models._
import play.api.libs.json.Json
import play.api.mvc.{Action, Controller}

@Singleton
class EstudianteController @Inject()() extends Controller {
  @throws[Exception]
  def detalle(id: Long) = Action {
    val objectMapper = new ObjectMapper
    val estudiante = Estudiante.find.byId(id)
    val result = new util.HashMap[String, Any]

    /*
    if(estudiante != null){
      result.put("id_estudiante", estudiante.getIdEstudiante)
      result.put("cedula", (if (estudiante.getCedula != null) estudiante.getCedula.toInt else null) )
      result.put("nombre", estudiante.getNombre)
      result.put("apellidos", estudiante.getApellidos)
      result.put("edad", estudiante.edad)//check
      result.put("correo", estudiante.getCorreo)
      result.put("telefono", estudiante.getTelefono)
      result.put("ciudad", estudiante.getCiudad_uno)
      result.put("localidad", estudiante.getLocalidad_uno)
      result.put("domicilio", estudiante.getDomicilio_uno)
      result.put("institucion", estudiante.getInstitucion)

      result.put("cursos", estudiante.cursos)
      result.put("actividades", estudiante.intActividades(-1L))
/*
      //Actividades con las que el estudiante ha interactuado más recientemente
        Las n actividades?
       //select interacciones n.o = idestud and n.d.tipo = 'a' -> aggregation sum -> order by desc -> limit 0, n
      [
        {
          id: “identificador de la actividad”
          nombre: “Nombre de la actividad”
          fecha: ”Fecha de la última interacción”
          tipo: “descripción de cuál fue la interacción”
        }
        ]
*/
    //myMap.put("actividades", docente.allInteraccion)
    Ok(objectMapper.writeValueAsString(result)).as("application/json")
    }else{
      result.put("code", 3)
      result.put("msg", "Estudiante no encontrado.")
      NotFound(objectMapper.writeValueAsString(result)).as("application/json")
    }
    */


    Option(Estudiante.find().byId(id)) map {
      estudiante =>
        Option(Nodo.find.byId(id)) map {
          nodo =>
            import scala.collection.JavaConverters._

            var prop = ( estudiante.detalle().asScala.map(
              property => (property._1, property._2)
            ) + ("trabaja"-> (estudiante.trabajos().size() > 0) )
              + ("edad"->estudiante.edad)
            )
            prop("ciudad") = prop.get("ciudad_uno").getOrElse("")
            prop("localidad") = prop.get("localidad_uno").getOrElse("")
            prop("domicilio") = prop.get("domicilio_uno").getOrElse("")

//            prop.remove("localidad")

            /*
             result.put("ciudad", estudiante.getCiudad_uno)
      result.put("localidad", estudiante.getLocalidad_uno)
      result.put("domicilio", estudiante.getDomicilio_uno)
             */
            val e = new EgoGraph(nodo.nodosEgoGraph().asScala.toList, nodo.intEgoGraph().asScala.toList, estudiante.cursos.asScala.toList, estudiante.intActividades(-1L).asScala.toList, prop)

            Ok(Json.toJson(e)(EgoGraph.EgoGraph))
        }getOrElse{
          result.put("code", 3)
          result.put("msj", "Estudiante no encontrado.")
          NotFound(objectMapper.writeValueAsString(result)).as("application/json")
        }
    }getOrElse{
      result.put("code", 3)
      result.put("msj", "Estudiante no encontrado.")
      NotFound(objectMapper.writeValueAsString(result)).as("application/json")
    }
  }

  @throws[Exception]
  def detalleByCurso(id_estudiante: Long, id_curso: Long) = Action {
    val objectMapper = new ObjectMapper
    val estudiante = Estudiante.find.byId(id_estudiante)
    val curso = Curso.find.byId(id_curso)
    val result = new util.HashMap[String, Any]

    if(estudiante == null){
      result.put("code", 3);
      result.put("msg", "Estudiante no encontrado");
      NotFound(objectMapper.writeValueAsString(result)).as("application/json")
    }else if(curso==null){
      result.put("code", 4);
      result.put("msg", "Curso no encontrado");
      NotFound(objectMapper.writeValueAsString(result)).as("application/json")
    }else{
      val estudianteCurso = estudiante.estudianteCurso(id_curso);
      if(estudianteCurso.size()>0) {
        result.put("id_estudiante", estudiante.getIdEstudiante)
        result.put("cedula", (if (estudiante.getCedula != null) estudiante.getCedula.toInt else null) )
        result.put("nombre", estudiante.getNombre)
        result.put("apellidos", estudiante.getApellidos)
        result.put("edad", estudiante.edad().toString) //check
        result.put("correo", estudiante.getCorreo)
        result.put("telefono", estudiante.getTelefono)
        result.put("ciudad", estudiante.getCiudad_uno)
        result.put("localidad", estudiante.getLocalidad_uno)
        result.put("domicilio", estudiante.getDomicilio_uno)
        result.put("institucion", estudiante.getInstitucion)

        //result.put("curso", estudiante.curso(id_curso))

        result.put("plataformas", estudiante.plataformas())
        result.put("actividades", estudiante.intActividades(id_curso))

        /*
            //Actividades con las que el estudiante ha interactuado más recientemente
              Las n actividades?
             //select interacciones n.o = idestud and n.d.tipo = 'a' -> aggregation sum -> order by desc -> limit 0, n
            [
              {
                id: “identificador de la actividad”
                nombre: “Nombre de la actividad”
                fecha: ”Fecha de la última interacción”
                tipo: “descripción de cuál fue la interacción”
              }
              ]
      */
        //myMap.put("actividades", docente.allInteraccion)
        Ok(objectMapper.writeValueAsString(result)).as("application/json")
      }else{
        result.put("code", 8)
        result.put("msg", "El estudiante no está inscrito al curso especificado.");
        NotAcceptable(objectMapper.writeValueAsString(result)).as("application/json")
      }
    }
  }


  @throws[Exception]
  def conexion(id_estudiante: Long, plataforma: String, fecha_inicio: String, fecha_fin: String, opcion: String) = Action {
    val objectMapper = new ObjectMapper
    val estudiante = Estudiante.find.byId(id_estudiante)
    val result = new util.HashMap[String, Any]

    if(estudiante != null){
      var date_filter = "YYYY-MM-DD";
      if(opcion.equalsIgnoreCase("anio")){
        date_filter = "YYYY";
      }else if(opcion.equalsIgnoreCase("mes")){
        date_filter = "YYYY-MM";
      }
      result.put("conexion", estudiante.estudianteCon(fecha_inicio, fecha_fin, plataforma, date_filter));
      Ok(objectMapper.writeValueAsString(result)).as("application/json")
    }else{
      result.put("code", 3);
      result.put("msg", "Estudiante no encontrado");
      NotFound(objectMapper.writeValueAsString(result)).as("application/json")
    }
  }

  import com.avaje.ebean.SqlRow
  import scala.collection.Map

  case class EgoGraph(nodes: List[SqlRow], interactions: List[SqlRow], cursos: List[SqlRow], actividades: List[SqlRow], properties: Map[String, Any])

  import play.api.libs.json._
  import play.api.libs.json.Json._

  object EgoGraph{
    import play.api.libs.json._
    import scala.collection.JavaConverters._

    //writter of Anyval

    implicit val EgoGraph = new Writes[EgoGraph] {
      def writes(eg: EgoGraph): JsValue = {
        Json.toJson(
          eg.properties.foldLeft(Json.obj())((obj, row) =>
            obj ++ Json.obj(row._1 -> (row._2 match {
              case n:Number => BigDecimal(n.toString)
              case b:Boolean => b
              case _ => row._2.toString
            })
            )
          ).
            ++(
              obj(
                "actividades" ->
                  eg.actividades.map(
                    row =>

                      row.asScala.foldLeft(Json.obj())((obj, row) =>
                        obj ++ Json.obj(row._1 -> (Option(row._2.asInstanceOf[Any]).getOrElse("") match {
                          case n:Number => BigDecimal(n.toString)
                          case b:Boolean => b
                          case c:AnyRef => c.toString
                        })
                        )
                      )

                  )
              )
            ).
            ++(
              obj(
                "cursos" ->
                  eg.cursos.map(
                    row =>
                      row.asScala.foldLeft(Json.obj())((obj, row) =>
                        obj ++ Json.obj(row._1 -> (row._2.asInstanceOf[Any] match {
                          case n:Number => BigDecimal(n.toString)
                          case b:Boolean => b
                          case _ => row._2.toString
                        })
                        )
                      )

                  )
              )
            ).
            ++(
              obj(
                "egoGrafo" -> obj(
                  "nodos" -> eg.nodes.map(
                    row =>
                      obj(
                        "id" -> BigDecimal(row.getLong("id")),
                        "tipo" -> row.getString("tipo"),
                        "nombre" -> row.getString("nombre")
                      )
                  ),
                  "interacciones" -> eg.interactions.map(
                    row =>
                      obj(
                        "id_interaccion" -> BigDecimal(row.getLong("id_interaccion")),
                        "origen" -> row.getString("origen"),
                        "origen_tipo" -> row.getString("origen_tipo"),
                        "destino" -> row.getString("destino"),
                        "destino_tipo" -> row.getString("destino_tipo"),
                        "tipo" -> row.getString("tipo")
                      )
                  )
                )
              )
            )
        )

      }
    }


  }
}
