name := """diia"""

version := "1.0"

lazy val root = (project in file(".")).enablePlugins(PlayScala, PlayEbean, RpmPlugin, DebianPlugin)

scalaVersion := "2.11.11"

libraryDependencies += jdbc
libraryDependencies += cache
libraryDependencies += ws
libraryDependencies += "org.scalatestplus.play" %% "scalatestplus-play" % "2.0.0" % Test
libraryDependencies += filters

// https://mvnrepository.com/artifact/org.graphstream/gs-algo
libraryDependencies += "org.graphstream" % "gs-algo" % "1.3"

// https://mvnrepository.com/artifact/org.graphstream/gs-core
libraryDependencies += "org.graphstream" % "gs-core" % "1.3"

libraryDependencies += "org.graphstream" % "gs-ui" % "1.3"

// https://mvnrepository.com/artifact/org.postgresql/postgresql
libraryDependencies += "org.postgresql" % "postgresql" % "42.1.4"

// https://mvnrepository.com/artifact/com.google.code.gson/gson
libraryDependencies += "com.google.code.gson" % "gson" % "2.8.2"

// https://mvnrepository.com/artifact/com.fasterxml.jackson.core/jackson-core
libraryDependencies += "com.fasterxml.jackson.core" % "jackson-core" % "2.9.4"

// Server distribution settings
rpmAutoreq in Rpm := "no"
maintainer in Linux := "DIIA <proyecto@diia.edu.uy>"
packageSummary in Linux := "DIIA Framework"
packageDescription := "DIIA Framework"
rpmRelease := "1"
rpmVendor := "www.diia.edu.uy"
rpmUrl := Some("https://github.com/GrupoDIIA/DIIA-Framework")
rpmLicense := Some("Apache v2")