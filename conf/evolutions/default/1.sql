# --- Created by Ebean DDL
# To stop Ebean DDL generation, remove this comment and start using Evolutions

# --- !Ups

create table actividad (
  id_actividad              bigserial not null,
  tipo                      varchar(2),
  fecha_limite              timestamp,
  calificacion_minima       float,
  calificacion_maxima       float,
  constraint pk_actividad primary key (id_actividad))
;

create table curso (
  id_curso                  bigserial not null,
  titulo                    varchar(100),
  codigo                    varchar(20),
  seccion                   varchar(20),
  periodo_inicio            timestamp,
  periodo_fin               timestamp,
  id_escuela                bigint,
  id_facebook               varchar(20),
  id_pam                    varchar(20),
  id_crea                   varchar(20),
  id_moodle                 varchar(20),
  constraint pk_curso primary key (id_curso))
;

create table cursodocente (
  id_curso                  bigint,
  id_docente                bigint)
;

create table docente (
  id_docente                bigserial not null,
  nombre                    varchar(150),
  apellidos                 varchar(150),
  sexo                      varchar(255),
  fecha_nacimiento          timestamp,
  correo                    varchar(100),
  area                      varchar(30),
  id_facebook               varchar(20),
  id_pam                    varchar(20),
  id_crea                   varchar(20),
  id_moodle                 varchar(20),
  constraint pk_docente primary key (id_docente))
;

create table escuela (
  id_escuela                bigserial not null,
  nombre                    varchar(150),
  direccion                 varchar(150),
  ciudad                    varchar(50),
  region                    varchar(50),
  latitude                  varchar(30),
  longitude                 varchar(30),
  constraint pk_escuela primary key (id_escuela))
;

create table EscuelaDocente (
  id_docente                bigint,
  id_escuela                bigint)
;

create table estudiante (
  id_estudiante             bigserial not null,
  nombre                    varchar(150),
  apellidos                 varchar(150),
  cedula                    varchar(10),
  fecha_nacimiento          timestamp,
  sexo                      varchar(255),
  promedio_vigente          float,
  promedio_acumulado        float,
  correo                    varchar(100),
  telefono                  varchar(13),
  institucion               varchar(100),
  id_facebook               varchar(20),
  id_pam                    varchar(20),
  id_crea                   varchar(20),
  id_moodle                 varchar(20),
  ciudad_uno                varchar(150),
  localidad_uno             varchar(150),
  domicilio_uno             varchar(150),
  ciudad_dos                varchar(150),
  localidad_dos             varchar(150),
  domicilio_dos             varchar(150),
  constraint pk_estudiante primary key (id_estudiante))
;

create table estudiantecurso (
  id_curso                  bigint,
  id_estudiante             bigint,
  tipo                      varchar(2))
;

create table handler)
;

create table interaccion (
  id_interaccion            bigserial not null,
  nodo_origen               bigint,
  nodo_destino              bigint,
  tipo_interaccion          varchar(3),
  id_curso_origen           bigint,
  id_curso_destino          bigint,
  tipo_contenido            varchar(3),
  contenido                 varchar(255),
  timestamp                 timestamp,
  sentimiento               varchar(1),
  plataforma                varchar(255),
  id_origen                 varchar(255),
  constraint pk_interaccion primary key (id_interaccion))
;

create table material (
  id_material               bigserial not null,
  id_curso                  bigint,
  plataforma                varchar(2),
  nombre                    varchar(255),
  tipo                      varchar(2),
  tipo_contenido            varchar(3),
  fecha_publicacion         timestamp,
  numero_accesos            integer,
  url_ubicacion             varchar(255),
  constraint pk_material primary key (id_material))
;

create table nodo (
  id_nodo                   bigserial not null,
  tipo                      varchar(255),
  constraint pk_nodo primary key (id_nodo))
;

create table programa (
  id_programa               bigserial not null,
  id_escuela                bigint,
  nombre                    varchar(255),
  constraint pk_programa primary key (id_programa))
;




# --- !Downs

drop table if exists actividad cascade;

drop table if exists curso cascade;

drop table if exists cursodocente cascade;

drop table if exists docente cascade;

drop table if exists escuela cascade;

drop table if exists EscuelaDocente cascade;

drop table if exists estudiante cascade;

drop table if exists estudiantecurso cascade;

drop table if exists handler cascade;

drop table if exists interaccion cascade;

drop table if exists material cascade;

drop table if exists nodo cascade;

drop table if exists programa cascade;

